package game.scenarios.modes 
{
	import engine.interfaces.IDeserializeable;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	/**
	 * A wave that is some amount of units away from its target (usually the player).
	 * Enemies are spawned in some number of clumps along the circumfrence of a circle
	 * dictated by the distance value.
	 */
	 
	public class RadialGroupedWave extends EventDispatcher implements ITickable, IDestroyable, IDeserializeable
	{		
		/* Configuration */		
		private var _Wave:Vector.<SpaceShip>;
		
		private var _GameData:HomeGameData;
		
		/* Active */
		private var _ActiveCombatants:Vector.<SpaceShip>;
		
		private var _DistanceAway:int = 0;
		private var _AreSpawningIn:Boolean = false;
		
		public function RadialGroupedWave( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			// Allocation
			_Wave:Vector.<SpaceShip> = new Vector.<SpaceShip>();
			_ActiveCombatants = new Vector.<SpaceShip>();
		}
		
		public function deserialize( xml:XML ):void
		{
			
		}
		
		public function spawn():void
		{
			var spawner:WaveSpawner = new WaveSpawner();
			
			spawner.spawnWave( _GameData, 
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
		
		public function destroy():void
		{
			
		}
	}
}