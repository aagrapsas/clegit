package game.scenarios.modes 
{
	import game.HomeGameData;
	import game.scenarios.GameMode;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CustardGameMode extends GameMode
	{
		public function CustardGameMode( gameData:HomeGameData ) 
		{
			super( gameData );
		}
	
		override public function tick( deltaTime:Number ):void
		{
			super.tick( deltaTime );
		}
		
		override public function deserialize( xml:XML ):void
		{
			super.deserialize( xml );
		}
		
		override public function destroy():void
		{
			super.destroy();
		}
	}
}