package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name indestructable
	// @description sets the target to indestructable if value is true
	// @category actor
	// @isConditional false
	// @param_required target:String value:Boolean
	public class IndestructableScript extends Script
	{
		private var _Target:String;
		private var _IsIndestructable:Boolean;
		
		public function IndestructableScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
			_IsIndestructable = xml.@value == "true";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Target == "player" )
			{
				gameData.PlayerShip.CanTakeDamage = !_IsIndestructable;
			}
			else
			{
				var ai:AIController = gameData.CurrentScenario.getAI( _Target );
			
				if ( ai )
				{
					ai.Ship.CanTakeDamage = !_IsIndestructable;
				}
				else
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "AI " + _Target + " doesn't exist" );
				}
			}
			
			return true;
		}
	}
}