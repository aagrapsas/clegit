package game.scenarios.scripting 
{
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CameraShakeScript extends Script
	{	
		private var _Magnitude:int;
		private var _Duration:Number;
		private var _HasExecuted:Boolean = false;
		private var _Number:int;
		private var _Current:int;
		
		// @name cameraShake
		// @description shakes the camera
		// @category camera
		// @isConditional false
		// @param_required magnitude:int duration:Number number:int
		public function CameraShakeScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Magnitude = int( xml.@magnitude );
			_Duration = Number( xml.@duration );
			_Number = int( xml.@number );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( !gameData.ActiveCamera )
				return true;
				
			if ( !_HasExecuted )
			{
				_HasExecuted = true;
				gameData.ActiveCamera.shake( _Magnitude, _Duration, null );
			}
			
			if ( _HasExecuted && _Current < _Number && !gameData.ActiveCamera.IsShaking )
			{
				_Current++;
				gameData.ActiveCamera.shake( _Magnitude, _Duration, null );
			}
			
			return gameData.ActiveCamera.IsShaking == false && _Current >= _Number;
		}
		
		override public function reset():void
		{
			super.reset();
			
			_Current = 0;
			_HasExecuted = false;
		}
	}
}