package game.scenarios.scripting 
{
	import game.HomeGameData;
	import game.ui.misc.UIHelpScreen;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShowHelpScript extends Script
	{
		public function ShowHelpScript() 
		{
			
		}
	
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.Scene.addPopup( new UIHelpScreen( gameData ) );
			
			return true;
		}
	}
}