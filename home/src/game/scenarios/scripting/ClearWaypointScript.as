package game.scenarios.scripting 
{
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name clearWaypoint
	// @description removes the currently set waypoint
	// @category actor
	// @isConditional false
	// @param_required
	// @param_optional
	public class ClearWaypointScript extends Script
	{
		public function ClearWaypointScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( gameData.PlayerWaypoint && gameData.PlayerWaypointDir )
			{
				gameData.Scene.removeActor( gameData.PlayerWaypoint );
				gameData.Scene.removeActor( gameData.PlayerWaypointDir );
				
				gameData.Scene.removeTickable( gameData.PlayerWaypoint );
				gameData.Scene.removeTickable( gameData.PlayerWaypointDir );
				
				gameData.PlayerWaypoint.destroy();
				gameData.PlayerWaypointDir.destroy();
				
				gameData.PlayerWaypoint = null;
				gameData.PlayerWaypointDir = null;
			}
			
			return true;
		}
	}
}