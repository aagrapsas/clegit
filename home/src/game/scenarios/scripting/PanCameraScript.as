package game.scenarios.scripting 
{
	import engine.misc.MathUtility;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name panCamera
	// @description pans the camera, can be used as a trigger
	// @category camera
	// @isConditional true
	// @param_required x:int y:int
	// @param_optional duration:int
	public class PanCameraScript extends Script
	{
		private var _X:int;
		private var _Y:int;
		
		private var _StartX:int;
		private var _StartY:int;
		private var _Duration:Number;
		
		private var _ShouldPan:Boolean;
		
		private var _StartPanStamp:Number = 0;
		
		public function PanCameraScript() 
		{
			
		}
	
		override public function deserialize( xml:XML ):void
		{
			_X = -int( xml.@x );
			_Y = int( xml.@y );
			_Duration = xml.@duration != undefined ? int( xml.@duration ) : 0;
			
			_ShouldPan = xml.@duration != undefined;
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _ShouldPan )
			{
				if ( _StartPanStamp == 0 )
				{
					_StartX = gameData.ActiveCamera.X;
					_StartY = gameData.ActiveCamera.Y;
					
					_StartPanStamp = gameData.Scene.GameTime;
				}
				
				const alpha:Number = MathUtility.getEaseInOutAlpha( Math.min( 1, ( gameData.Scene.GameTime - _StartPanStamp ) / _Duration ) );
				
				const x:int = gameData.ActiveCamera.X - MathUtility.lerpInt( _StartX, _X, alpha );
				const y:int = gameData.ActiveCamera.Y - MathUtility.lerpInt( _StartY, _Y, alpha );
				
				gameData.ActiveCamera.scrollXY( -x, -y );
			}
			else
			{
				if ( gameData.ActiveCamera.IsInitialized )
				{
					gameData.ActiveCamera.scrollXY( -( gameData.ActiveCamera.X - _X ), -( gameData.ActiveCamera.Y - _Y ) );
				}
				else
				{
					gameData.ActiveCamera.centerAt( _X, _Y );
				}
			}
			
			return gameData.ActiveCamera.X == _X && gameData.ActiveCamera.Y == _Y;
		}
		
		override public function reset():void
		{
			super.reset();
			
			_StartPanStamp = 0;
		}
	}
}