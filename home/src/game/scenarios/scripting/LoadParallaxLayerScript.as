package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import engine.render.scene.RandomSceneLayer;
	import engine.render.scene.SceneLayer;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.renderer.RepeatingSceneLayer;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name loadLayer
	// @description loads the appropriate layer
	// @category level
	// @isConditional false
	// @param_required name:String movementPercent:Number
	// @param_optional layerType:String images:String density:Number alpha:Number asset:String
	public class LoadParallaxLayerScript extends Script
	{
		private var _LayerName:String;
		private var _MovementSpeed:Number;
		private var _LayerType:String;
		private var _IsEmpty:Boolean;
		private var _LayerImages:String;
		private var _Density:Number;
		private var _Alpha:Number;
		private var _Asset:String;
		
		public function LoadParallaxLayerScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_LayerName = xml.@name != undefined ? String( xml.@name ) : "";
			_MovementSpeed = Number( xml.@movementPercent );
			_LayerType = xml.@layerType != undefined ? String( xml.@layerType ): "";
			_LayerImages = xml.@images != undefined ? String( xml.@images ) : "";
			_Density = xml.@density != undefined ? Number( xml.@density ) : 0;
			_Alpha = xml.@alpha != undefined ? Number( xml.@alpha ) : 1;
			_Asset = xml.@asset != undefined ? String( xml.@asset ) : "";
			
			_IsEmpty = _Asset == "";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			var layer:SceneLayer = null;
			
			if ( _LayerType == "repeating" )
			{
				layer = new RepeatingSceneLayer( _LayerName, gameData, _MovementSpeed, ( gameData.AssetManager.getImage( _Asset ) as Bitmap ).bitmapData );
			}
			else
			{
				layer = new SceneLayer( _LayerName, _MovementSpeed );
				
				if ( !_IsEmpty )
			{
				const image:Bitmap = gameData.AssetManager.getImage( _Asset ) as Bitmap;
				const newImage:Bitmap = new Bitmap( image.bitmapData.clone(), "auto", true );
				layer.Display.addChild( newImage );
			}
			}
			
			gameData.Scene.addLayer( layer );
				
			return true;
		}
	}
}