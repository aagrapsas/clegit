package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.fx.HyperspaceFX;
	import game.fx.ShipFXTypes;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name playHyperspace
	// @description tells a ship to go to hyperpsace
	// @category actor
	// @isConditional false
	// @param_required target:String isJumpingIn:Boolean
	public class PlayHyperspaceFXScript extends Script
	{
		private var _Target:String;
		private var _IsJumpingIn:Boolean;
		private var _IsDone:Boolean = false;
		private var _IsPlayingFX:Boolean = false;
		private var _HasToldShipToTurn:Boolean = false;
		
		private var _GameData:HomeGameData;
		
		public function PlayHyperspaceFXScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
			_IsJumpingIn = String( xml.@isJumpingIn ) == "true";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{	
			if ( _IsPlayingFX )
				return _IsDone;
			
			_GameData = gameData;
				
			if ( _Target == "player" )
			{
				//gameData.PlayerController.playFX( ShipFXTypes.HYPERSPACE_JUMP_FX, { isWarpingIn:_IsJumpingIn }, onDone );
			}
			else
			{
				var ai:AIController = gameData.CurrentScenario.getAI( _Target );
				
				if ( ai )
				{
					//ai.playFX( ShipFXTypes.HYPERSPACE_JUMP_FX, { isWarpingIn:_IsJumpingIn }, onDone );
					var fx:HyperspaceFX = new HyperspaceFX( gameData, ai.Ship, ai.Ship.Dir.x, ai.Ship.Dir.y );
					
					if ( _IsJumpingIn )
					{
						fx.jumpIn( ai.Ship.X, ai.Ship.Y, onDone );
					}
					else
					{
						fx.jumpOut( onDone );
					}
				}
				else
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "HyperspaceScript: AI " + _Target + " doesn't exist" );
				}
			}
			
			// gameData.SoundSystem.playSound( "HyperspaceJumpSound" );
			
			_IsPlayingFX = true;

			return _IsDone;
		}
		
		private function onDone( ship:SpaceShip ):void
		{
			if ( !_IsJumpingIn && ai && ai.Ship && ai.Ship.IsDead == false )
			{
				var ai:AIController = _GameData.CurrentScenario.getAI( _Target );
			
				if ( ai )
				{
					_GameData.CurrentScenario.removeAI( ai.Name );
				}
			}
			
			_GameData = null;
			
			_IsDone = true;
		}
	}
}