package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.gameplay.weapons.DamageTypes;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name damage
	// @description damages the target
	// @category actor
	// @isConditional false
	// @param_required target:String damage:int
	// @param_optional
	public class DamageScript extends Script
	{
		private var _Target:String;
		private var _Damage:int;
		
		public function DamageScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
			_Damage = int( xml.@amount );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Target == "player" )
			{
				gameData.PlayerShip.DoDamage( DamageTypes.MISC, _Damage, null, gameData.PlayerShip.X, gameData.PlayerShip.Y );
			}
			else
			{
				var ai:AIController = gameData.CurrentScenario.getAI( _Target );
			
				if ( ai )
				{
					ai.Ship.DoDamage( DamageTypes.MISC, _Damage, null, ai.Ship.X, ai.Ship.Y );
				}
				else
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "DamageScript: AI " + _Target + " doesn't exist" );
				}
			}
			
			return true;
		}
	}
}