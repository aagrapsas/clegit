package game.scenarios.scripting 
{
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name showBlinds
	// @description shows or hides cinematic blinds
	// @category camera
	// @isConditional false
	// @param_required value:Boolean
	public class ShowBlindsScript extends Script
	{
		private var _ShouldShow:Boolean;
		
		public function ShowBlindsScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_ShouldShow = xml.@value == "true";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.Scene.toggleBlinds( _ShouldShow );
			
			if ( _ShouldShow )
			{
				gameData.SoundSystem.playSound( "CinematicBarsSlideIn" );
			}
			
			return true;
		}
	}
}