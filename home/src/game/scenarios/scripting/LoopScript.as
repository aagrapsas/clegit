package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import flash.utils.getQualifiedClassName;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class LoopScript extends Script
	{
		private var _Iterations:int;
		private var _Count:int;
		
		private var _ChildrenCache:Vector.<Script>;
		
		private var _CurrentNodes:Vector.<Script>;
		
		public function LoopScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{			
			if ( xml.@i == "infinite" )
				_Iterations = -1;
			else
				_Iterations = int( xml.@i );
			
			_CurrentNodes = new Vector.<Script>();//this.Children;
			_ChildrenCache = new Vector.<Script>();
			
			for each ( var script:Script in this.Children )	// duplicate!
			{
				_CurrentNodes.push( script );
				_ChildrenCache.push( script );
			}
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Count < _Iterations || _Iterations == -1 )
			{
				var toAdd:Vector.<Script> = new Vector.<Script>();
				var toRemove:Vector.<Script> = new Vector.<Script>();
				
				// Activate children & update current
				for each ( var child:Script in _CurrentNodes )
				{
					//Debug.debugLog( GameLogChannels.DEBUG, "Executing script of " + getQualifiedClassName( child ) );
					if ( !child.isValid( gameData ) || child.execute( gameData ) )
					{
						if ( gameData.CurrentScenario.isMarkedForDestroy )
							return true;
							
						for each ( var script:Script in child.Children )
						{
							//Debug.debugLog( GameLogChannels.DEBUG, "Adding script " + getQualifiedClassName( script ) );
							toAdd.push( script );
						}
						
						toRemove.push( child );
					}
				}
				
				// Remove
				for each ( var remove:Script in toRemove )
				{
					_CurrentNodes.splice( _CurrentNodes.indexOf( remove ), 1 );
				}
				
				// Add
				for each ( var add:Script in toAdd )
				{
					_CurrentNodes.push( add );
				}
				
				if ( _CurrentNodes.length == 0 )
				{
					//Debug.debugLog( GameLogChannels.DEBUG, "Incrementing loop count from " + _Count + " to +1" );
					_Count++;
					softReset();	// resets this node and its children
				}
			}
			else
			{
				//Debug.debugLog( GameLogChannels.DEBUG, "Done looping" );
				// Remove all children before returning, so the chain isn't executed
				if ( this.Children.length > 0 )
				{
					this.Children.splice( 0, this.Children.length );	// remove all children
				}
				
				return true;
			}
			
			return false;
		}
		
		private function softReset():void
		{
			super.reset();
			
			_CurrentNodes = new Vector.<Script>();
			
			for each ( var script:Script in _ChildrenCache )
			{
				_CurrentNodes.push( script );
			}
		}
		
		override public function reset():void
		{
			super.reset();
			
			_CurrentNodes = new Vector.<Script>();
			this.Children = new Vector.<Script>();
			
			_Count = 0;
			
			for each ( var script:Script in _ChildrenCache )
			{
				_CurrentNodes.push( script );
			}
		}
	}

}