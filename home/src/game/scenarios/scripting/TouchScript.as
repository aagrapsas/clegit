package game.scenarios.scripting 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneNode;
	import flash.display.Graphics;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name touch
	// @description triggers on touch
	// @category trigger
	// @isConditional true
	// @param_required width:int height:int x:int y:int
	// @param_optional target:String drawDebug:Boolean
	public class TouchScript extends Script
	{
		private var _Width:int;
		private var _Height:int;
		private var _VolumeX:int;
		private var _VolumeY:int;
		private var _Target:String;
		private var _ShouldDrawDebug:Boolean;
		
		public function TouchScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Width = int( xml.@width );
			_Height = int( xml.@height );
			_VolumeX = int( xml.@x );
			_VolumeY = int( xml.@y );
			_Target = xml.@target != undefined ? String( xml.@target ) : null;
			_ShouldDrawDebug = xml.@drawDebug != undefined && xml.@drawDebug == "true";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			var graphics:Graphics;
			
			if ( _ShouldDrawDebug )
					graphics = gameData.DebugSystem.DebugSprite.graphics;
			
			var colliders:Vector.<SceneNode> = gameData.Scene.rectCheck( _VolumeX, _VolumeY, _Width, _Height, CollisionFlags.ACTORS, graphics, 0x99FF00 );
			var targetInVolume:Boolean = false;
			
			if ( colliders.length != 0 )
			{
				if ( !_Target )
					return true;
					
				var target:SpaceShip;
				
				if ( _Target == "player" )
				{
					target = gameData.PlayerShip;
				}
				else
				{
					target = gameData.CurrentScenario.getAI( _Target ).Ship;
				}
					
				if ( !target )
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "TouchScript: invalid target, given: " + _Target );
					return true;
				}
				
				for each ( var collider:SceneNode in colliders )
				{
					if ( collider.InternalObject == target )
					{
						return true;
					}
				}
			}
			
			return false;
		}
	}
}