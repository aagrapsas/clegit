package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name log
	// @description logs a message to the console
	// @category debug
	// @isConditional false
	// @param_required message:String
	public class LogScript extends Script
	{
		private var _Message:String;
		
		public function LogScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Message = String( xml.@message );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			Debug.debugLog( GameLogChannels.SCRIPT, _Message );
			
			return true;
		}
	}
}