package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.actors.SpaceShip;
	import game.ai.AttackState;
	import game.ai.IState;
	import game.ai.StateFactory;
	import game.ai.StateTypes;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name attackTarget
	// @description tells attacker to attack target
	// @category actor
	// @isConditional false
	// @param_required target:String attacker:String
	public class AttackTargetScript extends Script
	{
		private var _Target:String;
		private var _Attacker:String;
		
		public function AttackTargetScript() 
		{
			
		}
	
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
			_Attacker = String( xml.@attacker );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Attacker == "player" )
			{
				Debug.errorLog( GameLogChannels.SCRIPT, "AttackTargetScript: the player can't be set as the attacker, duh! Needs to be AI" );
				return true;
			}
			
			var ai:AIController = gameData.CurrentScenario.getAI( _Attacker );
			
			var target:SpaceShip;
			
			if ( _Target == "player" )
			{
				target = gameData.PlayerShip;
			}
			else
			{
				var targetAI:AIController = gameData.CurrentScenario.getAI( _Target );
				target = targetAI.Ship;
			}
			
			var state:IState = new AttackState( ai.Ship, gameData, ai, target );
			
			ai.goToExplicitState( StateTypes.ATTACK_STATE, state );
		
			return true;
		}
	}
}