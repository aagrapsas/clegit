package game.scenarios.scripting 
{
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name showCaption
	// @description shows text
	// @category camera
	// @isConditional false
	// @param_required text:String duration:Number
	public class ShowCaptionScript extends Script
	{
		private var _Text:String;
		private var _Duration:Number;
		
		private var _Stamp:Number = 0;
		
		public function ShowCaptionScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Text = String( xml.@text );
			_Duration = Number( xml.@duration );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Stamp == 0 )
			{
				gameData.Scene.showText( _Text, _Duration );
				_Stamp = gameData.Scene.GameTime;		
			}
			
			return ( gameData.Scene.GameTime - _Stamp >= _Duration );
		}
		
		override public function reset():void
		{
			super.reset();
			
			_Stamp = 0;
		}
	}
}