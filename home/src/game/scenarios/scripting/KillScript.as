package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.gameplay.weapons.DamageTypes;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name kill
	// @description kills the target
	// @category actor
	// @isConditional false
	// @param_required target:String
	public class KillScript extends Script
	{
		private var _Target:String;
		
		public function KillScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Target == "player" )
			{
				gameData.PlayerShip.DoDamage( DamageTypes.MISC, gameData.PlayerShip.ArmorHealth.current + gameData.PlayerShip.ShieldHealth ? gameData.PlayerShip.ShieldHealth.current : 0, null, gameData.PlayerShip.X, gameData.PlayerShip.Y );
			}
			else
			{
				var ai:AIController = gameData.CurrentScenario.getAI( _Target );
			
				if ( ai )
				{
					ai.Ship.DoDamage( DamageTypes.MISC, ai.Ship.ArmorHealth.current + ai.Ship.ShieldHealth ? ai.Ship.ShieldHealth.current : 0, null, ai.Ship.X, ai.Ship.Y );
				}
				else
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "KillScript: AI " + _Target + " doesn't exist" );
				}
			}
			
			return true;
		}
	}
}