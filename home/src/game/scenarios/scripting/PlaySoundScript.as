package game.scenarios.scripting 
{
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name playSound
	// @description plays the designated sound if it exists
	// @category audio
	// @isConditional false
	// @param_required name:String
	public class PlaySoundScript extends Script
	{
		private var _Sound:String;
		
		public function PlaySoundScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Sound = String( xml.@name );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.SoundSystem.playSound( _Sound );
			
			return true;
		}
	}
}