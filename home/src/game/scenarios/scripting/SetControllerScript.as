package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.controllers.AIController;
	import game.controllers.PlayerSpaceShipController;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name setController
	// @description sets the controller for the designated actor
	// @category actor
	// @isConditional false
	// @param_required target:String controller:String
	public class SetControllerScript extends Script
	{
		private var _Target:String;
		private var _Controller:String;
		
		public function SetControllerScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
			_Controller = String( xml.@controller );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			if ( _Controller == "player" )
			{
				var ai:AIController = gameData.CurrentScenario.getAI( _Target );
				
				if ( ai )
				{
					if ( gameData.PlayerController )
						gameData.PlayerController.destroy();
					
					gameData.PlayerController = new PlayerSpaceShipController( ai.Ship, gameData );
					gameData.PlayerShip = ai.Ship;
					
					gameData.CurrentScenario.removeAI( _Target );
				}
				else
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "SetControllerScript: Unable to find existing AI " + _Target + " in scene." );
				}
			}
			else if ( _Target == "player" && _Controller == "ai" )
			{
				
			}
			
			return true;
		}
	}
}