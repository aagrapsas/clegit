package game.scenarios.scripting 
{
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name loadScenario
	// @description loads a level
	// @category level
	// @isConditional false
	// @param_required name:String
	public class LoadScenarioScript extends Script
	{		
		private var _ScenarioName:String;
		
		public function LoadScenarioScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_ScenarioName = String( xml.@name );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.CurrentScenario.breakDown();
			gameData.CurrentScenario.destroy();
			
			gameData.CurrentScenario = new Scenario( gameData );
			gameData.CurrentScenario.deserialize( gameData.AssetManager.getXML( _ScenarioName ) );
			gameData.CurrentScenario.setup();
			
			return true;
		}
	}
}