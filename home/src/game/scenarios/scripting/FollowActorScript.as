package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.actors.SpaceShip;
	import game.ai.FollowActorLocomotionState;
	import game.ai.IState;
	import game.ai.StateFactory;
	import game.ai.StateTypes;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name followActor
	// @description makes the follower follow the target
	// @category actor
	// @isConditional true
	// @param_required target:String follower:String x:int y:int
	public class FollowActorScript extends Script
	{
		private var _Target:String;
		private var _Follower:String;
		private var _X:int;
		private var _Y:int;
		
		public function FollowActorScript()
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
			_Follower = String( xml.@follower );
			_X = int( xml.@x );
			_Y = int( xml.@y );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{			
			if ( _Target == "player" )
			{
				Debug.errorLog( GameLogChannels.SCRIPT, "FollowActorScript: functionality not implemented for player right now." );
			}
			else
			{
				var followerAI:AIController = gameData.CurrentScenario.getAI( _Follower );
				var targetAI:AIController = gameData.CurrentScenario.getAI( _Target );
				
				var state:IState = new FollowActorLocomotionState( followerAI.Ship, gameData, followerAI, targetAI.Ship, _X, _Y );
				
				followerAI.setLocomotionState( state );
			}
			
			return true;
		}
	}
}