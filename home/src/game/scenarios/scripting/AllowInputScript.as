package game.scenarios.scripting 
{
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name allowInput
	// @description disables and enabled input
	// @category toggle
	// @isConditional false
	// @param_required value:Boolean
	 
	public class AllowInputScript extends Script
	{
		private var _Allow:Boolean;
		
		public function AllowInputScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Allow = xml.@value != undefined && xml.@value == "true";
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.IsInputEnabled = _Allow;
			
			return true;
		}
	}
}