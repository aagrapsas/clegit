package game.scenarios.scripting 
{
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name fadeToBlack
	// @description fades the screen to or from black
	// @category camera
	// @isConditional false
	// @param_required duration:Number
	// @param_optional direction:String
	public class FadeToBlackScript extends Script
	{		
		private var _Duration:Number;
		private var _Direction:int;
		
		public function FadeToBlackScript()
		{
			
		}	
		
		override public function deserialize( xml:XML ):void
		{
			_Duration = Number( xml.@duration );
			_Direction = xml.@direction != undefined && xml.@direction == "to" ? 1 : -1;
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.Scene.fadeToBlack( _Direction, _Duration );
			
			return true;
		}
	}
}