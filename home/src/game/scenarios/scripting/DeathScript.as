package game.scenarios.scripting 
{
	import engine.interfaces.IDestroyable;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.controllers.AIController;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name death
	// @description waits for a death to occur
	// @category trigger
	// @isConditional true
	// @param_required target:String
	// @param_optional type:String
	public class DeathScript extends Script
	{
		private var _Scenario:Scenario;
		private var _Target:String;
		private var _Type:String;
		
		private var _Ships:Vector.<SpaceShip>;
		
		private var _IsFinished:Boolean = false;
		
		public function DeathScript() 
		{
			_Ships = new Vector.<SpaceShip>;
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@target );
			_Type = xml.@type != undefined ? String( xml.@type ) : null;
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{			
			if ( _Target == "player" )
			{
				if ( !gameData.PlayerShip || gameData.PlayerShip.ArmorHealth.isZero() )
					return true;
			}
			else if ( _Type == "squad" )
			{
				var ais:Vector.<AIController> = gameData.CurrentScenario.getSquadMembers( _Target );
				
				return ( ais && ais.length == 0 );
			}
			else
			{
				return ( gameData.CurrentScenario.getAI( _Target ) == null );
			}
			
			return false;
		}
	}
}