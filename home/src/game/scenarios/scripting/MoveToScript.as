package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import game.ai.MoveToLocomotionState;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name moveTo
	// @description moves the target to x y
	// @category actor
	// @isConditional false
	// @param_required target:String x:int y:int
	// @param_optional stopOnEnemyContact:Boolean
	// @param_optional speed:int
	public class MoveToScript extends Script
	{
		private var _Target:String;
		private var _MoveX:int;
		private var _MoveY:int;
		private var _ShouldStopOnEnemy:Boolean = false;
		private var _Speed:int;
		
		public function MoveToScript() 
		{

		}	
		
		override public function deserialize( xml:XML ):void
		{
			_Target = String( xml.@name );
			_MoveX = int( xml.@x );
			_MoveY = int( xml.@y );
			_ShouldStopOnEnemy = xml.@stopOnEnemyContact != undefined && String( xml.@stopOnEnemyContact ) == "true";
			
			if ( xml.@speed != undefined )
			{
				_Speed = int( xml.@speed );
			}
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			var ai:AIController = gameData.CurrentScenario.getAI( _Target );
			
			if ( ai )
			{
				ai.setLocomotionState( new MoveToLocomotionState( ai.Ship, gameData, ai, _MoveX, _MoveY, _ShouldStopOnEnemy, _Speed ) );
			}
			else
			{
				Debug.errorLog( GameLogChannels.SCRIPT, "MoveTo: AI " + _Target + " doesn't exist" );
			}
			
			return true;
			
		}
	}
}