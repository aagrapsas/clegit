package game.scenarios.scripting 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ScriptTypes 
	{
		public static const MOVE_TO:String = "moveTo";
		public static const TIMER:String = "timer";
		public static const TOUCH:String = "touch";
		public static const LOAD_SCENARIO:String = "loadScenario";
		public static const SPAWN:String = "spawn";
		public static const LOG:String = "log";
		public static const DAMAGE:String = "damage";
		public static const KILL:String = "kill";
		public static const INDESTRUCTABLE:String = "indestructable";
		public static const SHOW_BLINDS:String = "showBlinds";
		public static const SHOW_CAPTION:String = "showCaption";
		public static const ALLOW_INPUT:String = "allowInput";
		public static const PLAY_SOUND:String = "playSound";
		public static const STOP_SOUND:String = "stopSound";
		public static const DEATH:String = "death";
		public static const SET_WAYPOINT:String = "setWaypoint";
		public static const CLEAR_WAYPOINT:String = "clearWaypoint";
		public static const LOAD_PARALLAX_LAYER:String = "loadLayer";
		public static const PLAY_HYPERSPACE:String = "playHyperspace";
		public static const SET_VISIBILITY:String = "setVisibility";
		public static const FADE_TO_BLACK:String = "fadeToBlack";
		public static const PAN_CAMERA:String = "panCamera";
		public static const SET_CONTROLLER:String = "setController";
		public static const PLAY_ANIMATIC:String = "playAnimatic";
		public static const SET_HUD_VISIBILITY:String = "setHUDVisibility";
		public static const ATTACH_ACTOR:String = "attachActor";
		public static const ATTACK_TARGET:String = "attackTarget";
		public static const FOLLOW_ACTOR:String = "followActor";
		public static const CAMERA_SHAKE:String = "cameraShake";
		public static const FLASH:String = "flash";
		public static const LOOP:String = "loop";
		public static const WORLD_FX:String = "worldFX";
		public static const SCORE_EVENT:String = "scoreEvent";
		public static const SHOW_POPUP:String = "showPopup";
		public static const END_GAME:String = "endGame";
		public static const NOTIFY_WAVE:String = "notifyWave";
		public static const SET_SCORING_TYPE:String = "setScoringType";
		public static const SHOW_HELP:String = "showHelp";
	}
}