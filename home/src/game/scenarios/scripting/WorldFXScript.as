package game.scenarios.scripting 
{
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class WorldFXScript extends Script
	{
		private var _Type:String;
		private var _Scale:Number;
		private var _X:int;
		private var _Y:int;
		
		public function WorldFXScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Type = String( xml.@type );
			_X = int( xml.@x );
			_Y = int( xml.@y );
			_Scale = xml.@scale != undefined ? Number( xml.@scale ) : 1;
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			gameData.WorldFX.createFX( _Type, _X, _Y, _Scale );
			
			return true;
		}
	}

}