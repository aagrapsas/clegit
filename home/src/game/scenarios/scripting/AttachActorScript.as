package game.scenarios.scripting 
{
	import engine.debug.Debug;
	import engine.misc.MathUtility;
	import flash.geom.Point;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @name attachActor
	// @description attached one actor to another
	// @category actor
	// @isConditional false
	// @param_required primary:String target:String
	public class AttachActorScript extends Script
	{
		private var _Primary:String;
		private var _Target:String;
		
		public function AttachActorScript() 
		{
			
		}
		
		override public function deserialize( xml:XML ):void
		{
			_Primary = String( xml.@primary );
			_Target = String( xml.@target );
		}
		
		override public function execute( gameData:HomeGameData ):Boolean
		{
			// make primary face target
			// move primary to touch
			// and then attach target to primary and destroy target
			var primary:AIController = gameData.CurrentScenario.getAI( _Primary );
			var target:AIController = gameData.CurrentScenario.getAI( _Target );
			
			if ( !primary || !target )
			{
				Debug.errorLog( GameLogChannels.SCRIPT, "AttachActorScript: unable to attach to one of the specified actors" );
				return true;
			}
			
			const desiredDistance:int = primary.Ship.Node.CollisionComponent.Width / 2 + target.Ship.Node.CollisionComponent.Width / 2;
			const desiredDistanceSquared:int = desiredDistance * desiredDistance;
			
			const deltaX:int = primary.Ship.X - target.Ship.X;
			const deltaY:int = primary.Ship.Y - target.Ship.Y;
			
			const distance:int = deltaX * deltaX + deltaY * deltaY;
			
			target.IsLogicPaused = true;
			primary.IsLogicPaused = true;
			
			var dir:Point = new Point( target.Ship.X - primary.Ship.X, target.Ship.Y - primary.Ship.Y );
			dir.normalize( 1 );
			
			//const angleToTarget:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, _Owner.Dir.x, _Owner.Dir.y ) );
			primary.Ship.faceToward( dir.x, dir.y, target.Ship.Info.Engine.MaxTurnSpeed > 0 ? target.Ship.Info.Engine.MaxTurnSpeed : 2 );
			
			// @TODO: should consider having deltaTime be in the arguement list for script.execute
			
			// perform attachment and be done
			if ( distance <= desiredDistanceSquared )
			{				
				gameData.Scene.removeActor( target.Ship );
				
				//primary.Ship.Display.( target.Ship.Display.getBlitData() );
				
				target.Ship.Display.worldX = primary.Ship.Width / 2 - target.Ship.Width / 2;
				target.Ship.Display.worldY = -target.Ship.Height;
				
				target.destroy();
				
				primary.IsLogicPaused = false;
				
				return true;
			}
			else	// move primary toward target
			{
				primary.Ship.X += primary.Ship.Dir.x * primary.Ship.Info.Engine.MaxAcceleration;
				primary.Ship.Y += primary.Ship.Dir.y * primary.Ship.Info.Engine.MaxAcceleration;
			}
			
			return false;
		}
	}
}