package game.scenarios 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.RandomSeed;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class GameMode implements ITickable, IDestroyable
	{
		public static const PLAYER_TEAM:int = 1;
		public static const ENEMY_TEAM:int = 2;
		
		protected var ModeSeed:RandomSeed;
		
		protected var GameData:HomeGameData;
		protected var DifficultyValue:int = 0;
		
		public function GameMode( gameData:HomeGameData ) 
		{
			GameData = gameData;
		}
		
		public function setup():void
		{
			// Fix cost at start
			DifficultyValue = GameData.ClientData.getPlayerTotalValue();
		}
		
		public function tearDown():void
		{
			
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}

		public function deserialize( xml:XML ):void
		{
			
		}
		
		public function destroy():void
		{
			
		}
	}
}