package game.scenarios 
{
	import engine.Actor;
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneLayer;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.ai.AIEvent;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.gameplay.scoring.CustardScoring;
	import game.HomeGameData;
	import game.scenarios.scripting.Script;
	import game.scenarios.scripting.ScriptFactory;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Scenario implements ITickable, IDestroyable
	{
		private var _ScenarioName:String;
		private var _GameData:HomeGameData;
		private var _AIControllerMap:Dictionary;
		private var _EnemyAIControllers:Vector.<AIController>;
		private var _AIControllers:Vector.<AIController>;
		
		private var _TeamControllers:Dictionary;	// controllers based on what team they're on
		
		private var _ActiveScripts:Vector.<Script>;
		
		private var _Squads:Dictionary;
		
		private var _IsMarkedForDestroy:Boolean = false;
		
		private var _Scoring:CustardScoring;
		
		public function Scenario( gameData:HomeGameData ) 
		{
			_AIControllerMap = new Dictionary();
			_EnemyAIControllers = new Vector.<AIController>();
			_TeamControllers = new Dictionary();
			_AIControllers = new Vector.<AIController>();
			
			_Squads = new Dictionary();
			_ActiveScripts = new Vector.<Script>();

			_GameData = gameData;
			
			_GameData.IsInputEnabled = true;
		}
		
		public function deserialize( xml:XML ):void
		{
			if ( xml.@name == undefined )
			{
				Debug.errorLog( GameLogChannels.GAMEMODE, "could not find start level. Check game_config.xml!" );
				return;
			}
			
			_ScenarioName = xml.@name;
			
			Debug.debugLog( GameLogChannels.GAMEMODE, "Deserializing level " + _ScenarioName );
			
			for each ( var subXML:XML in xml.* )
			{
				var script:Script = ScriptFactory.getScript( String( subXML.localName() ) );
				
				if ( !script )
				{
					Debug.errorLog( GameLogChannels.SCRIPT, "Invalid script name " + subXML.localName() );
					continue;
				}

				script.deserializeChildren( subXML );
				script.deserialize( subXML );
				
				_ActiveScripts.push( script );
			}
		}
		
		public function setup():void
		{
			_GameData.Scene.addTickable( this );
			
			_Scoring = new CustardScoring( _GameData );
		}
		
		public function tick( deltaTime:Number ):void
		{		
			var toRemove:Vector.<Script> = new Vector.<Script>();
			var toAdd:Vector.<Script> = new Vector.<Script>();
			
			// Update scoring system
			_Scoring.tick( deltaTime );
			
			for ( var i:int = 0; i < _ActiveScripts.length; i++ )
			{				
				if ( _GameData.Scene.IsPaused )	// A script may trigger pause
				{
					break;
				}
				
				var activeScript:Script = _ActiveScripts[ i ];
				
				if ( activeScript.isValid( _GameData ) == false || activeScript.execute( _GameData ) )
				{			
					// This can happen from script execution!
					if ( _IsMarkedForDestroy )
					{
						return;
					}
					
					for each ( var script:Script in _ActiveScripts[ i ].Children )
					{
						toAdd.push( script );
					}
					
					toRemove.push( activeScript );
				}
			}
			
			// Remove the stale
			for each ( var remove:Script in toRemove )
			{
				var index:int = _ActiveScripts.indexOf( remove );
				
				if ( index > -1 )
				{
					_ActiveScripts.splice( index, 1 );
				}
			}
			
			// Add the new
			for each ( var add:Script in toAdd )
			{
				_ActiveScripts.push( add );
			}
			
			var runningScripts:String = "\n";
			
			for each ( var runningScript:Script in _ActiveScripts )
			{
				runningScripts += getQualifiedClassName( runningScript ) + "\n";
			}
			
			_GameData.DebugSystem.PerfHUD.setValue( "Scenario", _ScenarioName );
			//_GameData.DebugSystem.PerfHUD.setValue( "Scripts", runningScripts );
		}
		
		public function sortAI():void
		{
			if ( _GameData.PlayerShip )
				sortAIByDistance();
		}
		
		private function sortAIByDistance():void
		{
			_EnemyAIControllers.sort( compare );
		}
		
		private function compare( x:AIController, y:AIController ):Number
		{
			const xDeltaX:int = _GameData.PlayerShip.X - x.Ship.X;
			const xDeltaY:int = _GameData.PlayerShip.Y - x.Ship.Y;
			const yDeltaX:int = _GameData.PlayerShip.X - y.Ship.X;
			const yDeltaY:int = _GameData.PlayerShip.Y - y.Ship.Y;
			
			const distToX:int = xDeltaX * xDeltaX + xDeltaY * xDeltaY;
			const distToY:int = yDeltaX * yDeltaX + yDeltaY * yDeltaY;
			
			if ( distToX < distToY )
				return -1;
			
			if ( distToY < distToX )
				return 1;
			
			return 0;
		}
		
		public function breakDown():void
		{
			_GameData.Scene.removeTickable( this );
		}
		
		public function getAI( name:String ):AIController
		{
			return _AIControllerMap[ name ];
		}
		
		public function getAIByShip( ship:SpaceShip ):AIController
		{
			for each ( var ai:AIController in _AIControllerMap )
			{
				if ( ai.Ship == ship )
				{
					return ai;
				}
			}
			
			return null;
		}
		
		// Going to need to listen for death event
		public function registerAI( name:String, AI:AIController ):void
		{
			//trace( "Registering AI: " + name );
			
			if ( _AIControllerMap[ name ] )
			{
				Debug.errorLog( GameLogChannels.SCRIPT, "AI was registered with duplicate name! " + name + " already exists!" );
			}
			
			_AIControllerMap[ name ] = AI;
			
			AI.Name = name;
			
			// OLD --> @TODO: Make this less hacky -- have an official "player" and "enemy" team in the xml!
			// @TODO make this use _TeamControllers
			const isOnEnemyTeam:Boolean = _GameData.PlayerShip && _GameData.PlayerShip.Team != AI.Ship.Team;
			const isOnTempEnemyTeam:Boolean = AI.Ship.Team != 1;
			
			if ( isOnEnemyTeam || isOnTempEnemyTeam )
				_EnemyAIControllers.push( AI );
			
			AI.addEventListener( AIEvent.AI_DEATH, onShipDied );
			
			_AIControllers.push( AI );
			
			/*
			if ( _TeamControllers[ AI.Ship.Team ] == null )
			{
				_TeamControllers[ AI.Ship.Team ] = new Vector.<AIController>();
			}
			
			_TeamControllers[ AI.Ship.Team ].push( AI );
			*/
		}
		
		public function removeAI( name:String ):void
		{
			var ai:AIController = _AIControllerMap[ name ];
			
			//trace( "Removing AI: " + name );
			
			if ( ai )
			{
				ai.removeEventListener( AIEvent.AI_DEATH, onShipDied );
				
				if ( ai.Squad != null && ai.Squad != "" )
				{
					ai.removeEventListener( AIEvent.AI_DEATH, onSquadMemberDeath );
					
					const squadIndex:int = _Squads[ ai.Squad ].indexOf( ai );
					
					if ( squadIndex > -1 )
					{
						_Squads[ ai.Squad ].splice( squadIndex, 1 );
					}
				}
				
				var index:int = _EnemyAIControllers.indexOf( ai );
				
				if ( index > -1 )
				{
					_EnemyAIControllers.splice( index, 1 );
				}
				
				var mainIndex:int = _AIControllers.indexOf( ai );
				
				if ( mainIndex > -1 )
				{
					_AIControllers.splice( mainIndex, 1 );
				}
				
				// @TODO make this safer!
				// Remove from team controller list
				// _TeamControllers[ ai.Ship.Team ].splice( _TeamControllers[ ai.Ship.Team ].indexOf( ai ), 1 );
				
				ai.destroy();
				
				delete _AIControllerMap[ name ];
			}
			else
			{
				Debug.errorLog( GameLogChannels.GAMEMODE, "Scenario ERROR: unable to remove AI controller " + name + " from scene. Controller does not exist." );
			}
		}
		
		public function registerSquadMember( squad:String, AI:AIController ):void
		{
			if ( !_Squads[ squad ] )
				_Squads[ squad ] = new Vector.<AIController>;
			
			_Squads[ squad ].push( AI );
			
			AI.Squad = squad;
			
			AI.addEventListener( AIEvent.AI_DEATH, onSquadMemberDeath, false, 0, true );
		}
		
		private function onShipDied( e:AIEvent ):void
		{
			var owner:AIController = e.Owner;
			
			owner.removeEventListener( AIEvent.AI_DEATH, onShipDied );
			
			var ownerKey:String;
			
			for ( var key:String in _AIControllerMap )
			{
				if ( _AIControllerMap[ key ] == owner )
				{
					ownerKey = key;
					break;
				}
			}
			
			//trace( "Removing AI from death: " + ownerKey );
			
			var index:int = _EnemyAIControllers.indexOf( owner );
			
			if ( index > -1 )
			{
				_EnemyAIControllers.splice( index, 1 );
			}
			
			var mainIndex:int = _AIControllers.indexOf( owner );
			
			if ( mainIndex > -1 )
			{
				_AIControllers.splice( mainIndex, 1 );
			}
			
			// @TODO make this safer
			// _TeamControllers[ owner.Ship.Team ].splice( _TeamControllers[ owner.Ship.Team ].indexOf( owner ), 1 );
			
			if ( ownerKey != null )
				delete _AIControllerMap[ ownerKey ];
		}
		
		private function onSquadMemberDeath( e:AIEvent ):void
		{
			var owner:AIController = e.Owner;
			
			owner.removeEventListener( AIEvent.AI_DEATH, onSquadMemberDeath );
			
			var index:int = _Squads[ owner.Squad ].indexOf( owner );
			
			if ( index > -1 )
			{
				_Squads[ owner.Squad ].splice( index, 1 );
			}
		}
		
		public function getSquadMembers( squad:String):Vector.<AIController> { return _Squads[ squad ]; }
		
		public function get EnemyAIControllers():Vector.<AIController> { return _EnemyAIControllers; }
		
		private function clearScenario():void
		{
			// Clear all AI data
			for each ( var ai:AIController in _AIControllerMap )
			{
				if ( !ai )
					continue;
					
				ai.removeEventListener( AIEvent.AI_DEATH, onShipDied );
				
				_GameData.Scene.removeActor( ai.Ship );
				ai.Ship.destroy();
				
				_GameData.Scene.removeTickable( ai );
				ai.destroy();
			}
			
			for each ( var squad:Vector.<AIController> in _Squads )
			{
				for each ( var squadMember:AIController in squad )
				{
					if ( !squadMember )
						continue;
						
					squadMember.removeEventListener( AIEvent.AI_DEATH, onSquadMemberDeath );
				}
			}
			
			if ( _AIControllers.length > 0 )
			{
				_AIControllers.splice( 0, _AIControllers.length );
			}
			
			// _TeamControllers = new Dictionary();	// drop all references
			
			var aiToDestroy:Vector.<AIController> = new Vector.<AIController>();

			for each ( var toDestroy:AIController in aiToDestroy )
			{
				toDestroy.destroy();
			}
			
			// Clear player data
			if ( _GameData.PlayerController )
			{
				_GameData.Scene.removeTickable( _GameData.PlayerController )
				_GameData.PlayerController.destroy();
				_GameData.PlayerController = null;
			}
			
			if ( _GameData.PlayerShip )
			{
				_GameData.Scene.removeActor( _GameData.PlayerShip );
				_GameData.PlayerShip.destroy();
				_GameData.PlayerShip = null;
			}
			
			if ( _GameData.PlayerWaypoint )
			{
				_GameData.Scene.removeActor( _GameData.PlayerWaypoint );
				_GameData.Scene.removeTickable( _GameData.PlayerWaypoint );
				_GameData.PlayerWaypoint.destroy();
				_GameData.PlayerWaypoint = null;
			}
			
			if ( _GameData.PlayerWaypointDir )
			{
				_GameData.Scene.removeActor( _GameData.PlayerWaypointDir );
				_GameData.Scene.removeTickable( _GameData.PlayerWaypointDir );
				_GameData.PlayerWaypointDir.destroy();
				_GameData.PlayerWaypointDir = null;
			}
			
			_GameData.Scene.notifyNewScene();
			_GameData.WorldFX.releaseAll();
			_GameData.Pools.forceFreeAll();
			
			/*
			for each ( var possibleAI:AIController in _GameData.Scene.Tickables )
			{
				if ( !possibleAI )
					continue;
					
				aiToDestroy.push( possibleAI );
			}
			*/
			// _GameData.Scene.clearActors();
			
			// Memory clean up & debug warning
			for each ( var actor:Actor in _GameData.Scene.Actors )
			{
				Debug.errorLog( GameLogChannels.ERROR, "FOUND LIVING ACTOR! of class " + getQualifiedClassName( actor ) );
				
				// actor.destroy();
			}
			
			_GameData.SoundSystem.stopAllSounds();
			
			_GameData.Scene.clearLayers();
			
			_GameData.ActiveCamera.IsInitialized = false;
		}
		
		public function get isMarkedForDestroy():Boolean { return _IsMarkedForDestroy; }
		
		public function get name():String { return _ScenarioName; }
		
		public function get scoringSystem():CustardScoring { return _Scoring; }
		
		public function get teamAIDictionary():Dictionary { return _TeamControllers; }
		
		public function get aiControllers():Vector.<AIController> { return _AIControllers; }
		
		public function destroy():void
		{
			_IsMarkedForDestroy = true;
			
			_Scoring.destroy();
			
			clearScenario();
			_GameData = null;
			_EnemyAIControllers = null;
			_AIControllerMap = null;
			_ActiveScripts = null;
			_AIControllers = null;
			_Squads = null;
			_TeamControllers = null;
		}
	}
}