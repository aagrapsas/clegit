package game.debug 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class GameLogChannels
	{
		public static const ACTOR:String = "actor";
		public static const GAMEMODE:String = "gamemode";
		public static const SCRIPT:String = "script";
		public static const ERROR:String = "error";
		public static const WARNING:String = "warning";
		public static const DEBUG:String = "debug";
		public static const DATA:String = "data";
	}
}