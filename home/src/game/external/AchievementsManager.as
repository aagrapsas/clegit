package game.external 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AchievementsManager 
	{
		public static const ENEMY_KILL:String = "enemy kills";
		public static const REINFORCEMENTS:String = "reinforcements called";
		public static const WAVES_SURVIVED:String = "waves survived";
		public static const LONGEST_SURVIVAL:String = "longest survival";
		
		public static function report( type:String, value:int ):void
		{
			Config::kong
			{
				if ( Kongregate.api )
				{
					Kongregate.api.stats.submit( type, value );
				}
			}
		}
	}
}