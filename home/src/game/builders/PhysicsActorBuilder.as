package game.builders 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.render.scene.SceneNode;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import game.actors.PhysicsActor;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PhysicsActorBuilder 
	{		
		private var _GameData:HomeGameData;
		
		private var _ActorData:Dictionary;
		private var _CollectionData:Dictionary;
		
		public function PhysicsActorBuilder( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			initialize();
		}	
		
		private function initialize():void
		{
			_ActorData = new Dictionary;
			_CollectionData = new Dictionary;
			
			const xml:XML = _GameData.AssetManager.getXML( "physics_actors" );
			
			for each ( var actorXML:XML in xml.* )
			{
				const asset:String = String( actorXML.@asset );
				const transfer:Number = Number( actorXML.@velocityTransfer );
				const deceleration:Number = Number( actorXML.@deceleration );
				const name:String = String( actorXML.@name );
				
				_ActorData[ name ] = new PhysicsActorData( transfer, deceleration, asset );
			}
			
			const collectionsXML:XML = _GameData.AssetManager.getXML( "physics_actor_collection" );
			
			for each ( var collectionXML:XML in collectionsXML.* )
			{
				const collection:String = String( collectionXML.@name );
				var data:Vector.<CollectionData> = new Vector.<CollectionData>;
				
				for each ( var pointXML:XML in collectionXML.* )
				{
					const x:int = int( pointXML.@x );
					const y:int = int( pointXML.@y );
					const collectionAsset:String = String( pointXML.@asset );
					
					data.push( new CollectionData( collectionAsset, new Point( x, y ) ) );
				}
				
				_CollectionData[ collection ] = data;
			}
		}
		
		public function createPhysicsActor( name:String, x:int, y:int ):PhysicsActor
		{
			var data:PhysicsActorData = _ActorData[ name ];
			
			if ( !data )
				return null;
				
			var actor:PhysicsActor = new PhysicsActor;
			actor.VelocityTransfer = data.VelocityTransfer;
			actor.Deceleration = data.Deceleration;
			
			var bitmapData:Bitmap = _GameData.AssetManager.getImage( data.Asset ) as Bitmap;
			actor.setDrawingData( bitmapData.bitmapData.clone(), _GameData.GameConfigData[ "subpixel_support" ] );
			
			actor.Node = new SceneNode( new RectCollider( 0, 0, actor.Display.width, actor.Display.height, CollisionFlags.ACTORS ) );
			
			_GameData.PhysActorManager.addActor( actor );
			
			actor.Data = _GameData;
			
			actor.X = x;
			actor.Y = y;
			
			actor.initialize();
			
			return actor;
		}
		
		public function createCollection( name:String, x:int, y:int ):Vector.<PhysicsActor>
		{
			var collection:Vector.<PhysicsActor> = new Vector.<PhysicsActor>;
			var collectionDatas:Vector.<CollectionData> = _CollectionData[ name ];
			
			if ( !collectionDatas )
				return null;
				
			for each ( var collectionData:CollectionData in collectionDatas )
			{
				var actor:PhysicsActor = createPhysicsActor( collectionData.PhysicsActorType, x + collectionData.Location.x, y + collectionData.Location.y );
				
				collection.push( actor );
			}
			
			return collection;
		}
	}
}
import flash.geom.Point;

class CollectionData
{
	public var Location:Point;
	public var PhysicsActorType:String;
	
	public function CollectionData( type:String, location:Point )
	{
		PhysicsActorType = type;
		Location = location;
	}
}

class PhysicsActorData
{
	public var VelocityTransfer:Number;
	public var Deceleration:Number;
	public var Asset:String;
	
	public function PhysicsActorData( velocityTransfer:Number, deceleration:Number, asset:String )
	{
		VelocityTransfer = velocityTransfer;
		Deceleration = deceleration;
		Asset = asset;
	}
}