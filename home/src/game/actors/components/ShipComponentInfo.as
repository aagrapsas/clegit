package game.actors.components 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipComponentInfo 
	{
		// @TODO: this should all be replaced with public getters and protected setters and default implementations
		// For now, I just want to get it done.
		
		/**
		 * General name for displaying this component
		 */
		public var Name:String;
		
		/**
		 * Unique identifier for this component
		 */
		public var Key:String;
		
		/**
		 * Type of component
		 */
		public var Type:String;
		
		public var Audio:Dictionary;
		
		public var RawXML:XML;
		
		public var Tonnage : int;
		
		public function ShipComponentInfo() 
		{
			Audio = new Dictionary;
		}
		
		public var SoundSet:String;
		
		/**
		 * Deserialize xml describing a component
		 * @param	xml
		 */
		public function deserialize( xml:XML ):void
		{
			RawXML = xml;
			Name = String( xml.@name );
			Key = String( xml.@key );
			SoundSet = String( xml.@soundSet );
			Tonnage = int( xml.@tonnage );
		}
	}
}