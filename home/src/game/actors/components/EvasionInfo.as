package game.actors.components 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class EvasionInfo extends ShipComponentInfo
	{
		public var velocityMultiplier:Number;
		public var stopDampener:Number;
		public var deceleration:Number;
		public var cooldown:Number;
		
		public function EvasionInfo() 
		{
			this.Type = ShipComponentTypes.EVASION_COMPONENT;
		}
		
		override public function deserialize( xml:XML ):void
		{
			velocityMultiplier = Number( xml.@velocityMultiplier );
			stopDampener = Number( xml.@stopDampener );
			deceleration = Number( xml.@deceleration );
			cooldown = Number( xml.@cooldown );
			
			super.deserialize( xml );
		}
	}
}