package game.actors.components 
{
	import game.actors.components.ShipComponentInfo;
	import game.actors.components.ShipComponentTypes;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CombatSystemInfo extends ShipComponentInfo
	{
		/*
		public var Deviation:Number;	// how far the bullets spread
		public var Accuracy:Number;	// how often the shots find their true trajectory
		public var Speed:Number; // how fast it moves
		*/
		public var FireDelay:Number;	// how fast the weapon fires
		public var Damage:Number;	// how much damage a direct hit does
		public var SystemType:String;	// type of system, includes instant, missile, projectile, etc.
		/*
		public var Particle:String;
		*/
		public var FireAngle:Number;
		/*
		public var LifeTime:Number;
		public var Width:Number;
		public var Height:Number;
		*/
		public var Trigger:String;
		/* public var TurnSpeed:Number; */
		public var Icon:String;
		
		public var Range:int;
		public var RangeSquared:int;
		
		public var DoesOverheat:Boolean;
		
		public var BurstDuration:Number;
		public var BurstCooldown:Number;
		
		public var IsTurret:Boolean = false;
		
		public var DPS:Number = 0;
		
		public function CombatSystemInfo()
		{
			this.Type = ShipComponentTypes.COMBAT_SYSTEM;
		}
		
		override public function deserialize( xml:XML ):void
		{
			super.deserialize( xml );
			
			this.SystemType = String( xml.@type );
			this.Icon = String( xml.@icon );
			this.FireAngle = Number( xml.@fireAngle );
			this.FireDelay = Number( xml.@fireDelay );
			this.Range = int( xml.@range );
			this.RangeSquared = this.Range * this.Range;
			this.Damage = Number( xml.@damage );
			this.Trigger = String( xml.@trigger );
			this.BurstCooldown = Number( xml.@burstCooldown );
			this.BurstDuration = Number( xml.@burstDuration );

			if ( BurstDuration == 0 )
			{
				const perBurst:Number = 1 / FireDelay;
				
				DPS = perBurst * Damage;
			}
			else
			{
				const shotsInBurst:Number = BurstDuration / FireDelay;
				const totalBurstDuration:Number = BurstCooldown + BurstDuration;
				const burstsInSecond:Number = 1 / totalBurstDuration;
				const shotsInSecond:Number = shotsInBurst * burstsInSecond;
				DPS = shotsInSecond * Damage;
			}
			
			if ( xml.@isTurret != undefined && String( xml.@isTurret ) == "true" )
				this.IsTurret = true;
		}
	}
}