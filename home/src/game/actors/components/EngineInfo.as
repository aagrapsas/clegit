package game.actors.components 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class EngineInfo extends ShipComponentInfo
	{
		public var MaxSpeed:Number;
		public var MaxAcceleration:Number;
		public var AccelerationStep:Number;
		public var Deceleration:Number;
		public var TurnAcceleration:Number;
		public var MaxTurnSpeed:Number;
		public var CompatibleChassis:Vector.<String>;
		
		public function EngineInfo() 
		{
			this.Type = ShipComponentTypes.ENGINE_COMPONENT;
		}
		
		override public function deserialize( xml:XML ):void 
		{
			// Engine values
			MaxSpeed = Number( xml.@maxSpeed );
			MaxAcceleration = Number( xml.@maxAcceleration );
			AccelerationStep = Number( xml.@accelerationStep );
			Deceleration = Number( xml.@deceleration );
			TurnAcceleration = Number( xml.@turnAcceleration );
			MaxTurnSpeed = Number( xml.@maxTurnSpeed );
			
			// Chassis
			
			// Super handles name & key
			super.deserialize( xml );
		}
	}
}