package game.actors.components 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PlatformInfo extends ShipComponentInfo
	{
		public var BuildTime:Number;
		public var Upgrades:Vector.<String>;
		public var ShipKey:String;
		
		public function PlatformInfo() 
		{
			Upgrades = new Vector.<String>();
		}
		
		override public function deserialize( xml:XML ):void
		{
			this.BuildTime = xml.@buildTime;
			this.ShipKey = xml.@shipKey;
			
			for each ( var xmlAttribute:XML in xml.attributes() )
			{
				const attName:String = xmlAttribute.name();	// option0
				const className:String = attName.substr( 0, 6 );	// option
				const numeric:int = int( attName.substr( 6 ) );	// 0
				
				if ( className == "option" )
				{
					Upgrades.push(  String( xmlAttribute ) );
				}
			}
			
			super.deserialize( xml );
		}
	}
}