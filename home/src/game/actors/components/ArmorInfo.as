package game.actors.components 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ArmorInfo extends ShipComponentInfo
	{
		public var FullHitPoints:uint;
		public var ArmorRegenPerSecond:Number;
		public var ArmorRegenDelay:Number;
		
		public function ArmorInfo() 
		{
			this.Type = ShipComponentTypes.ARMOR_COMPONENT;
		}
		
		override public function deserialize( xml:XML ):void 
		{
			FullHitPoints = Number( xml.@fullHitPoints );
			ArmorRegenDelay = Number( xml.@armorRegenDelay );
			ArmorRegenPerSecond = Number( xml.@armorRegenPerSecond );
			
			super.deserialize( xml );
		}
	}
}