package game.actors.components 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SensorInfo extends ShipComponentInfo
	{
		public var MaxRange:uint;
		
		public function SensorInfo() 
		{
			this.Type = ShipComponentTypes.SENSOR_COMPONENT;
		}
		
		override public function deserialize(xml:XML):void 
		{
			MaxRange = uint( xml.@maxRange );
			
			super.deserialize( xml );
		}
	}
}