package game.actors 
{
	import engine.collision.CollisionFlags;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneNode;
	import game.builders.PhysicsActorBuilder;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PhysicsActorManager implements ITickable, IDestroyable
	{
		private var _GameData:HomeGameData;
		
		private var _PhysicsActors:Vector.<PhysicsActor>;
		
		public var Spawner:PhysicsActorBuilder;
		
		public function PhysicsActorManager( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			_PhysicsActors = new Vector.<PhysicsActor>;
			
			_GameData.Scene.addTickable( this );
		}
		
		public function addActor( actor:PhysicsActor ):void
		{
			_PhysicsActors.push( actor );
			_GameData.Scene.addActor( actor );
		}
		
		public function removeActor( actor:PhysicsActor ):void
		{
			const index:int = _PhysicsActors.indexOf( actor );
			
			if ( index > -1 )
			{
				_GameData.Scene.removeActor( _PhysicsActors[ index ] );
				_PhysicsActors[ index ].destroy();
				_PhysicsActors.splice( index, 1 );
			}
		}
	
		public function tick( deltaTime:Number ):void
		{
			if ( !_GameData.PlayerShip )
				return;
			
			const isFullScreen:Boolean = _GameData.Scene.IsFullScreen;
			const width:int = isFullScreen ? _GameData.Scene.Display.stage.fullScreenWidth : _GameData.Scene.Display.stage.stageWidth;
			const height:int = isFullScreen ? _GameData.Scene.Display.stage.fullScreenHeight : _GameData.Scene.Display.stage.stageHeight;
			
			const left:int = _GameData.PlayerShip.X - width / 2;
			const right:int = _GameData.PlayerShip.X + width / 2;
			const top:int = _GameData.PlayerShip.Y + height / 2;
			const bottom:int = _GameData.PlayerShip.Y - height / 2;
			
			var nodes:Vector.<SceneNode> = _GameData.Scene.rectCheck( left, top, width, height, CollisionFlags.ALL );
			
			for each ( var node:SceneNode in nodes )
			{
				var actor:PhysicsActor = node.InternalObject as PhysicsActor;
				
				if ( actor )
				{
					actor.tick( deltaTime );
				}
			}
		}
		
		public function clearData():void
		{
			for each ( var actor:PhysicsActor in _PhysicsActors )
			{
				_GameData.Scene.removeActor( actor );
				actor.destroy();
			}
		}
		
		public function destroy():void
		{
			clearData();
			
			_PhysicsActors = null;
			_GameData.Scene.removeTickable( this );
			_GameData = null;
		}
	}
}