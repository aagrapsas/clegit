package game.actors 
{
	import engine.Actor;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneNode;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PhysicsActor extends Actor implements ITickable
	{
		public var VelocityTransfer:Number = 0;
		public var VelocityX:Number = 0;
		public var VelocityY:Number = 0;
		public var Deceleration:Number = 0;
		
		public function PhysicsActor() 
		{
			
		}
		
		public function tick( deltaTime:Number ):void
		{						
			const colliders:Vector.<SceneNode> = Data.Scene.collisionCheck( this.Node );
			
			for each ( var collider:SceneNode in colliders )
			{
				const spaceShip:SpaceShip = collider.InternalObject as SpaceShip;
				var speed:Number;
				var dir:Point;
				
				trace( "COLLIDING!" );
				
				if ( spaceShip )
				{
					speed = Math.sqrt( spaceShip.VelocityX * spaceShip.VelocityX + spaceShip.VelocityY * spaceShip.VelocityY ) * VelocityTransfer;
					dir = new Point( this.X - spaceShip.X, this.Y - spaceShip.Y );
					dir.normalize( 1 );
				}
				else
				{
					const physicsActor:PhysicsActor = collider.InternalObject as PhysicsActor;
					
					if ( physicsActor )
					{
						speed = Math.sqrt( physicsActor.VelocityX * physicsActor.VelocityX + physicsActor.VelocityY * physicsActor.VelocityY ) * VelocityTransfer;
						dir = new Point( this.X - physicsActor.X, this.Y - physicsActor.Y );
						dir.normalize( 1 );
					}
				}
				
				if ( speed > 0 )
				{
					trace( "Speed: " + speed );
					VelocityX += dir.x * speed;
					VelocityY += dir.y * speed;
				}
			}
			
			const deceleration:Number = Deceleration * deltaTime;
			
			this.X = this.X + VelocityX * deltaTime;
			this.Y = this.Y + VelocityY * deltaTime;
			
			VelocityX = VelocityX > 0 ? Math.max( VelocityX - deceleration, 0 ) : Math.min( VelocityX + deceleration, 0 );
			VelocityY = VelocityY > 0 ? Math.max( VelocityY - deceleration, 0 ) : Math.min( VelocityY + deceleration, 0 );
		}
		
		override public function destroy():void
		{			
			super.destroy();
		}
	}
}