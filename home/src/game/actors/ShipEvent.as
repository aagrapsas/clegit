package game.actors 
{
	import flash.events.Event;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipEvent extends Event
	{
		public var Owner:SpaceShip;
		public var Instigator:SpaceShip;
		
		public function ShipEvent( type:String, owner:SpaceShip, instigator:SpaceShip ) 
		{
			Owner = owner;
			Instigator = instigator;
			
			super( type );
		}
	}
}