package game.data 
{
	import engine.debug.Debug;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.actors.SpaceShipInfo;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PlayerData 
	{
		private var _Salvage:int = 0;
		private var _FBC:int = 0;
		private var _Level:int = 0;
		private var _Ships:Vector.<SpaceShipInfo>;
		
		private var _GameData:HomeGameData;
		
		private var _ActiveShip:SpaceShipInfo;
		
		private static const LOCAL_SAVE_DATA:String = "local_ships";
		
		public function PlayerData( gameData:HomeGameData ) 
		{
			// Assignment
			_GameData = gameData;
			
			// Allocation
			_Ships = new Vector.<SpaceShipInfo>();
			
			initialize();
		}
		
		private function initialize():void
		{
			_GameData.GameEvents.register( ShipEventTypes.SHIP_DESTROYED, handleShipDeath );
			
			setupTestShips();
		}
		
		// @HACK the below is just for demo purposes until we hook up
		// to an actual backend.
		private function setupTestShips():void
		{
			var local:Object = _GameData.LocalDatabase.readField( LOCAL_SAVE_DATA );
			
			if ( !local )
			{
				var xml:XML = _GameData.AssetManager.getXML( "game_config" );
				
				for each ( var shipXML:XML in xml.start_ships.start_ship )
				{
					var key:String = String( shipXML.@key );
					
					var info:SpaceShipInfo = _GameData.ShipCompCatalog.getShipTemplate( key );
					
					_Ships.push( info );
				}
			}
			else
			{
				_Ships = local as Vector.<SpaceShipInfo>;
			}
		}
		
		public function saveData():void
		{
			// _GameData.LocalDatabase.writeField( LOCAL_SAVE_DATA, _Ships );
		}
		
		private function handleShipDeath( data:Object ):void
		{
			if ( !_GameData.PlayerShip )
			{
				return;
			}
			
			var destroyed:SpaceShip = data.destroyed;
			var destroyer:SpaceShip = data.destroyer;
			
			if ( destroyer != _GameData.PlayerShip )
			{
				return;
			}
			
			var salvagePercent:Number = _GameData.GameConfigData[ "kill_salvage_perc" ];
			var cost:int = _GameData.InfoCatalog.getShipCost( destroyed ) * salvagePercent;
			
			if ( cost > 0 )
			{
				_Salvage += cost;
			}
		}
		
		public function deserialize( data:String ):void
		{
			if ( !data )
			{
				Debug.errorLog( GameLogChannels.DATA, "Attempted to process null data for player!" );
				return;
			}
			
			var json:Object = JSON.parse( data );
			
			if ( json == null )
			{
				Debug.errorLog( GameLogChannels.DATA, "Attempted to process bad data for player!" );
				return;
			}
			
			// Break down json into appropriate data
		}
		
		public function getPlayerTotalValue():int
		{
			var shipValues:int = 0;
			
			for each ( var ship:SpaceShipInfo in _Ships )
			{
				shipValues += _GameData.InfoCatalog.getShipInfoCost( ship );
			}
			
			const totalCost:int = shipValues + _Salvage;
			
			return totalCost;
		}
		
		public function get salvage():int { return _Salvage; }
		public function set salvage( value:int ):void { _Salvage = value; }
		
		public function get fbc():int { return _FBC; }
		public function set fbc( value:int ):void { _FBC = value; }
		
		public function get playerLevel():int { return _Level; }
		
		public function get ships():Vector.<SpaceShipInfo> { return _Ships; }
		
		public function get activeShip():SpaceShipInfo { return _ActiveShip; }
		public function set activeShip( value:SpaceShipInfo ):void { _ActiveShip = value; }
	}
}