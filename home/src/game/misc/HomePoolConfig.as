package game.misc 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.interfaces.IGameData;
	import engine.misc.IPoolConfig;
	import engine.misc.PoolManager;
	import engine.render.animation.Animation;
	import engine.render.scene.SceneNode;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.utils.Dictionary;
	import game.fx.WorldNumber;
	import game.gameplay.weapons.projectiles.Projectile;
	import game.HomeGameData;
	import game.ui.world.UIShipInfo;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class HomePoolConfig implements IPoolConfig
	{
		private var _GameData:IGameData;
		
		public static const FX_POOL_80MM_FIRING:String = "80mm_firing";
		
		public static const WORLD_NUMBER:String = "world_number";
		public static const WORLD_SHIP_INFO:String = "world_ship_info";
		
		public function HomePoolConfig() 
		{
			
		}
		
		public function execute( gameData:IGameData, poolManager:PoolManager ):void
		{
			var fxXML:XML = gameData.AssetManager.getXML( "fx_pools" );
			
			_GameData = gameData;	// somewhat round about way to get gamedata
			
			for each ( var animXML:XML in fxXML.anim_pool )
			{
				const asset:String = animXML.@asset;
				const size:int = int( animXML.@size );
				const cap:int = int( animXML.@cap );
				
				poolManager.createPool( createAnimation, asset, size, cap );
			}
			
			for each ( var projXML:XML in fxXML.proj_pool )
			{
				const projType:String = projXML.@asset;
				const projPoolSize:int = int( projXML.@size );
				const projCap:int = int( projXML.@cap );
				
				poolManager.createPool( createProjectile, projType, projPoolSize, projCap );
			}
			
			/*
			for each ( var swfXML:XML in fxXML.swf_pool )
			{
				const swfType:String = swfXML.@asset;
				const swfPoolSize:int = int( swfXML.@size );
				
				poolManager.createPool( createSwf, swfType, swfPoolSize );
			}
			*/
			
			for each ( var numXML:XML in fxXML.num_pool )
			{
				const numName:String = numXML.@name;
				const numPoolSize:int = int( numXML.@size );
				const numCap:int = int( numXML.@cap );
				
				poolManager.createPool( createWorldNumber, numName, numPoolSize, numCap );
			}
			
			const shipInfoPoolSize:int = fxXML.ship_info_pool.@size;
			const shipInfoCap:int = fxXML.ship_info_pool.@cap;
			
			poolManager.createPool( createWorldShipInfo, WORLD_SHIP_INFO, shipInfoPoolSize, shipInfoCap );
		}
		
		private function createAnimation( type:String ):Animation
		{
			var animation:Animation = _GameData.AnimationSystem.getAnimation( type );
			
			return animation;
		}
		
		private function createProjectile( type:String ):Projectile
		{
			var projectile:Projectile = new Projectile();
			
			const memDisplay:Bitmap = _GameData.AssetManager.getImage( type ) as Bitmap;
			projectile.setDrawingData( memDisplay.bitmapData.clone(), (_GameData as HomeGameData).GameConfigData[ "subpixel_support" ] );
			projectile.Node = new SceneNode( new RectCollider( 0, 0, projectile.Display.width, projectile.Display.height, CollisionFlags.ACTORS ) );
			projectile.Data = _GameData;
			
			return projectile;
		}
		
		/*
		private function createSwf( name:String ):DisplayObject
		{
			var type:Class = _GameData.AssetManager.swfManager.getClass( name );
			
			var obj:DisplayObject = new type();
			
			return obj;
		}
		*/
		
		private function createWorldNumber( type:String ):WorldNumber
		{
			var number:WorldNumber = new WorldNumber();
			number.data = _GameData as HomeGameData;
			
			return number;
		}
		
		private function createWorldShipInfo( type:String ):UIShipInfo
		{
			var info:UIShipInfo = new UIShipInfo( _GameData as HomeGameData );
			
			return info;
		}
	}
}