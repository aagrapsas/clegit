package game.controllers 
{
	import engine.interfaces.IGameData;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ControllerFactory 
	{
		public static function getController( type:String, ship:SpaceShip, gameData:IGameData ):IController
		{
			switch ( type )
			{
				case ControllerTypes.PLAYER_CONTROLLER:
					return new PlayerSpaceShipController( ship, gameData );
			}
			
			return null;
		}
	}
}