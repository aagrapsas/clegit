package game.ui.misc 
{
	import engine.interfaces.IDestroyable;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIGenericButton extends Sprite implements IDestroyable
	{
		private var _TextField:TextField;
		private var _Text:String;
		private var _GameData:HomeGameData;
		private var _Callback:Function;
		
		private var _Filter:GlowFilter;
		
		private var _ClickAudio:String;
		
		public function UIGenericButton( text:String, gameData:HomeGameData, clickCallback:Function, clickAudio:String = "UI_Generic_Select" ) 
		{
			_Text = text;
			_GameData = gameData;
			_Callback = clickCallback;
			_ClickAudio = clickAudio;
			
			initialize();
		}	
		
		private function initialize():void
		{
			var image:Bitmap = _GameData.AssetManager.getImageCopy( "UIGenericButtonImg" );
			
			this.addChild( image );
			
			_TextField = new TextField();
			_TextField.embedFonts = true;
			_TextField.antiAliasType = AntiAliasType.ADVANCED;
			_TextField.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 20, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			_TextField.width = image.width;
			_TextField.height = image.height;
			_TextField.selectable = false;
			_TextField.x = 0;
			_TextField.y = 0;
			
			_TextField.text = _Text;
			
			this.addChild( _TextField );
			
			_TextField.mouseEnabled = false;
			
			this.mouseChildren = false;
			this.mouseEnabled = true;
			this.useHandCursor = true;
			this.buttonMode = true;
			
			this.addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			this.addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			this.addEventListener( MouseEvent.CLICK, onClick );
			
			_Filter = new GlowFilter();
			_Filter.blurX = 8;
			_Filter.blurY = 8;
			_Filter.strength = 0.80;
			_Filter.color = 0xFFFFFF;
			_Filter.quality = BitmapFilterQuality.LOW;
		}
		
		private function onRollOver( e:MouseEvent ):void
		{
			_TextField.filters = [ _Filter ];
		}
		
		private function onRollOut( e:MouseEvent ):void
		{
			_TextField.filters = [];
		}
		
		private function onClick( e:MouseEvent ):void
		{
			if ( _ClickAudio )
			{
				_GameData.SoundSystem.playSound( _ClickAudio );
			}
			
			if ( _Callback != null )
			{
				_Callback.apply( null, [ this ] );
			}
		}
		
		public function destroy():void
		{
			this.removeEventListener( MouseEvent.ROLL_OVER, onRollOver );
			this.removeEventListener( MouseEvent.ROLL_OUT, onRollOut );
			this.removeEventListener( MouseEvent.CLICK, onClick );
		}
	}
}