package game.ui.misc 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UILoadingBar 
	{
		private var _Clip:MovieClip;
		
		// Members
		private var _LoadedTextOne:TextField;
		private var _LoadedTextTwo:TextField;
		private var _LoadedTextThree:TextField;
		
		public function UILoadingBar( clip:MovieClip ) 
		{
			_Clip = clip;
			
			initialize();
		}
		
		private function initialize():void
		{
			_LoadedTextOne = _Clip.txtOne;
			_LoadedTextTwo = _Clip.txtTwo;
			_LoadedTextThree = _Clip.txtThree;
			
			_LoadedTextOne.visible = false;
			_LoadedTextTwo.visible = false;
			_LoadedTextThree.visible = false;
		}
		
		public function addLoaded( text:String ):void
		{
			var textOne:String = _LoadedTextOne.text;
			var textTwo:String = _LoadedTextTwo.text;
			var textThree:String = _LoadedTextThree.text;
			
			if ( textOne )
			{
				_LoadedTextTwo.text = textOne;
				_LoadedTextTwo.visible = true;
			}
			
			if ( textTwo )
			{
				_LoadedTextThree.text = textTwo;
				_LoadedTextThree.visible = true;
			}
			
			_LoadedTextOne.text = text;
			_LoadedTextOne.visible = true;
		}
		
		public function setPercentage( percent:int ):void
		{
			_Clip.gotoAndStop( percent );
		}
		
		public function get clip():MovieClip { return _Clip; }
	}
}