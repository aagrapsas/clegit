package game.ui.misc 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import flash.display.Sprite;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIScreenText extends Sprite implements ITickable, IDestroyable, ISceneNotifiable
	{
		private var _GameData:HomeGameData;
		
		private var _StartAlpha:Number = 1.0;
		private var _EndAlpha:Number = 0.0;
		private var _Accumulator:Number = 0;
		private var _Duration:Number = 3.0;
		
		private var _Text:TextField;
		
		public function UIScreenText( gameData:HomeGameData, text:String, x:int, y:int, color:uint = 0xFFFF00, duration:Number = 3.0 ) 
		{
			_GameData = gameData;
			_Duration = duration;
			
			initialize( text, x, y, color );
		}
		
		private function initialize( text:String, x:int, y:int, color:uint ):void
		{
			_Text = new TextField();
			_Text.embedFonts = true;
			_Text.antiAliasType = AntiAliasType.ADVANCED;
			_Text.selectable = false;
			_Text.defaultTextFormat = new TextFormat( FontSystem.MYRIAD_PRO, 18, color, true, null, null, null, null, TextFormatAlign.LEFT );
			_Text.text = text;
			_Text.width = _Text.textWidth + 5;
			_Text.height = _Text.textHeight;
			
			this.addChild( _Text );
			
			this.x = x;
			this.y = y;
			
			if ( this.x + this.width > _GameData.Scene.GameStage.stageWidth )
			{
				this.x = _GameData.Scene.GameStage.stageWidth - this.width;
			}
			
			_GameData.Scene.addTickable( this );
			_GameData.Scene.UILayer.addChild( this );
		}
		
		public function tick( deltaTime:Number ):void
		{
			_Accumulator += deltaTime;
			
			var alpha:Number = Math.min( _Accumulator / _Duration, 1.0 );
			
			this.alpha = MathUtility.easeOut( _StartAlpha, _EndAlpha, alpha );
			
			if ( this.alpha <= 0 )
			{
				destroy();
			}
		}
		
		public function handleSceneTransition():void
		{
			destroy();
		}
		
		public function destroy():void
		{
			_GameData.Scene.removeTickable( this );
			_GameData.Scene.UILayer.removeChild( this );
		}
	}
}