package game.ui.controllers 
{
	import engine.debug.Debug;
	import engine.render.scene.SceneNode;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getQualifiedClassName;
	import game.actors.components.PlatformInfo;
	import game.actors.SpaceShip;
	import game.catalogue.InfoElement;
	import game.controllers.AIController;
	import game.debug.GameLogChannels;
	import game.gameplay.weapons.DamageTypes;
	import game.HomeGameData;
	import game.ui.world.UICircleCollection;
	import game.ui.world.UICircleSelector;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIPlatformController implements IUIController
	{
		private var _GameData:HomeGameData;
		private var _Node:SceneNode;
		private var _Info:PlatformInfo;
		
		private var _IsDisposing:Boolean = false;
		private var _IsDisposable:Boolean = false;
		
		private var _SellIcon:Bitmap;
		private var _FBCIcon:Bitmap;
		private var _SalvageIcon:Bitmap;
		
		private var _UpgradeInfo:Vector.<InfoElement>;
		
		private var _RootIcons:Vector.<UICircleSelector>;
		
		private var _IsMouseOverNode:Boolean = false;
		private var _IsMouseOverUI:Boolean = false;
		
		private var _IsVisible:Boolean = false;
		
		private var _SecondAccumulator:Number = 0;
		private static const PERSIST_DURATION:Number = 0.5;
		
		private var _CircleCollection:UICircleCollection;
		
		public function UIPlatformController( sceneNode:SceneNode, data:HomeGameData, info:PlatformInfo ) 
		{
			_GameData = data;
			_Node = sceneNode;
			_Info = info;
			
			initialize();
		}
		
		private function initialize():void
		{
			var upgradeData:Bitmap = _GameData.AssetManager.getImage( "upgrade_icon" );
			var softCurrency:Bitmap = _GameData.AssetManager.getImage( "salvage_icon" );
			var hardCurrency:Bitmap = _GameData.AssetManager.getImage( "fbc_icon" );
			var back:Bitmap = _GameData.AssetManager.getImage( "return_icon" );
			var sell:Bitmap = _GameData.AssetManager.getImage( "sell_icon" );
			
			_CircleCollection = new UICircleCollection( ( _Node.InternalObject.Width / 2 ) + 20 );
			
			_UpgradeInfo = new Vector.<InfoElement>();
			
			_CircleCollection.display.x = _Node.InternalObject.X;
			_CircleCollection.display.y = -_Node.InternalObject.Y - _Node.InternalObject.Height / 2;
			
			for each ( var upgrade:String in _Info.Upgrades )
			{
				var additionalInfo:InfoElement = _GameData.InfoCatalog.getInfo( upgrade );
				
				_UpgradeInfo.push( additionalInfo );
				
				var children:Vector.<UICircleSelector> = new Vector.<UICircleSelector>();
				
				// Setup icons
				var upgradeIcon:UICircleSelector = new UICircleSelector( "upgrade", additionalInfo.description, new Bitmap( upgradeData.bitmapData.clone(), "auto", true ), children, null, cbUpgrade, additionalInfo );
				
				var softIcon:UICircleSelector = new UICircleSelector( "soft_currency", "Salvage: " + additionalInfo.softCost, new Bitmap( softCurrency.bitmapData.clone(), "auto", true ), null, upgradeIcon, cbSoftCurrency, additionalInfo );
				var hardIcon:UICircleSelector = new UICircleSelector( "hard_currency", "Facebook Credits: " + additionalInfo.hardCost, new Bitmap( hardCurrency.bitmapData.clone(), "auto", true ), null, upgradeIcon, cbHardCurrency, additionalInfo );
				var returnIcon:UICircleSelector = new UICircleSelector( "return", "Back", new Bitmap( back.bitmapData.clone(), "auto", true ), null, upgradeIcon, cbReturn, additionalInfo );
				
				children.push( softIcon );
				children.push( hardIcon );
				children.push( returnIcon );
				
				_CircleCollection.addRoot( upgradeIcon );
			}
			
			var sellIcon:UICircleSelector = new UICircleSelector( "sell", "Scuttle platform", sell, null, null, cbSell, null );
			
			_CircleCollection.addRoot( sellIcon );
		}

		private function cbUpgrade( btn:UICircleSelector ):void
		{
			_CircleCollection.showCollection( btn );
		}
		
		private function cbSoftCurrency( btn:UICircleSelector ):void
		{
			Debug.debugLog( GameLogChannels.DEBUG, "Purchased soft currency for " + btn.info.target );
			
			purchase( btn.info as InfoElement, true );
			
			dispose();
		}
		
		private function cbHardCurrency( btn:UICircleSelector ):void
		{
			Debug.debugLog( GameLogChannels.DEBUG, "Purchased hard currency for " + btn.info.target );
			
			purchase( btn.info as InfoElement, false );
			
			dispose();
		}
		
		private function purchase( info:InfoElement, isSoftCurrency:Boolean ):void
		{
			// @TODO subtract cost!
			var newShip:SpaceShip = _GameData.Spawner.getShip( info.target );
			
			if ( !newShip )
			{
				return;
			}
			
			newShip.Team = ( _Node.InternalObject as SpaceShip ).Team;
			
			var x:int = _Node.InternalObject.X;
			var y:int = _Node.InternalObject.Y;
			
			var ai:AIController = _GameData.CurrentScenario.getAIByShip( _Node.InternalObject as SpaceShip );
			
			var newAI:AIController = new AIController( newShip, _GameData );
			
			if ( ai )
			{
				newAI.Squad = ai.Squad;
				newAI.Name = ai.Name;
				
				_GameData.CurrentScenario.removeAI( ai.Name );	// remove old ai
				
				_GameData.CurrentScenario.registerAI( newAI.Name, newAI );	// add new one
			}
			
			_GameData.Scene.removeActor( _Node.InternalObject );
			
			_Node.InternalObject.destroy();
			
			newShip.X = x;
			newShip.Y = y;
		}
		
		private function cbSell( btn:UICircleSelector ):void
		{
			Debug.debugLog( GameLogChannels.DEBUG, "Scuttle platform" );
			
			var ship:SpaceShip = _Node.InternalObject as SpaceShip;
			
			ship.DoDamage( DamageTypes.MISC, ship.ArmorHealth.current + ( ship.ShieldHealth ? ship.ShieldHealth.current : 0 ), null, 0, 0 );
			
			dispose();
		}
		
		private function cbReturn( btn:UICircleSelector ):void
		{
			_CircleCollection.showCollection( btn.circleParent.circleParent );
		}
		
		public function click():void
		{
			_GameData.Scene.Layers[ "world_ui" ].Display.addChild( _CircleCollection.display );
			
			_CircleCollection.showCollection();
		}
		
		public function clear():void
		{
			var rect:Rectangle = _CircleCollection.display.getRect( _GameData.Scene.GameStage );
			var mouseX:int = _GameData.Scene.GameStage.mouseX;
			var mouseY:int = _GameData.Scene.GameStage.mouseY;
			
			if ( rect.containsPoint( new Point( mouseX, mouseY ) ) == false )
			{
				dispose();
			}
		}
		
		private function dispose():void
		{
			_IsDisposable = true;
			_IsDisposing = true;
			
			_IsVisible = false;
		}
		
		// Called by UIInteractionManager, every frame not disposed
		public function tick( deltaTime:Number ):void
		{
			if ( _IsDisposing )
			{
				return;
			}
			
			_CircleCollection.tick( deltaTime );
		}
		
		// Called by UIInteractionManager, every frame over
		public function over():void
		{
			if ( _IsVisible == false )
			{				
				_SecondAccumulator = 0;
				
				Debug.debugLog( GameLogChannels.DEBUG, "Over Scene" );
			}
			
			_IsVisible = true;
			
			_IsMouseOverNode = true;
		}
		
		public function get nodeHandling():SceneNode { return _Node; }
		public function get isDisposing():Boolean { return _IsDisposing; }
		public function get isDisposable():Boolean { return _IsDisposable; }
		
		// Called by UIInteractionManager
		public function destroy():void
		{
			_GameData.Scene.Layers[ "world_ui" ].Display.removeChild( _CircleCollection.display );
			
			for each ( var icon:UICircleSelector in _RootIcons )
			{
				icon.destroy();
			}
			
			Debug.debugLog( GameLogChannels.DEBUG, "DESTROYED" );
			
			_RootIcons = null;
		}
	}
}