package game.ui.world 
{
	import engine.interfaces.ITickable;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import game.ui.world.UICircleSelector;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UICircleCollection implements ITickable
	{
		private var _Display:Sprite;

		private var _Roots:Vector.<UICircleSelector>;
		private var _Active:Vector.<UICircleSelector>;
		private var _Radius:int;
		private var _PreviousCollection:UICircleSelector;
		
		public function UICircleCollection( radius:int ) 
		{
			_Roots = new Vector.<UICircleSelector>();
			_Display = new Sprite();
			
			_Radius = radius;
			
			_Display.mouseChildren = true;
			_Display.mouseEnabled = false;
		}
		
		public function addRoot( root:UICircleSelector ):void
		{			
			_Roots.push( root );
		}
		
		public function showPrevious():void
		{
			showCollection( _PreviousCollection );
			_PreviousCollection = _PreviousCollection.circleParent;
		}
		
		public function showCollection( circle:UICircleSelector = null ):void
		{			
			if ( circle == null )
			{
				_Active = _Roots;
			}
			else
			{
				_Active = circle.circleChildren;
				_PreviousCollection = circle;
			}
			
			clearDisplayChildren();
			
			_Display.visible = true;
			
			var radiansEach:Number = ( 2 * Math.PI ) / _Active.length;
			var runningRadians:Number = 0;
			
			// Evenly distribute icons across circle
			for each ( var selector:UICircleSelector in _Active )
			{
				_Display.addChild( selector );
				
				// Move on radius of circle
				selector.x = ( -selector.display.width / 2 ) + _Radius * Math.sin( runningRadians );
				selector.y = ( -selector.display.height / 2 ) + _Radius * Math.cos( runningRadians );
				selector.y *= -1;
				
				// Add to running separation
				runningRadians += radiansEach;
			}
		}
		
		private function clearDisplayChildren():void
		{
			if ( _Display.numChildren > 0 )
			{
				var toRemove:Vector.<DisplayObject> = new Vector.<DisplayObject>();
				
				for ( var i:int = 0; i < _Display.numChildren; i++ )
				{
					toRemove.push( _Display.getChildAt( i ) );
				}
				
				for each ( var child:DisplayObject in toRemove )
				{
					_Display.removeChild( child );
				}
			}
		}
		
		public function hide():void
		{
			clearDisplayChildren();
			
			_Display.visible = false;
		}
		
		public function destroy():void
		{
			for each ( var circle:UICircleSelector in _Roots )
			{
				circle.destroy();
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !_Active )
			{
				return;
			}
			
			for each ( var circle:UICircleSelector in _Active )
			{
				circle.tick( deltaTime );
			}
		}
		
		public function get display():Sprite { return _Display; }
	}
}