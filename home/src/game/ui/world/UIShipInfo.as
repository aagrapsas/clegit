package game.ui.world 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.misc.GenericPool;
	import engine.misc.IPoolable;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIShipInfo extends Sprite implements ITickable, IPoolable, IDestroyable, ISceneNotifiable
	{
		private var _GameData:HomeGameData;
		private var _Target:SpaceShip;
		
		private var _Clip:MovieClip;
		
		// Clip elements
		private var _NameText:TextField;
		private var _Shields:MovieClip;
		private var _Armor:MovieClip;
		
		private var _IsInitialized:Boolean = false;
		
		private var _Pool:GenericPool;
		
		private var _IsAlive:Boolean = false;
		
		private var _ExpectedTeam:int = 0;
		
		public function UIShipInfo( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			var shipInfoClass:Class = _GameData.AssetManager.getClass( "UIHUDShipInfoSWF" );
			
			_Clip = new shipInfoClass();
			_NameText = _Clip.txtName;
			_Armor = _Clip.armor;
			_Shields = _Clip.shields;
			
			this.addChild( _Clip );
		}
		
		public function setup( target:SpaceShip, nameOverride:String = null ):void
		{
			_Target = target;
			
			_IsAlive = true;
			
			_Target.addEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			
			_GameData.Scene.addTickable( this );	// Add to scene
			_GameData.Scene.addWorldObject( this );
			
			// Setup clip
			if ( nameOverride )
			{
				_NameText.text = nameOverride;
			}
			else
			{
				_NameText.text = _Target.Info.Name;
			}
			
			_ExpectedTeam = _Target.Team;
			
			colorTeam();
			
			_Shields.visible = _Target.Info.Shield != null;
			
			// Force update of shields and armor display
			updateHealth();
		}
		
		private function colorTeam():void
		{
			if ( _GameData.PlayerShip && _GameData.PlayerShip.Team != _Target.Team )
			{
				_NameText.textColor = 0xFF0000;	// red
			}
			else
			{
				_NameText.textColor = 0xFFFFFF;	// white
			}
		}
		
		private function onShipDestroyed( e:ShipEvent ):void
		{
			_Pool.push( this );
		}
		
		private function updateHealth():void
		{
			// Calculate percentages
			const healthPercentage:Number = Math.min( _Target.ArmorHealth.current / _Target.Info.Armor.FullHitPoints, 1 );
			
			_Armor.gotoAndStop( int( 100 * healthPercentage ) );
			
			if ( _Shields.visible )
			{
				const shieldPercentage:Number = Math.min( _Target.ShieldHealth.current / _Target.Info.Shield.MaxShields, 1 );
				
				_Shields.gotoAndStop( int( 100 * shieldPercentage ) );
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !_Target )
			{
				return;
			}
			
			if ( _Target.isDisposing )
			{
				_Pool.push( this );
				return;
			}
			
			if ( _ExpectedTeam != _Target.Team )	// Team change
			{
				colorTeam();
				_ExpectedTeam = _Target.Team;
			}
			
			this.x = _Target.X;
			this.y = -_Target.Y;	// Flipped y coordinate
			
			if ( _GameData.PlayerShip && _Target == _GameData.PlayerShip )
			{
				_Pool.push( this );
			}
			
			updateHealth();
		}
		
		public function resurrect():void
		{
			
		}
		
		public function suspend():void
		{
			if ( !_IsAlive )
			{
				return;
			}
			
			_GameData.Scene.removeTickable( this );	// Remove from scene
			_GameData.Scene.removeWorldObject( this );
			
			if ( _Target )
			{
				_Target.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			}
			
			_IsAlive = false;
		}
		
		public function handleSceneTransition():void
		{
			_Pool.push( this );
		}
		
		public function set pool( value:GenericPool ):void { _Pool = value; }
		
		public function destroy():void
		{
			suspend();
		}
	}
}