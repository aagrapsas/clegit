package game.ui.world 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UICircleSelector extends Sprite implements IDestroyable, ITickable
	{
		private var _Children:Vector.<UICircleSelector>;
		private var _Display:Bitmap;
		private var _ClickCallback:Function;
		private var _Name:String;
		private var _Info:Object;
		private var _TextField:TextField;
		private var _Text:String;
		private var _Parent:UICircleSelector;
		
		private var _IsOver:Boolean = false;
		private var _SecondAcumulator:Number = 0;
		private static const PERSIST_DURATION:Number = 3.0;
		
		private var _ChildMap:Dictionary;
		
		public function UICircleSelector( name:String, text:String, image:Bitmap, children:Vector.<UICircleSelector>, parent:UICircleSelector, clickCallback:Function, info:Object ) 
		{
			_Display = image;
			_Children = children;
			_ClickCallback = clickCallback;
			_Parent = parent;
			_Name = name;
			_Info = info;
			_Text = text;
			
			_ChildMap = new Dictionary();
			
			initialize();
		}
		
		private function initialize():void
		{
			this.addEventListener( MouseEvent.CLICK, onClick );
			this.addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			this.addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			
			this.addChild( _Display );
			
			this.useHandCursor = true;
			this.buttonMode = true;
			
			this.mouseEnabled = true;
			
			if ( _Text )
			{
				_TextField = new TextField();
				
				var style:TextFormat = new TextFormat();
				style.align = "left";
				style.size = "12";
				style.color = 0xCCFF00;
				
				_TextField.defaultTextFormat = style;
				
				_TextField.text = _Text;
				_TextField.width = _TextField.textWidth * 2;
				_TextField.height = _TextField.textHeight * 2;
				
				this.addChild( _TextField );
				
				_TextField.y = ( _Display.height / 2 ) - ( _TextField.height / 2 );
				_TextField.x = _Display.width + 5;
				
				_TextField.selectable = false;
				
				_TextField.visible = false;
			}
		}
		
		public function tick( deltaTime:Number ):void
		{			
			
		}
		
		private function onClick( e:MouseEvent ):void
		{
			if ( _ClickCallback != null )
			{
				_ClickCallback.apply( null, [this] );
			}
		}
		
		private function onRollOver( e:MouseEvent ):void
		{
			if ( _TextField )
			{
				_TextField.visible = true;
			}
		}
		
		private function onRollOut( e:MouseEvent ):void
		{
			if ( _TextField )
			{
				_TextField.visible = false;
			}
		}
		
		public function get info():Object { return _Info; }
		public function get circleParent():UICircleSelector { return _Parent; }
		public function get circleChildren():Vector.<UICircleSelector> { return _Children; }
		public function get display():Bitmap { return _Display; }
		
		public function destroy():void
		{			
			this.removeEventListener( MouseEvent.CLICK, onClick );
			this.removeEventListener( MouseEvent.ROLL_OVER, onRollOver );
			this.removeEventListener( MouseEvent.ROLL_OUT, onRollOut );
			
			if ( _Children )
			{
				for each ( var child:UICircleSelector in _Children )
				{				
					child.destroy();
				}
			}
		}
	}
}