package game.ui.menu 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIShipInfoField extends Sprite
	{
		private var _Clip:MovieClip;
		
		public function UIShipInfoField( gameData:HomeGameData )
		{
			var classInfo:Class = gameData.AssetManager.getClass( "UIFieldMeterSWF" );
			
			_Clip = new classInfo();
			
			this.addChild( _Clip );
			
			_Clip.bar.gotoAndStop( 1 );
		}
		
		public function setField( field:String ):void
		{
			_Clip.txtField.text = field;
		}
		
		public function setValue( value:Number ):void
		{
			_Clip.txtValue.text = value;
		}
		
		public function setProgress( value:int ):void
		{
			value = Math.min( Math.max( 1, value ), 100 );
			
			_Clip.bar.gotoAndStop( value );
		}
	}
}