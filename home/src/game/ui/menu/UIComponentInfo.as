package game.ui.menu 
{
	import flash.display.Sprite;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.actors.components.ArmorInfo;
	import game.actors.components.CombatSystemInfo;
	import game.actors.components.EngineInfo;
	import game.actors.components.EvasionInfo;
	import game.actors.components.SensorInfo;
	import game.actors.components.ShieldInfo;
	import game.actors.components.ShipComponentInfo;
	import game.actors.components.ShipComponentTypes;
	import game.gameplay.weapons.WeaponTypes;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIComponentInfo extends Sprite
	{
		private var _Component:ShipComponentInfo
		
		private var _TextOne:TextField;
		private var _TextTwo:TextField;
		private var _TextThree:TextField;
		private var _TextFour:TextField;
		private var _TextFive:TextField;
		private var _TextSix:TextField;
		
		private var _ValueOne:TextField;
		private var _ValueTwo:TextField;
		private var _ValueThree:TextField;
		private var _ValueFour:TextField;
		private var _ValueFive:TextField;
		private var _ValueSix:TextField;
		
		private const LEFT_WIDTH:int = 125;
		private const RIGHT_WIDTH:int = 130;
		private const PADDING:int = 2;
		
		public function UIComponentInfo() 
		{
			
		}
		
		private function clearAll():void
		{
			_TextOne.text = "";
			_TextTwo.text = "";
			_TextThree.text = "";
			_TextFour.text = "";
			_TextFive.text = "";
			_TextSix.text = "";
			_ValueOne.text = "";
			_ValueTwo.text = "";
			_ValueThree.text = "";
			_ValueFour.text = "";
			_ValueFive.text = "";
			_ValueSix.text = "";
		}
		
		public function setInfo( component:ShipComponentInfo ):void
		{
			if ( _TextOne == null )
			{
				build();
			}
			
			clearAll();
			
			if ( component.Type == ShipComponentTypes.ARMOR_COMPONENT )
			{
				var armor:ArmorInfo = component as ArmorInfo;
				
				_TextOne.text = "FULL HP: ";
				_ValueOne.text = String( armor.FullHitPoints );
				
				_TextTwo.text = "REGEN: ";
				_ValueTwo.text = armor.ArmorRegenPerSecond + "/second";
				
				_TextThree.text = "REGEN DELAY: ";
				_ValueThree.text = armor.ArmorRegenDelay + " seconds";
				
				if ( component.Tonnage > 0 )
				{
					_TextFour.text = "TONNAGE: ";
					_ValueFour.text = String( component.Tonnage );
				}
			}
			else if ( component.Type == ShipComponentTypes.COMBAT_SYSTEM )
			{
				var combatSystem:CombatSystemInfo = component as CombatSystemInfo;
				
				_TextOne.text = "DAMAGE: ";
				_ValueOne.text = String( combatSystem.Damage );
				
				var dps:String = String( combatSystem.DPS );
				
				if ( dps.indexOf( "." ) != -1 )
				{
					var period:int = dps.indexOf( "." );
					
					if ( dps.length > period + 3 )
					{
						dps = dps.substr( 0, period + 3 );
					}
				}
				
				_TextTwo.text = "DPS: ";
				_ValueTwo.text = dps;
				
				_TextThree.text = "RANGE: ";
				_ValueThree.text = combatSystem.Range + " meters";
				
				_TextFour.text = "SPEED: ";
				_ValueFour.text = String( int( combatSystem.RawXML.@speed ) );
				
				_TextFive.text = "COOLDOWN: ";
				_ValueFive.text = String( combatSystem.FireDelay );
				
				
				_TextSix.text = "TONNAGE: ";
				_ValueSix.text = String( combatSystem.Tonnage );
			}
			else if ( component.Type == ShipComponentTypes.ENGINE_COMPONENT )
			{
				var engine:EngineInfo = component as EngineInfo;
				
				_TextOne.text = "TOP SPEED: ";
				_ValueOne.text = String( engine.MaxSpeed );
				
				_TextTwo.text = "ACCELERATION: ";
				_ValueTwo.text = String( engine.MaxAcceleration );
				
				_TextThree.text = "TURN SPEED: ";
				_ValueThree.text = String( engine.MaxTurnSpeed );
				
				if ( component.Tonnage > 0 )
				{
					_TextFour.text = "TONNAGE: ";
					_ValueFour.text = String( component.Tonnage );
				}
			}
			else if ( component.Type == ShipComponentTypes.EVASION_COMPONENT )
			{
				var evasion:EvasionInfo = component as EvasionInfo;
				
				_TextOne.text = "VELOCITY BOOST: ";
				_ValueOne.text = evasion.velocityMultiplier + "x";
				
				_TextTwo.text = "STOP DAMPENER: ";
				_ValueTwo.text = ( evasion.stopDampener * 100 ) + "%";
				
				_TextThree.text = "COOLDOWN: ";
				_ValueThree.text = evasion.cooldown + " seconds";
				
				if ( component.Tonnage > 0 )
				{
					_TextFour.text = "TONNAGE: ";
					_ValueFour.text = String( component.Tonnage );
				}
			}
			else if ( component.Type == ShipComponentTypes.SENSOR_COMPONENT )
			{
				var sensor:SensorInfo = component as SensorInfo;
				
				_TextOne.text = "RANGE: "
				_ValueOne.text = String( sensor.MaxRange );
				
				if ( component.Tonnage > 0 )
				{
					_TextTwo.text = "TONNAGE: ";
					_ValueTwo.text = String( component.Tonnage );
				}
			}
			else if ( component.Type == ShipComponentTypes.SHIELD_COMPONENT )
			{
				var shield:ShieldInfo = component as ShieldInfo;
				
				_TextOne.text = "SHIELD HP: ";
				_ValueOne.text = String( shield.MaxShields );
				
				_TextTwo.text = "REGEN: ";
				_ValueTwo.text = shield.ShieldRegenPerSecond + "/second";
				
				_TextThree.text = "REGEN DELAY: ";
				_ValueThree.text = shield.ShieldRegenDelay + " seconds";
				
				if ( component.Tonnage > 0 )
				{
					_TextFour.text = "TONNAGE: ";
					_ValueFour.text = String( component.Tonnage );
				}
			}
		}
		
		private function build():void
		{
			_TextOne = buildTextField( TextFormatAlign.RIGHT, LEFT_WIDTH );
			_TextTwo = buildTextField( TextFormatAlign.RIGHT, LEFT_WIDTH );
			_TextThree = buildTextField( TextFormatAlign.RIGHT, LEFT_WIDTH );
			_TextFour = buildTextField( TextFormatAlign.RIGHT, LEFT_WIDTH );
			_TextFive = buildTextField( TextFormatAlign.RIGHT, LEFT_WIDTH );
			_TextSix = buildTextField( TextFormatAlign.RIGHT, LEFT_WIDTH );
			
			_ValueOne = buildTextField( TextFormatAlign.LEFT, RIGHT_WIDTH );
			_ValueTwo = buildTextField( TextFormatAlign.LEFT, RIGHT_WIDTH );
			_ValueThree = buildTextField( TextFormatAlign.LEFT, RIGHT_WIDTH );
			_ValueFour = buildTextField( TextFormatAlign.LEFT, RIGHT_WIDTH );
			_ValueFive = buildTextField( TextFormatAlign.LEFT, RIGHT_WIDTH );
			_ValueSix = buildTextField( TextFormatAlign.LEFT, RIGHT_WIDTH );
			
			var text:Vector.<TextField> = new Vector.<TextField>();
			
			text.push( _TextOne );
			text.push( _TextTwo );
			text.push( _TextThree );
			text.push( _TextFour );
			text.push( _TextFive );
			text.push( _TextSix );
			
			var values:Vector.<TextField> = new Vector.<TextField>();
			
			values.push( _ValueOne );
			values.push( _ValueTwo );
			values.push( _ValueThree );
			values.push( _ValueFour );
			values.push( _ValueFive );
			values.push( _ValueSix );
			
			var runningY:int = 0;
			
			for each ( var textfield:TextField in text )
			{
				textfield.y = runningY;
				textfield.x = 0;
				
				runningY += textfield.height + PADDING;
			}
			
			runningY = 0;
			
			for each ( var valueField:TextField in values )
			{
				valueField.y = runningY;
				valueField.x = 132;
				
				runningY += valueField.height + PADDING;
			}
		}
		
		private function buildTextField( alignment:String, width:int ):TextField
		{
			var textField:TextField = new TextField();
			textField.embedFonts = true;
			textField.antiAliasType = AntiAliasType.ADVANCED;
			textField.defaultTextFormat = new TextFormat( FontSystem.AVANT_GARDE, 18, 0xFFFFFF, null, null, null, null, null, alignment );
			textField.x = 0;
			textField.width = width;
			textField.height = 22;
			textField.selectable = false;
			
			this.addChild( textField );
			
			return textField;
		}
	}
}