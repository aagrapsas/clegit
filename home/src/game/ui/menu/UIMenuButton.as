package game.ui.menu 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIMenuButton extends Sprite implements ITickable, IDestroyable
	{
		private var _Clip:MovieClip;
		private var _ClickCallback:Function;
		private var _RollOverCallback:Function;
		private var _Name:String;
		
		private var _IsOver:Boolean = false;
		
		private var _ClickAudio:String;
		
		private var _GameData:HomeGameData;
		
		public function UIMenuButton( gameData:HomeGameData, name:String, clip:MovieClip, clickCallback:Function, clickAudio:String = "UI_Generic_Select" ) 
		{
			_Clip = clip;
			_ClickCallback = clickCallback;
			_Name = name;
			_ClickAudio = clickAudio;
			_GameData = gameData;
			
			initialize();
		}
		
		public function registerRollOverCallback( callback:Function ):void
		{
			_RollOverCallback = callback;
		}
		
		private function initialize():void
		{
			this.addChild( _Clip );
			
			this.addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			this.addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			this.addEventListener( MouseEvent.CLICK, onClick );
			
			_Clip.gotoAndStop( 1 );
			
			_Clip.txtName.text = _Name;
		}
		
		private function onRollOver( e:MouseEvent ):void
		{
			_IsOver = true;
			
			_Clip.gotoAndStop( 2 );
			_Clip.txtName.text = _Name;
			
			if ( _RollOverCallback != null )
			{
				_RollOverCallback.apply( null, [this] );
			}
		}
		
		private function onRollOut( e:MouseEvent ):void
		{
			_IsOver = false;
			
			_Clip.gotoAndStop( 1 );
			_Clip.txtName.text = _Name;
		}
		
		private function onClick( e:MouseEvent ):void
		{
			if ( _ClickAudio )
			{
				_GameData.SoundSystem.playSound( _ClickAudio );
			}
			
			if ( _ClickCallback != null )
			{
				_ClickCallback.apply( null, [this] );
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !this.stage )
			{
				return;
			}
			
			const mouseX:int = this.stage.mouseX;
			const mouseY:int = this.stage.mouseY;
			
			if ( this.getRect( this.stage ).contains( mouseX, mouseY ) )
			{
				// Over
				if ( _IsOver == false )
				{
					onRollOver( null );
				}
			}
			else
			{
				// Out
				if ( _IsOver )
				{
					onRollOut( null );
				}
			}
		}
		
		public function get buttonName():String { return _Name; }
		
		public function destroy():void
		{
			this.removeEventListener( MouseEvent.ROLL_OVER, onRollOver );
			this.removeEventListener( MouseEvent.ROLL_OUT, onRollOut );
			this.removeEventListener( MouseEvent.CLICK, onClick );
			
			_ClickCallback = null;
			_RollOverCallback = null;
		}
	}
}