package game.ui.menu 
{
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import game.actors.components.ShipComponentTypes;
	import game.actors.SpaceShipInfo;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIShipCustomizationOptions extends UIMenuPanel
	{
		private var _Ship:SpaceShipInfo;
		
		// Text
		private var _MainText:TextField;
		
		private var _SelectionsButtons:Vector.<UIMenuButton>;
		
		private var _ActivePanel:UIShipHighlights;
		
		private var _WhiteDivider:Bitmap;
		private var _LeftFade:Bitmap;
		
		public function UIShipCustomizationOptions( ship:SpaceShipInfo ) 
		{
			_Ship = ship;
		}
	
		override public function initialize( gameData:HomeGameData, container:UIMenuContainer ):void
		{
			super.initialize( gameData, container );
			
			_WhiteDivider = _GameData.AssetManager.getBitmapFromClass( "UIMenuWhiteLineSWF" );
			
			this.addChild( _WhiteDivider );
			
			_MainText = new TextField();
			_MainText.embedFonts = true;
			_MainText.defaultTextFormat = new TextFormat( FontSystem.PIRULEN, 14, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.RIGHT );
			_MainText.selectable = false;
			_MainText.text = _Ship.Name + " Config";
			_MainText.width = _WhiteDivider.width;
			_MainText.height = 20;
			
			this.addChild( _MainText );
			
			_LeftFade = _GameData.AssetManager.getBitmapFromClass( "UIMenuVertShadowSWF" );
			
			this.addChild( _LeftFade );
			
			_ActivePanel = new UIShipHighlights( _GameData, _Ship );
			this.addChild( _ActivePanel );
			
			var buttonClass:Class = _GameData.AssetManager.getClass( "UIMenuButtonSWF" );
			
			_SelectionsButtons = new Vector.<UIMenuButton>();
			
			_SelectionsButtons.push( new UIMenuButton( _GameData, "Weapons", new buttonClass(), onWeaponsClicked ) );
			_SelectionsButtons.push( new UIMenuButton( _GameData, "Armor", new buttonClass(), onArmorClicked ) );
			_SelectionsButtons.push( new UIMenuButton( _GameData, "Shields", new buttonClass(), onShieldsClicked ) );
			_SelectionsButtons.push( new UIMenuButton( _GameData, "Engines", new buttonClass(), onEnginesClicked ) );
			_SelectionsButtons.push( new UIMenuButton( _GameData, "Sensors", new buttonClass(), onSensorsClicked ) );
			_SelectionsButtons.push( new UIMenuButton( _GameData, "Back", new buttonClass(), onBackClicked, "UI_Generic_Cancel" ) );
			
			for each ( var button:UIMenuButton in _SelectionsButtons )
			{
				this.addChild( button );
			}
			
			align();
		}
		
		private function onWeaponsClicked( button:UIMenuButton ):void
		{
			var panel:UIShipCustomizationCategory = new UIShipCustomizationCategory( _Ship, ShipComponentTypes.COMBAT_SYSTEM );
			panel.isHardpointSelector = true;
			
			_Container.transition( panel );
		}
		
		private function onArmorClicked( button:UIMenuButton ):void
		{
			_Container.transition( new UIShipCustomizationCategory( _Ship, ShipComponentTypes.ARMOR_COMPONENT ) );
		}
		
		private function onShieldsClicked( button:UIMenuButton ):void
		{
			_Container.transition( new UIShipCustomizationCategory( _Ship, ShipComponentTypes.SHIELD_COMPONENT ) );
		}
		
		private function onEnginesClicked( button:UIMenuButton ):void
		{
			_Container.transition( new UIShipCustomizationCategory( _Ship, ShipComponentTypes.ENGINE_COMPONENT ) );
		}
		
		private function onSensorsClicked( button:UIMenuButton ):void
		{
			_Container.transition( new UIShipCustomizationCategory( _Ship, ShipComponentTypes.SENSOR_COMPONENT ) );
		}
		
		private function onBackClicked( button:UIMenuButton ):void
		{
			// Go back to main menu
			_Container.transition( new UIHangar() );
		}
		
		override public function align():void
		{
			const left:int = 0;
			const right:int = _GameData.Scene.GameStage.stageWidth;
			const top:int = 0;
			const bottom:int = _GameData.Scene.GameStage.stageHeight;
			const width:int = Math.abs( left ) + right;
			const height:int = Math.abs( top ) + bottom;
			
			if ( _ActivePanel )
			{
				_ActivePanel.x = right - _ActivePanel.width - 40; // some buffer
				_ActivePanel.y = top;
			}
			
			_MainText.x = left + 10;
			_MainText.y = top + 77;
			
			_WhiteDivider.x = left + 10;
			_WhiteDivider.y = top + 99;
			
			_LeftFade.height = height;
			_LeftFade.x = left + 254;
			_LeftFade.y = top;
			
			var runningY:int = top + 101;
			
			for each ( var button:UIMenuButton in _SelectionsButtons )
			{
				button.x = left + 10;
				button.y = runningY;
				
				runningY += 20;
			}
		}
		
		override public function tick( deltaTime:Number ):void
		{
			for each ( var button:UIMenuButton in _SelectionsButtons )
			{
				button.tick( deltaTime );
			}
		}
		
		override public function destroy():void
		{
			for each ( var button:UIMenuButton in _SelectionsButtons )
			{
				button.destroy();
			}
		}
	}
}