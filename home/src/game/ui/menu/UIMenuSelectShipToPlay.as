package game.ui.menu 
{
	import game.actors.SpaceShipInfo;
	import game.HomeGameData;
	import game.scenarios.Scenario;
	import game.ui.misc.UIHelpScreen;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIMenuSelectShipToPlay extends UIHangar
	{
		private var _Level:String;
		
		private var _HasBeenClicked:Boolean = false;
		
		public function UIMenuSelectShipToPlay( level:String ) 
		{
			_Level = level;
		}
		
		override public function initialize( gameData:HomeGameData, container:UIMenuContainer ):void
		{
			super.initialize( gameData, container );
			
			_MainText.text = "Select Ship";
		}
		
		override protected function onShipClicked( button:UIMenuButton ):void
		{
			if ( _HasBeenClicked )
			{
				return;
			}
			
			_HasBeenClicked = true;
			
			var shipName:String = button.buttonName;
			
			for each ( var ship:SpaceShipInfo in _GameData.ClientData.ships )
			{
				if ( ship.Name == shipName )
				{
					_GameData.ClientData.activeShip = ship;
					break;
				}
			}
			
			_GameData.SoundSystem.stopAllSounds();
			
			_GameData.CurrentScenario = new Scenario( _GameData );
			_GameData.CurrentScenario.deserialize( _GameData.AssetManager.getXML( _Level ) );
			_GameData.CurrentScenario.setup();
			
			_Container.destroy();
		}
	}
}