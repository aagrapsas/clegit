package game.ui 
{
	import engine.Actor;
	import engine.collision.CollisionFlags;
	import engine.debug.Debug;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneNode;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import game.actors.components.PlatformInfo;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.ui.controllers.IUIController;
	import game.ui.controllers.UIPlatformController;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIInteractionManager implements ITickable, ISceneNotifiable
	{
		private var _GameData:HomeGameData
		private var _Controllers:Vector.<IUIController>;
		
		public function UIInteractionManager( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			// Setup listeners
			_GameData.Scene.GameStage.addEventListener( MouseEvent.CLICK, onMouseClick );
			
			_Controllers = new Vector.<IUIController>();
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _GameData.IsInputEnabled == false || _GameData.Scene.Layers[ "objects" ] == null )
			{
				return;
			}
			
			var toDispose:Vector.<IUIController> = new Vector.<IUIController>();
			
			for each ( var controller:IUIController in _Controllers )
			{
				if ( controller.isDisposable )
				{
					toDispose.push( controller );
				}
				else
				{
					controller.tick( deltaTime );
				}
			}
			
			for each ( var disposable:IUIController in toDispose )
			{
				_Controllers.splice( _Controllers.indexOf( disposable ), 1 );
				disposable.destroy();
			}
		}
		
		private function determineInteraction( node:SceneNode ):void
		{
			var doesExist:Boolean = false;
			
			for each ( var controller:IUIController in _Controllers )
			{
				if ( controller.nodeHandling == node )
				{
					doesExist = true;
					break;
				}
				else
				{
					controller.clear();		// clear other controllers
				}
			}
			
			// If already exists, do nothing
			if ( doesExist )
			{
				return;
			}
			
			// Otherwise, build a new controller
			var newController:IUIController = null;
			
			var spaceShip:SpaceShip = node.InternalObject as SpaceShip;
			
			if ( spaceShip == null || !_GameData.PlayerShip || spaceShip.Team != _GameData.PlayerShip.Team )
			{
				return;
			}
			
			// TODO:
			// Add any controllers over here needed for hover
			var platformInfo:PlatformInfo = _GameData.ShipCompCatalog.getPlatform( spaceShip.Info.Key );
			
			if ( platformInfo != null )
			{
				newController = new UIPlatformController( node, _GameData, platformInfo );
				
				Debug.debugLog( GameLogChannels.DEBUG, "Creating platform UI controller" );
			}
			
			// If new controller exists, add to list and notify that it should start up
			if ( newController )
			{
				_Controllers.push( newController );
				
				newController.click();
			}
		}
		
		public function handleSceneTransition():void
		{
			for each ( var controller:IUIController in _Controllers )
			{
				controller.destroy();
			}
			
			_Controllers = new Vector.<IUIController>();
		}
		
		private function sortOver( a:SceneNode, b:SceneNode ):int
		{
			// 0 - equals
			// -1 - a
			// 1 - b
			
			if ( !_GameData.PlayerShip )	// no player space ship, ignore
			{
				return 0;
			}
			
			// Base on if it's player
			if ( a.InternalObject == _GameData.PlayerShip )
				return -1;
			if ( b.InternalObject == _GameData.PlayerShip )
				return 1;
				
			// Base on size of object
			const areaA:int = a.InternalObject.Width * a.InternalObject.Height;
			const areaB:int = b.InternalObject.Width * b.InternalObject.Height;
			
			if ( areaA < areaB )
				return -1;
				
			if ( areaB < areaA )
				return 1;
				
			return 0;
		}
		
		private function onMouseClick( e:MouseEvent ):void
		{
			// Game hasn't started yet
			if ( !_GameData.Scene.Layers[ "objects" ] )
			{
				return;
			}
			
			const mouseX:int = _GameData.Scene.GameStage.mouseX;
			const mouseY:int = _GameData.Scene.GameStage.mouseY;
			const worldX:int = mouseX - _GameData.Scene.Layers[ "objects" ].X;
			const worldY:int = -mouseY + _GameData.Scene.Layers[ "objects" ].Y;
			
			var over:Vector.<SceneNode> = _GameData.Scene.pointCheck( worldX, worldY, CollisionFlags.ACTORS );
			
			if ( over.length > 0 )
			{
				over.sort( sortOver );
			
				determineInteraction( over[ 0 ] );
			}
			else
			{
				for each ( var controller:IUIController in _Controllers )
				{
					controller.clear();
				}
			}
		}
	}
}