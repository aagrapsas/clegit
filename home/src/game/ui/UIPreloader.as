package game.ui 
{
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.ui.misc.UILoadingBar;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UIPreloader extends Sprite implements ITickable, IDestroyable
	{	
		public static const ASSET_SYSTEM:String = "assets";
		public static const ANIMATION_SYSTEM:String = "animations";
		public static const AUDIO_SYSTEM:String = "audio";
		public static const SWF_SYSTEM:String = "swfs";
		public static const DONE:String = "done";
		
		private var _GameData:HomeGameData;
		private var _Stage:Stage;
		
		private var _CurrentlyLoading:String;
		private var _PreviousText:String;
		
		private static const SHOULD_GO_TO_FULLSCREEN:Boolean = false;
		private var _ProgressBar:UILoadingBar;		
		private var _ProgressBarMovieClip:MovieClip;
		
		[Embed(source = '../../../bin/swf/preloader.swf', symbol = "UILoadingBarSWF")]
		private var _ProgressBarClass:Class;
		
		[Embed(source = '../../../bin/swf/preloader.swf', symbol = "UIPreloaderBGSWF")]
		private var _BackgroundClass:Class;
		
		private var _Background:Bitmap;
		private var _BackgroundWidth:int;
		
		private var _Extracting:String;
		private var _ExtractingPercentage:int = 80;
		
		public function UIPreloader( gameData:HomeGameData, stage:Stage ) 
		{
			_GameData = gameData;
			_Stage = stage;
			
			_GameData.Scene.addTickable( this );
			
			_Stage.addChild( this );
			
			_ProgressBarMovieClip = new _ProgressBarClass();
			_ProgressBar = new UILoadingBar( _ProgressBarMovieClip );
			
			_Background = new _BackgroundClass() as Bitmap;
			_BackgroundWidth = _Background.width;
			
			this.addChild( _Background );
			this.addChild( _ProgressBarMovieClip );
		}
		
		public function setLoading( type:String ):void
		{
			_CurrentlyLoading = type;
		}
		
		public function tick( deltaTime:Number ):void
		{
			var text:String = "";
			var percentage:int = 0;
			
			switch( _CurrentlyLoading )
			{
				case ANIMATION_SYSTEM:
					text = "Loading animations";
					percentage = 10;
					break;
				case AUDIO_SYSTEM:
					text = "Loading audio";
					percentage = 20;
					break;
				case ASSET_SYSTEM:
					text = "Loading " + _GameData.AssetManager.getLoadingAssetNames();
					
					percentage = 60;
					
					if ( text == "Loading " )
					{
						text = _Extracting ? "Extracting " + _Extracting : "Starting extraction...";
						percentage = Math.min( _ExtractingPercentage, 95 );
						_ExtractingPercentage += 2;
					}
					
					break;
				case SWF_SYSTEM:
					text = "Loading game swfs";
					percentage = 95;
					break;
				case DONE:
					text = "Loading game!";
					percentage = 100;
					// _Stage.addEventListener( KeyboardEvent.KEY_DOWN, onEnterKeyDown );
					break;
			}
			
			if ( text != _PreviousText )
			{
				_ProgressBar.addLoaded( text );
				_ProgressBar.setPercentage( percentage );
				
				_PreviousText = text;
			}
			
			_ProgressBarMovieClip.x = ( _Stage.stageWidth / 2 ) - ( 275 / 2 );
			_ProgressBarMovieClip.y = ( _Stage.stageHeight / 2 ) - ( _ProgressBar.clip.height / 2 );
			
			this.x = 0;
			this.y = 0;
			
			var diffWidth:int = 0;
			
			if ( _Stage.stageWidth > _BackgroundWidth )
			{
				diffWidth = _Stage.stageWidth - _BackgroundWidth;
			}
			
			_Background.x = 0;
			_Background.y = 0;
			
			_Background.width = _Stage.stageWidth;
			_Background.height = _Stage.stageHeight + diffWidth;
			
			if ( _CurrentlyLoading == DONE )
			{
				this.dispatchEvent( new Event( Event.COMPLETE ) );
			}
		}
		
		public function extractionListener( asset:String ):void
		{
			_Extracting = asset;
		}
		
		public function onEnterKeyDown( e:KeyboardEvent ):void
		{
			if ( e.keyCode == Keyboard.ENTER )
			{
				_Stage.removeEventListener( KeyboardEvent.KEY_DOWN, onEnterKeyDown );
				
				if ( SHOULD_GO_TO_FULLSCREEN )
					_GameData.Scene.setFullScreen( true );
				
				this.dispatchEvent( new Event( Event.COMPLETE ) );
			}
		}
		
		public function destroy():void
		{
			_Stage.removeChild( this );
			_GameData.Scene.removeTickable( this );
			
			this.removeChild( _ProgressBar.clip );
			
			_ProgressBar = null;
			
			_GameData = null;
			_Stage = null;
		}
	}
}