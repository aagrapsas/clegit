package game.ui.filters 
{
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PlayerIndicatorFilter
	{
		public function PlayerIndicatorFilter() 
		{
			
		}
		
		public static function getFilter():GlowFilter
		{
			var filter:GlowFilter = new GlowFilter();
			filter.blurX = 3;
			filter.blurY = 3;
			
			filter.strength = 0.62;	// 62%
			
			filter.color = 0x3AC8BA;	// teal
			
			filter.quality = BitmapFilterQuality.HIGH;
			
			return filter;
		}
	}
}