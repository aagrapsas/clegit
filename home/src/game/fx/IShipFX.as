package game.fx 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.misc.IPoolable;
	import flash.display.Sprite;
	import flash.events.IEventDispatcher;
	import game.actors.SpaceShip;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IShipFX extends IPoolable, ITickable
	{
		function get drawObject():Sprite;
	}
}