package game.fx 
{
	import engine.Actor;
	import engine.collision.CollisionInfo;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.misc.GenericPool;
	import engine.misc.IPoolable;
	import engine.misc.MathUtility;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import game.HomeGameData;
	import game.misc.FontSystem;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class WorldNumber extends Sprite implements IPoolable, ITickable, ISceneNotifiable
	{
		private var _Pool:GenericPool;
		private var _GameData:HomeGameData;
		
		private var _StartX:int;
		private var _StartY:int;
		private var _EndX:int;
		private var _EndY:int;
		
		private var _TargetX:int;
		private var _TargetY:int;
		
		private var _StartAlpha:Number = 1.0;
		private var _EndAlpha:Number = 0.0;
		
		private var _Accumulator:Number = 0;
		private var _Duration:Number = 1.5;
		
		private var _Movement:int = 10;
		
		private var _IsAlive:Boolean = false;
		
		private var _ShouldInterpolate:Boolean = false;
		
		private var _Text:TextField;
		private var _TextFormat:TextFormat;
		
		private var _Dir:Point;
		
		private var _ShouldForceToScreen:Boolean = false;
		
		public function WorldNumber() 
		{
			_Text = new TextField();
			_Text.embedFonts = true;
			_TextFormat = new TextFormat();
			
			_TextFormat.align = "center";
			_TextFormat.size = "18";
			_TextFormat.bold = true;
			_TextFormat.color = 0xFFFF00;
			_TextFormat.font = FontSystem.MYRIAD_PRO;
			
			_Text.defaultTextFormat = _TextFormat;
			
			_Dir = new Point();
			
			this.addChild( _Text );
		}
		
		public function spawn( text:String, x:int, y:int, color:uint = 0xFFFF00, duration:Number = 1.5, shouldForceToScreen:Boolean = false ):void
		{			
			_Duration = duration;
			
			_Text.textColor = color;
			
			_Accumulator = 0;
			
			_ShouldInterpolate = true;
			
			_Text.text = text;
			
			_Text.width = _Text.textWidth * 2;
			_Text.height = _Text.textHeight * 2;
			
			this.x = x - _Text.width / 2;
			this.y = y - _Text.height / 2;
			
			_StartX = this.x;
			_StartY = this.y;
			
			_EndX = this.x;
			_EndY = this.y - _Movement;
			
			_TargetX = this.x;
			_TargetY = _StartY;
			
			_ShouldForceToScreen = shouldForceToScreen;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !_ShouldInterpolate )
			{
				return;
			}
			
			_Accumulator += deltaTime;
			
			var lerpAlpha:Number = Math.min( _Accumulator / _Duration, 1.0 );
			
			this.y = MathUtility.easeInInt( _StartY, _EndY, lerpAlpha );
			_TargetY = this.y;
			
			this.alpha = MathUtility.easeIn( _StartAlpha, _EndAlpha, lerpAlpha );
			
			if ( _Accumulator >= _Duration )
			{
				_ShouldInterpolate = false;
				_Pool.push( this );
			}
			
			if ( _ShouldForceToScreen && isInBounds() == false )
			{
				forceToBounds();
			}
		}
		
		private function isInBounds():Boolean
		{
			const width:int = _GameData.Scene.Display.stage.stageWidth;
			const height:int = _GameData.Scene.Display.stage.stageHeight;
			const halfWidth:int = width / 2;
			const halfHeight:int = height / 2;
			
			var x:int = -_GameData.ActiveCamera.X;
			var y:int = -_GameData.ActiveCamera.Y;	// screenspace
			
			// Screen coordinates
			var left:int = x - halfWidth;
			var right:int = x + halfWidth;
			var top:int = y - halfHeight;
			var bottom:int = y + halfHeight;
			
			if ( _TargetX < left || _TargetX > right || _TargetY < top || _TargetY > bottom )
			{
				return false;
			}
			
			return true;
		}
		
		private function forceToBounds():void
		{
			// Screen
			const width:int = _GameData.Scene.Display.stage.stageWidth;
			const height:int = _GameData.Scene.Display.stage.stageHeight;
			const halfWidth:int = width / 2;
			const halfHeight:int = height / 2;
			
			var x:int = -_GameData.ActiveCamera.X;
			var y:int = _GameData.ActiveCamera.Y;
			
			// World
			var left:int = x - halfWidth;
			var right:int = x + halfWidth;
			var top:int = y + halfHeight;
			var bottom:int = y - halfHeight;			
			
			var intersection:CollisionInfo;
			
			// LEFT
			intersection = MathUtility.getLineIntersection( x, y, _TargetX, -_TargetY, left, top, left, bottom );
			
			var intersectX:int = 0;
			var intersectY:int = 0;
			
			if ( !intersection.DidCollide )
			{
				// RIGHT
				intersection = MathUtility.getLineIntersection( x, y, _TargetX, -_TargetY, right, top, right, bottom );
				
				if ( !intersection.DidCollide )
				{
					// TOP
					intersection = MathUtility.getLineIntersection( x, y, _TargetX, -_TargetY, left, top, right, top );
					
					if ( !intersection.DidCollide )
					{
						// BOTTOM
						intersection = MathUtility.getLineIntersection( x, y, _TargetX, -_TargetY, left, bottom, right, bottom );
					}
				}
			}
			
			if ( intersection && intersection.DidCollide )
			{	
				intersectX = intersection.X;
				intersectY = intersection.Y;
				
				_Dir.x = _TargetX - x;
				_Dir.y = -_TargetY - y;
				_Dir.normalize( 1 );
				
				var offsetX:int = _Dir.x > 0 ? ( ( this.width - _Text.textWidth + 5 ) / 2 ) + _Text.textWidth : -( this.width - _Text.textWidth + 5 ) / 2;
				
				this.x = intersectX - ( _Dir.x * offsetX );
				this.y = -intersectY + ( _Dir.y * ( this.height ) );
			}
		}
		
		public function resurrect():void
		{
			if ( _IsAlive )
				return;
			
			_ShouldInterpolate = false;
				
			_GameData.Scene.addWorldObject( this );
			_GameData.Scene.addTickable( this );
			
			this.alpha = 0;
			
			_IsAlive = true;
		}
		
		public function suspend():void 
		{
			if ( !_IsAlive )
				return;
			
			_GameData.Scene.removeWorldObject( this );
			_GameData.Scene.removeTickable( this );
			
			_IsAlive = false;
		}
		
		public function set pool( value:GenericPool ):void
		{
			_Pool = value;
		}
		
		public function set data( value:HomeGameData ):void
		{
			_GameData = value;
		}
		
		public function handleSceneTransition():void 
		{
			if ( _IsAlive )
			{
				_Pool.push( this );
			}
		}
		
		public function destroy():void
		{
			suspend();
		}
	}
}