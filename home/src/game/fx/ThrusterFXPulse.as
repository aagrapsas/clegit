package game.fx 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ThrusterFXPulse extends Bitmap implements ITickable, IDestroyable
	{		
		private var _GameData:HomeGameData;
		private var _Parent:SpaceShip;
		
		private var _AlphaStep:Number = 0.08;
		private var _DecelStep:Number = 0.12;

		private var _IsAccelerating:Boolean = false;
		
		public function ThrusterFXPulse( parent:SpaceShip, gameData:HomeGameData, asset:String, offsets:Vector.<Point> ) 
		{
			_Parent = parent;
			_GameData = gameData;
			
			if ( asset )
				build( asset, offsets );
		}
		
		private function build( asset:String, offsets:Vector.<Point> ):void
		{		
			_GameData.Scene.addTickable( this );
			
			const bitmap:Bitmap = _GameData.AssetManager.getImage( asset ) as Bitmap;
			const data:BitmapData = bitmap.bitmapData;
			const scale:Number = _Parent.Info.Chassis.ExhaustScale > 0 ? _Parent.Info.Chassis.ExhaustScale : 1;
			
			var left:int = 0;
			var right:int = _Parent.Display.cachedWidth;
			var top:int = 0;
			var bottom:int = _Parent.Display.cachedHeight;
			
			// @TODO This could be more tightly bound if we offset the bitmap or something
			
			for each ( var offset:Point in offsets )
			{
				if ( offset.x < left )
					left = offset.x;
					
				if ( offset.x > right )
					right = offset.x;
					
				if ( offset.y < top )
					top = offset.y;
					
				if ( offset.y > bottom )
					bottom = offset.y;
			}
			
			right += data.width;
			bottom += data.height;
			
			this.bitmapData = new BitmapData( ( right - left ) * scale, ( bottom - top ) * scale, true, 0x00ffffff );
			
			for each ( var offsetDraw:Point in offsets )
			{
				var matrix:Matrix = new Matrix();
				matrix.identity();
				
				matrix.scale( scale, scale );
				
				matrix.translate( offsetDraw.x, offsetDraw.y );
				
				this.bitmapData.draw( data, matrix, null, null, null, true );
			}
			
			_Parent.Display.addChild( this );
			
			this.alpha = 0;
		}
		
		public function update( isAccelerating:Boolean ):void
		{
			_IsAccelerating = isAccelerating;
		}
		
		public function tick( deltaTime:Number ):void
		{			
			if ( _IsAccelerating )
			{
				this.alpha = Math.min( this.alpha + _AlphaStep, 1 );
			}
			else
			{
				this.alpha = Math.max( this.alpha - _DecelStep, 0 );
			}
		}
		
		public function destroy():void
		{
			_GameData.Scene.removeTickable( this );
		}
	}
}