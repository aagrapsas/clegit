package game.fx 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipFXTypes 
	{
		public static const HYPERSPACE_JUMP_FX:String = "hyperspace";
		public static const HIT_FX:String = "hit";
		public static const DESTROY_FX:String = "destroy";
		public static const SHIELD_HIT_FX:String = "shield_hit";
	}
}