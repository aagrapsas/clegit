package game.fx 
{
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DamageFX extends Sprite implements ITickable
	{	
		private var _Explosions:Vector.<Bitmap>;
		private var _Particles:Vector.<ExplosiveParticle>;	// this will need to be a subclass
		private var _Manager:ExplosionManager;
		private var _IsAlive:Boolean = false;
		private var _ElapsedTime:Number = 0;
		private var _ShouldIncludeParticles:Boolean = false;
		
		private static const MIN_EXPLOSION:int = 3;
		private static const MAX_EXPLOSION:int = 6;
		
		private static const MIN_PARTICLE_SPEED:int = 50;
		private static const MAX_PARTICLE_SPEED:int = 100;
		
		private static const MIN_ALPHA_SPEED:Number = 0.5;
		private static const MAX_ALPHA_SPEED:Number = 1;
		
		private static const MIN_SCALE_SPEED:Number = 0.25;
		private static const MAX_SCALE_SPEED:Number = 0.5;
		
		private static const MIN_PARTICLES:int = 5;
		private static const MAX_PARTICLES:int = 10;
		private static const MIN_START_SCALE:Number = 0.35;
		private static const MAX_START_SCALE:Number = 0.50;
		
		private static const MAX_PARTICLE_ROT_SPEED:int = 10;
		private static const MIN_PARTICLE_ROT_SPEED:int = 2;
		
		private static const MAX_TRIGGER_TIME:Number = 0.2;
		
		private static const JITTER:int = 5;
		
		public function DamageFX( manager:ExplosionManager, explosions:Vector.<BitmapData>, particles:Vector.<BitmapData> ) 
		{
			_Explosions = new Vector.<Bitmap>;
			_Particles = new Vector.<ExplosiveParticle>;
			
			_Manager = manager
			
			setupInternal( explosions, particles );
		}
		
		private function setupInternal( explosions:Vector.<BitmapData>, particles:Vector.<BitmapData> ):void
		{
			var numParticles:int = ( Math.random() * ( MAX_PARTICLES - MIN_PARTICLES ) ) + MIN_PARTICLES;
			var numExplosions:int = ( Math.random() * ( MAX_EXPLOSION - MIN_EXPLOSION ) ) + MIN_EXPLOSION;
			
			// generate particles
			for ( var particleIndex:int = 0; particleIndex < numParticles; particleIndex++ )
			{
				var particleDir:Point = new Point( getRandomCoordinate(), getRandomCoordinate() );
				particleDir.normalize( 1 );
				
				var randomData:int = Math.random() * particles.length;
				var particle:ExplosiveParticle = new ExplosiveParticle( particles[ randomData ].clone() );
				particle.VelocityX = particleDir.x * MAX_PARTICLE_SPEED;
				particle.VelocityY = particleDir.y * MAX_PARTICLE_SPEED;
				
				particle.x = -particle.width / 2;
				particle.y = -particle.height / 2;
				
				particle.RotSpeed = Math.random() * ( MAX_PARTICLE_ROT_SPEED - MIN_PARTICLE_ROT_SPEED ) + MIN_PARTICLE_ROT_SPEED;
				
				_Particles.push( particle );
				
				this.addChild( particle );
			}
			
			// generate explosions
			for ( var expIndex:int = 0; expIndex < numExplosions; expIndex++ )
			{
				var xJitter:int = Math.random() * JITTER * ( Math.random() > 0.5 ? 1 : -1 );
				var yJitter:int = Math.random() * JITTER * ( Math.random() > 0.5 ? 1 : -1 );
				var expRandomData:int = Math.random() * explosions.length;
				var explosion:ExplosiveImage = new ExplosiveImage( explosions[ expRandomData ].clone() );
				explosion.x = -explosion.width / 2;
				explosion.y = -explosion.height / 2;
				explosion.x += xJitter;
				explosion.y += yJitter;
				explosion.JitterX = xJitter;
				explosion.JitterY = yJitter;
				
				var scale:Number = Math.random() * ( MAX_START_SCALE - MIN_START_SCALE ) + MIN_START_SCALE;
				explosion.scaleX = scale;
				explosion.scaleY = scale;
				explosion.OriginalScale = scale;
				explosion.OriginalX = explosion.x;
				explosion.OriginalY = explosion.y;
				
				explosion.AlphaSpeed = -( Math.random() * ( MAX_ALPHA_SPEED - MIN_ALPHA_SPEED ) + MIN_ALPHA_SPEED );
				explosion.ScaleSpeed = Math.random() * ( MAX_SCALE_SPEED - MIN_SCALE_SPEED ) + MIN_SCALE_SPEED;
				
				explosion.TriggerAt = Math.random() * MAX_TRIGGER_TIME;
				
				_Explosions.push( explosion );
				
				this.addChild( explosion );
			}
		}
		
		private function getRandomCoordinate():int { return Math.random() * 1000 * ( Math.random() > 0.5 ? 1 : -1 ); }
		
		public function explode( shouldIncludeDebris:Boolean = true ):void
		{
			_IsAlive = true;
			_ElapsedTime = 0;
			
			_ShouldIncludeParticles = shouldIncludeDebris;
			
			for each ( var explosion:ExplosiveImage in _Explosions )
			{
				explosion.x = -explosion.width / 2 ;
				explosion.y = -explosion.height / 2;
				
				explosion.x += explosion.JitterX;
				explosion.y += explosion.JitterY;
			}
			
			if ( !_ShouldIncludeParticles )
			{
				for each ( var particle:ExplosiveParticle in _Particles )
				{
					particle.alpha = 0;
				}
			}
		}
		
		public function reset():void
		{
			for each ( var explosion:ExplosiveImage in _Explosions )
			{
				explosion.scaleX = explosion.OriginalScale;
				explosion.scaleY = explosion.OriginalScale;
				explosion.alpha = 0;
				explosion.HasBeenTriggered = false;
			}
			
			for each ( var particle:ExplosiveParticle in _Particles )
			{
				particle.alpha = 1;
				particle.rotation = 0;
				particle.x = -particle.width / 2;
				particle.y = -particle.height / 2;
			}
			
			_ElapsedTime = 0;
		}
		
		public function tick( deltaTime:Number ):void
		{		
			if ( !_IsAlive )
			{
				return;
			}
			
			var zeroAlphaCount:int = 0;
			var haveAllExplosionsTriggered:Boolean = true;
			
			for each ( var explosion:ExplosiveImage in _Explosions )
			{
				if ( !explosion.HasBeenTriggered && _ElapsedTime >= explosion.TriggerAt )
				{
					explosion.HasBeenTriggered = true;
					explosion.alpha = 1;
				}
				
				if ( explosion.HasBeenTriggered )
				{
					explosion.scaleX = Math.min( explosion.scaleX + explosion.ScaleSpeed * deltaTime, 1 );
					explosion.scaleY = Math.min( explosion.scaleY + explosion.ScaleSpeed * deltaTime, 1 );
					explosion.alpha = Math.max( explosion.alpha + explosion.AlphaSpeed * deltaTime, 0 );
					
					explosion.x = -explosion.width / 2;
					explosion.y = -explosion.height / 2;
					
					explosion.x += explosion.JitterX;
					explosion.y += explosion.JitterY;
					
					if ( explosion.alpha <= 0 )
					{
						zeroAlphaCount++;
					}
				}
				else
				{
					haveAllExplosionsTriggered = false;
				}
			}
			
			if ( _ShouldIncludeParticles )
			{
				for each ( var particle:ExplosiveParticle in _Particles )
				{
					particle.alpha = Math.max( particle.alpha - 0.5 * deltaTime, 0 );
					particle.rotation += 2;
					particle.x += particle.VelocityX * deltaTime;
					particle.y += particle.VelocityY * deltaTime;
				}
			}
			
			_ElapsedTime += deltaTime;
			
			if ( haveAllExplosionsTriggered && zeroAlphaCount == _Explosions.length )
			{
				_IsAlive = false;
				_Manager.freeExplosion( this );
			}
		}
	}
}

import flash.display.Bitmap;
import flash.display.BitmapData;

class ExplosiveParticle extends Bitmap
{
	public var VelocityX:int;
	public var VelocityY:int;
	public var RotSpeed:int;
	
	public function ExplosiveParticle( bitmapData:BitmapData )
	{
		super( bitmapData, "auto", true );
	}
}

class ExplosiveImage extends Bitmap
{
	public var AlphaSpeed:Number;
	public var ScaleSpeed:Number;
	
	public var OriginalScale:Number;
	public var OriginalX:int;
	public var OriginalY:int;
	public var HasBeenTriggered:Boolean = false;
	public var TriggerAt:Number;
	
	public var JitterX:int;
	public var JitterY:int;
	
	public function ExplosiveImage( bitmapData:BitmapData )
	{
		super( bitmapData, "auto", true );
	}
}