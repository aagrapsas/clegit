package game.fx 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.GenericPool;
	import engine.misc.IPoolable;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipMovieClipFX extends Sprite implements ITickable, IPoolable
	{
		private var _Fx:MovieClip;
		private var _Ship:SpaceShip;
		
		private var _Pool:GenericPool;
		
		private var _IsPlaying:Boolean = false;
		
		public function ShipMovieClipFX() 
		{
					
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
		
		public function suspend():void
		{
			if ( _IsPlaying )
				stop();
			
			_Ship = null;
		}
		
		public function resurrect():void
		{
			
		}
		
		public function play( ship:SpaceShip ):void
		{
			_Ship = ship;
			
			if ( !_IsPlaying )
			{
				_Ship.Display.addChild( this );
				_Ship.AttachedDisplayObjects.push( this );
				
				_Fx.visible = true;
			}
			
			_Fx.gotoAndPlay( 1 );
			
			_IsPlaying = true;
		}
		
		public function stop():void
		{
			_Fx.visible = false;
			_Fx.gotoAndStop( 1 );
			
			if ( _Ship )
			{
				_Ship.Display.removeChild( this );
				_Ship.AttachedDisplayObjects.splice( _Ship.AttachedDisplayObjects.indexOf( this ), 1 );
			}
			
			_IsPlaying = false;
		}
		
		public function free():void
		{
			_Pool.push( this );
		}
		
		public function destroy():void
		{
			_Fx = null;
			_Ship = null;
		}
		
		public function set fx( value:MovieClip ):void { _Fx = value; }
		public function set pool( value:GenericPool ):void { _Pool = value; }
	}
}