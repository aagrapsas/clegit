package game.fx 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import game.actors.ShipEvent;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ShipFXPlayer implements ITickable, IDestroyable
	{
		private var _Ship:SpaceShip;
		private var _GameData:HomeGameData;
		
		public function ShipFXPlayer( ship:SpaceShip, gameData:HomeGameData ) 
		{
			_Ship = ship;
			_GameData = gameData;
			
			_Ship.addEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			
			_GameData.Scene.addTickable( this );
		}
		
		private function onShipDestroyed( e:ShipEvent ):void
		{
			_Ship.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
		
		public function playFX( type:String ):void
		{
			
		}
		
		public function destroy():void
		{
			_Ship.removeEventListener( ShipEventTypes.SHIP_DESTROYED, onShipDestroyed );
			
			_Ship = null;
			
			_GameData.Scene.removeTickable( this );
			
			_GameData = null;
		}
	}
}