package game.ai 
{
	import engine.interfaces.IGameData;
	import engine.misc.MathUtility;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CapitalShipLocomotionState implements IState
	{
		private var _Owner:SpaceShip;
		private var _GameData:IGameData;
		private var _StateMachine:IStateMachine;
		
		private static const ENGAGEMENT_ANGLE:int = 2;
		
		public function CapitalShipLocomotionState( owner:SpaceShip, gameData:IGameData, stateMachine:IStateMachine ) 
		{
			_Owner = owner;
			_GameData = gameData;
			_StateMachine = stateMachine;
		}	
		
		public function enterState():void { }
		
		public function updateState( deltaTime:Number ):void	
		{
			var lastSeenTarget:SpaceShip = _StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ];
			
			if ( lastSeenTarget )
			{
				const engagementDistance:int = _Owner.Node.CollisionComponent.Width > _Owner.Node.CollisionComponent.Height ? _Owner.Node.CollisionComponent.Width : _Owner.Node.CollisionComponent.Height;
				const engagementDistSquared:int = engagementDistance * engagementDistance;
				
				const deltaX:Number = ( lastSeenTarget.X - _Owner.X );
				const deltaY:Number = ( lastSeenTarget.Y - _Owner.Y );
				
				var dir:Point = new Point( deltaX, deltaY );
				dir.normalize( 1 );
				
				const distToTargetSquared:Number = deltaX * deltaX + deltaY * deltaY;
				const angleToTarget:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, _Owner.Dir.x, _Owner.Dir.y ) );
				
				// Turn ship first and then close distance
				if ( Math.abs( angleToTarget ) >= ENGAGEMENT_ANGLE )
				{
					_GameData.DebugSystem.PerfHUD.setValue( "AI_Stuff", "turning to face" );
					_Owner.faceToward( dir.x, dir.y, _Owner.Info.Engine.MaxTurnSpeed );
				}
				else	// close distance
				{
					// Slow as we reach
					if ( distToTargetSquared <= engagementDistSquared )
					{
						_Owner.VelocityY = 0;
						_Owner.VelocityX = 0;
					}
					else	// Accelerate toward
					{
						_Owner.VelocityX += _Owner.Dir.x * _Owner.Info.Engine.MaxAcceleration;
						_Owner.VelocityY += _Owner.Dir.y * _Owner.Info.Engine.MaxAcceleration;
					}						
				}
				
				const currentMagSquared:Number = _Owner.VelocityX * _Owner.VelocityX + _Owner.VelocityY * _Owner.VelocityY;
				const maxMagSquared:Number = _Owner.Info.Engine.MaxSpeed * _Owner.Info.Engine.MaxSpeed;
				
				// @TODO: I'm not sure this math actually works...
				const ratio:Number = currentMagSquared / maxMagSquared;
				
				if ( ratio > 1 )
				{
					_Owner.VelocityX /= ratio;
					_Owner.VelocityY /= ratio;
				}
				
				_Owner.X += _Owner.VelocityX * deltaTime;
				_Owner.Y += _Owner.VelocityY * deltaTime;
			}
		}
		
		public function leaveState():void { }
		
		public function destroy():void { }
	}
}