package game.ai 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class StateData 
	{
		private var _Blackboard:Dictionary;
		
		public function StateData() 
		{
			_Blackboard = new Dictionary;
		}
		
		
		public function get Blackboard():Dictionary { return _Blackboard; }
	}
}