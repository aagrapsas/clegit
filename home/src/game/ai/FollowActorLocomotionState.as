package game.ai 
{
	import engine.interfaces.IGameData;
	import engine.misc.MathUtility;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class FollowActorLocomotionState implements IState
	{		
		private var _Owner:SpaceShip;
		private var _GameData:IGameData;
		private var _StateMachine:IStateMachine;
		private var _OffsetX:int;
		private var _OffsetY:int;
		private var _Target:SpaceShip;
		
		private static const DIST_TO_SNAP_SQUARED:int = 4;
		
		public function FollowActorLocomotionState( owner:SpaceShip, gameData:IGameData, stateMachine:IStateMachine, target:SpaceShip, x:int, y:int ) 
		{
			_Owner = owner;
			_GameData = gameData;
			_StateMachine = stateMachine;
			_OffsetX = x;
			_OffsetY = y;
			_Target = target;
		}
		
		public function enterState():void
		{
			
		}
		
		public function updateState( deltaTime:Number ):void
		{
			if ( !_Target || _Target.IsDead )
			{
				_StateMachine.setLocomotionState( _StateMachine.MainLocomotionState );
				return;
			}
			
			const distX:Number = ( _Owner.X - ( _Target.X + _OffsetX ) );
			const distY:Number = ( _Owner.Y - ( _Target.Y + _OffsetY ) );
				
			// @TODO: Optimize this to not use actual distance if needed
			const distance:Number = Math.sqrt( distX * distX + distY * distY );
			
			if ( distance > DIST_TO_SNAP_SQUARED )
			{
				var dir:Point = new Point( _Target.X + _OffsetX - _Owner.X, _Target.Y + _OffsetY - _Owner.Y );
				dir.normalize( 1 );
					
				const angleToTarget:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, _Owner.Dir.x, _Owner.Dir.y ) );
				
				_Owner.faceToward( dir.x, dir.y, _Owner.Info.Engine.MaxTurnSpeed );
				
				if ( angleToTarget <= 15 )
				{
					_Owner.VelocityX += _Owner.Dir.x * _Owner.Info.Engine.MaxAcceleration;
					_Owner.VelocityY += _Owner.Dir.y * _Owner.Info.Engine.MaxAcceleration;
				}
			}
			else
			{
				// match it
				_Owner.VelocityX = 0;
				_Owner.VelocityY = 0;
				_Owner.X = _Target.X + _OffsetX;
				_Owner.Y = _Target.Y + _OffsetY;
				
				_Owner.faceToward( _Target.Dir.x, _Target.Dir.y, _Owner.Info.Engine.MaxTurnSpeed );
			}
			
			const currentMagSquared:Number = _Owner.VelocityX * _Owner.VelocityX + _Owner.VelocityY * _Owner.VelocityY;
			const maxMagSquared:Number = _Owner.Info.Engine.MaxSpeed * _Owner.Info.Engine.MaxSpeed;
			
			// @TODO: I'm not sure this math actually works...
			const ratio:Number = currentMagSquared / maxMagSquared;
			
			if ( ratio > 1 )
			{
				_Owner.VelocityX /= ratio;
				_Owner.VelocityY /= ratio;
			}
			
			_Owner.X += _Owner.VelocityX;
			_Owner.Y += _Owner.VelocityY;
		}
		
		public function leaveState():void
		{
			
		}
		
		public function destroy():void
		{
			_GameData = null;
			_Target = null;
			_Owner = null;
		}
	}
}