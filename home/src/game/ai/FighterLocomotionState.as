package game.ai 
{
	import engine.interfaces.IGameData;
	import engine.misc.MathUtility;
	import engine.render.scene.SceneNode;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class FighterLocomotionState implements IState
	{
		private var _Owner:SpaceShip;
		private var _GameData:HomeGameData;
		private var _StateMachine:IStateMachine;
		
		private var _EngageTurnDistanceSqrd:int = 100;
		private var _DistanceBeforeTurnSqrd:int = 90000;
		private var _TurnScale:Number = 0.65;
		
		private var _ChaseOffsetX:int;
		private var _ChaseOffsetY:int;
		
		private var _BunchingRepulsion:Number = 0.1;
		
		public function FighterLocomotionState( owner:SpaceShip, gameData:IGameData, stateMachine:IStateMachine ) 
		{
			_Owner = owner;
			_GameData = gameData as HomeGameData;
			_StateMachine = stateMachine;
			
			const minDistBeforeTurn:int = _GameData.GameConfigData[ "min_fig_8_dist" ];
			const maxDistBeforeTurn:int = _GameData.GameConfigData[ "max_fig_8_dist" ];
			
			_DistanceBeforeTurnSqrd = minDistBeforeTurn + ( ( Math.random() * maxDistBeforeTurn ) - minDistBeforeTurn );	// get from cache
			_DistanceBeforeTurnSqrd *= _DistanceBeforeTurnSqrd;	// square
			
			const maxChaseOffset:int = _GameData.GameConfigData[ "max_chase_offset" ];
			const minChaseOffset:int = _GameData.GameConfigData[ "min_chase_offset" ];
			
			_ChaseOffsetX = minChaseOffset + ( ( Math.random() * maxChaseOffset ) - minChaseOffset );
			_ChaseOffsetY = minChaseOffset + ( ( Math.random() * maxChaseOffset ) - minChaseOffset );
			_ChaseOffsetX *= Math.random() > 0.5 ? 1 : -1;
			_ChaseOffsetY *= Math.random() > 0.5 ? 1 : -1;
			
			_BunchingRepulsion = _GameData.GameConfigData[ "bunching_repel_strength" ];
		}
		
		public function enterState():void
		{
			
		}
		
		public function updateState( deltaTime:Number ):void
		{
			var lastSeenTarget:SpaceShip = _StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ];
			
			if ( lastSeenTarget )
			{	
				const deltaX:Number = ( lastSeenTarget.X - _Owner.X );
				const deltaY:Number = ( lastSeenTarget.Y - _Owner.Y );
				const distToTargetSquared:Number = deltaX * deltaX + deltaY * deltaY;
				
				var colliders:Vector.<SceneNode> = _GameData.Scene.collisionCheck( _Owner.Node, false );
				
				var dir:Point = new Point( lastSeenTarget.X - _Owner.X - _ChaseOffsetX, lastSeenTarget.Y - _Owner.Y - _ChaseOffsetY );
				dir.normalize( 1 );
				
				if ( colliders.length > 0 )
				{
					var aggregateX:int = 0;
					var aggregateY:int = 0;
					
					for each ( var collider:SceneNode in colliders )
					{
						var ship:SpaceShip = collider.InternalObject as SpaceShip;
						
						// We're only concerned with ships that are on the same team (avoiding colliding with them)
						if ( !ship || ship.Team != _Owner.Team )
						{
							continue;
						}
						
						// Only handle first one, for sanity's sake
						var dirModifier:Point = new Point( _Owner.X - ship.X, _Owner.Y - ship.Y );
						dirModifier.normalize( 1 );
						
						dir.x += ( dirModifier.x * _BunchingRepulsion );	// Add normals together to get a simple resultant
						dir.y += ( dirModifier.y * _BunchingRepulsion );	// percentage scales so it's less drastic
						
						dir.normalize( 1 );
						
						break;
					}
				}
				
				// Target is in front
				const angleToTarget:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, _Owner.Dir.x, _Owner.Dir.y ) );
				
				_Owner.Speed = Math.min( _Owner.Speed + _Owner.Info.Engine.MaxAcceleration, _Owner.Info.Engine.MaxSpeed );
				
				if ( angleToTarget < 90 )	// Target in front
				{
					if ( distToTargetSquared > _EngageTurnDistanceSqrd )	// Turn toward target
					{
						// Engage target
						_Owner.faceToward( dir.x, dir.y, _Owner.Info.Engine.MaxTurnSpeed );
						
						var moveSpeedScale:Number = 1;
						if ( angleToTarget > 15 )
						{
							moveSpeedScale = _TurnScale;
						}
						
						_Owner.VelocityX += _Owner.Dir.x * _Owner.Info.Engine.MaxAcceleration * moveSpeedScale;
						_Owner.VelocityY += _Owner.Dir.y * _Owner.Info.Engine.MaxAcceleration * moveSpeedScale;						
						//trace( "Target in front, turning to face");
					}
					else
					{
						// Fly through target
						_Owner.VelocityX += _Owner.Dir.x * _Owner.Info.Engine.MaxAcceleration;
						_Owner.VelocityY += _Owner.Dir.y * _Owner.Info.Engine.MaxAcceleration;	
						
						//trace( "Target in front & close, flying through" );
					}
				}
				else // Target is in back
				{
					if ( distToTargetSquared < _DistanceBeforeTurnSqrd )
					{
						// Fly forward
						_Owner.VelocityX += _Owner.Dir.x * _Owner.Info.Engine.MaxAcceleration;
						_Owner.VelocityY += _Owner.Dir.y * _Owner.Info.Engine.MaxAcceleration;	
						//trace( "Target behind, flying forward until appropriate distance" );
					}
					else
					{
						// Turn to face
						_Owner.faceToward( dir.x, dir.y, _Owner.Info.Engine.MaxTurnSpeed );
						
						_Owner.VelocityX += _Owner.Dir.x * _Owner.Info.Engine.MaxAcceleration * _TurnScale;
						_Owner.VelocityY += _Owner.Dir.y * _Owner.Info.Engine.MaxAcceleration * _TurnScale;
						
						//trace( "Target behind, turning to face" );
					}
				}
			}
			else
			{
				if ( _Owner.VelocityX > 0 )
				{
					_Owner.VelocityX = Math.max( 0, _Owner.VelocityX - _Owner.Info.Engine.MaxAcceleration );
				}
				else if ( _Owner.VelocityX < 0 )
				{
					_Owner.VelocityX = Math.min( 0, _Owner.VelocityX + _Owner.Info.Engine.MaxAcceleration );
				}
				
				if ( _Owner.VelocityY > 0 )
				{
					_Owner.VelocityY = Math.max( 0, _Owner.VelocityY - _Owner.Info.Engine.MaxAcceleration );
				}
				else if ( _Owner.VelocityY < 0 )
				{
					_Owner.VelocityY = Math.min( 0, _Owner.VelocityY + _Owner.Info.Engine.MaxAcceleration );
				}
			}
			
			const currentMagSquared:Number = _Owner.VelocityX * _Owner.VelocityX + _Owner.VelocityY * _Owner.VelocityY;
			const maxMagSquared:Number = _Owner.Info.Engine.MaxSpeed * _Owner.Info.Engine.MaxSpeed;
			const ratio:Number = currentMagSquared / maxMagSquared;
			
			if ( ratio > 1 )
			{
				_Owner.VelocityX /= ratio;
				_Owner.VelocityY /= ratio;
			}
			
			_Owner.X += _Owner.VelocityX * deltaTime;
			_Owner.Y += _Owner.VelocityY * deltaTime;
		}
		
		public function leaveState():void
		{
			
		}
		
		public function destroy():void
		{
			
		}
	}
}