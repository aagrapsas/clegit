package game.ai 
{
	import engine.collision.CollisionFlags;
	import engine.interfaces.IGameData;
	import engine.render.scene.SceneNode;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class IdleState implements IState
	{
		private var _Owner:SpaceShip;
		private var _AggroDistance:uint = 400;
		private var _GameData:IGameData;
		private var _StateMachine:IStateMachine;
		
		private const UPDATE_DELAY:uint = 10;
		private var _UpdateAccumulator:uint;
		
		public function IdleState( owner:SpaceShip, gameData:IGameData, stateMachine:IStateMachine ) 
		{
			_Owner = owner;
			_GameData = gameData;
			_StateMachine = stateMachine;
			
			_AggroDistance = owner.Info.Sensor.MaxRange;
		}
		
		public function enterState():void
		{
			//_StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ] = null;
		}
		
		public function updateState( deltaTime:Number ):void
		{	
			// When idling, no need to update every frame
			if ( _UpdateAccumulator++ > UPDATE_DELAY )
			{
				_UpdateAccumulator = 0;
				
				var colliders:Vector.<SceneNode> = _GameData.Scene.rectCheck( _Owner.X, _Owner.Y, _AggroDistance * 2, _AggroDistance * 2, CollisionFlags.ACTORS );			
				
				if ( colliders.length > 0 )
				{					
					// Find first valid target and fire everything at it
					for each ( var collider:SceneNode in colliders )
					{
						var spaceObj:SpaceShip = collider.InternalObject as SpaceShip;
						
						// If there's an enemy, attack
						if ( spaceObj && spaceObj.Team != _Owner.Team )
						{
							_StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ] = spaceObj;
							_StateMachine.goToState( StateTypes.ATTACK_STATE );
							break;
						}
					}
				}
			}
		}
		
		public function leaveState():void
		{
			
		}
		
		public function destroy():void
		{
			
		}
		
		public function toString():String	{ return "idle"; }
	}
}