package game.ai 
{
	import engine.collision.CollisionFlags;
	import engine.interfaces.IGameData;
	import engine.misc.MathUtility;
	import engine.render.scene.SceneNode;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	import game.controllers.AIController;
	import game.gameplay.CombatSystem;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AttackState implements IState
	{	
		private var _Owner:SpaceShip;
		private var _AggroDistance:uint = 400;
		private var _AggroDistanceSquared:uint = 160000;
		private var _GameData:HomeGameData;
		private var _LastSeenTarget:SpaceShip;
		private var _StateMachine:IStateMachine;
		
		private var _AttackTarget:SpaceShip;
		
		private var _ShouldSeek:Boolean = true;	// not currently dynamic, this prevents ships from going idle as long as there are enemies in the world
		
		public function AttackState( owner:SpaceShip, gameData:HomeGameData, stateMachine:IStateMachine, attackTarget:SpaceShip = null ) 
		{
			_Owner = owner;
			_GameData = gameData;
			_StateMachine = stateMachine;
			
			_AggroDistance = owner.Info.Sensor.MaxRange;
			_AggroDistanceSquared = _AggroDistance * _AggroDistance;
			
			_AttackTarget = attackTarget;
		}
		
		public function enterState():void
		{
			
		}
		
		public function updateState( deltaTime:Number ):void
		{
			var shouldFindNewTarget:Boolean = true;
			
			var lastSeenTarget:SpaceShip;
			
			if ( _AttackTarget )
			{
				// @TODO: clean this up, you shouldn't have to set the blackboard each frame
				lastSeenTarget = _AttackTarget;
				shouldFindNewTarget = false;
				
				_StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ] = _AttackTarget;
			}
			else
			{
				 lastSeenTarget = _StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ];
			}
			
			if ( lastSeenTarget && !lastSeenTarget.IsDead )
			{
				const deltaX:Number = ( lastSeenTarget.X - _Owner.X );
				const deltaY:Number = ( lastSeenTarget.Y - _Owner.Y );
				const distToTarget:Number = deltaX * deltaX + deltaY * deltaY;
				
				if ( distToTarget <= _Owner.AggroDistanceSquared )
				{
					shouldFindNewTarget = false;
					
					shootAt( lastSeenTarget );
				}
			}
			
			if ( shouldFindNewTarget )
			{
				// Find all colliders
				var colliders:Vector.<SceneNode> = _GameData.Scene.rectCheck( _Owner.X, _Owner.Y, _Owner.AggroDistance, _Owner.AggroDistance, CollisionFlags.ACTORS );			
				
				if ( colliders.length > 0 )
				{					
					// Find first valid target and fire everything at it
					for each ( var collider:SceneNode in colliders )
					{
						var spaceObj:SpaceShip = collider.InternalObject as SpaceShip;
						
						if ( spaceObj && spaceObj.Team != _Owner.Team )
						{
							shootAt( spaceObj );
							
							_StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ] = spaceObj;
							shouldFindNewTarget = false;
							
							break;
						}
					}
				}
			}
			
			// @TODO make support more than 2 teams
			if ( shouldFindNewTarget && _ShouldSeek && _GameData.PlayerShip )
			{
				if ( _GameData.PlayerShip.Team != _Owner.Team )
				{
					// Seek toward player
					shouldFindNewTarget = false;
					_StateMachine.SharedData.Blackboard[ BlackboardDataTypes.LAST_SEEN_TARGET ] = _GameData.PlayerShip;
				}
				else
				{
					if ( _GameData.CurrentScenario.EnemyAIControllers.length > 0 )
					{
						shouldFindNewTarget = false;
						var firstEnemy:SpaceShip = _GameData.CurrentScenario.EnemyAIControllers[ 0 ].Ship;
					}
				}
			}
			
			if ( _ShouldSeek == false && shouldFindNewTarget )
			{
				_StateMachine.goToState( StateTypes.IDLE_STATE );
			}
		}
		
		private function shootAt( target:SpaceShip ):void
		{
			// Cache this?
			var dir:Point = new Point( target.X - _Owner.X, target.Y - _Owner.Y );
			dir.normalize( 1 );
			var angleBetween:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, _Owner.Dir.x, _Owner.Dir.y ) );
			
			for each ( var gameSystem:CombatSystem in _Owner.CombatSystems )
			{
				if ( gameSystem.Info.FireAngle > 0 && gameSystem.Info.FireAngle < 360 )
				{
					if ( angleBetween < gameSystem.Info.FireAngle )
					{
						gameSystem.execute( _Owner, _Owner.Dir, _Owner.X, _Owner.Y, target.X, target.Y );
					}
				}
				else
				{
					gameSystem.execute( _Owner, _Owner.Dir, _Owner.X, _Owner.Y, target.X, target.Y );
				}
			}
		}
		
		public function leaveState():void
		{
			
		}
		
		public function destroy():void
		{
			
		}
		
		public function toString():String { return "attack"; }
	}
}