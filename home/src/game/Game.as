package game 
{
	import engine.assets.AdvancedAssetManager;
	import engine.assets.AnimationManager;
	import engine.audio.SoundManager;
	import engine.debug.Debug;
	import engine.debug.DebugManager;
	import engine.debug.ErrorScreen;
	import engine.debug.IDebugChannel;
	import engine.input.InputCodes;
	import engine.interfaces.ITickable;
	import engine.misc.EngineEvents;
	import engine.misc.EngineProperties;
	import engine.misc.EventBus;
	import engine.misc.LocalStorage;
	import engine.misc.PoolManager;
	import engine.misc.XMLValidatorManager;
	import engine.render.Camera;
	import engine.render.scene.SceneManager;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	import game.actors.PhysicsActorManager;
	import game.builders.PhysicsActorBuilder;
	import game.builders.ShipBuilder;
	import game.builders.ShipSpawner;
	import game.catalogue.InfoCatalogue;
	import game.catalogue.ShipComponentCatalogue;
	import game.controllers.PlayerSpaceShipController;
	import game.controllers.RoamingCameraController;
	import game.data.PlayerData;
	import game.debug.GameLogChannels;
	import game.debug.HomeDebugConfig;
	import game.external.Kongregate;
	import game.fx.WorldFXManager;
	import game.gameplay.player.OutOfBoundsManager;
	import game.misc.FontSystem;
	import game.misc.HomePoolConfig;
	import game.scenarios.Scenario;
	import game.ui.menu.UIMainMenu;
	import game.ui.menu.UIMenuContainer;
	import game.ui.misc.UIGenericPopup;
	import game.ui.misc.UIGenericPopupSlim;
	import game.ui.UIInteractionManager;
	import game.ui.UIPreloader;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Game
	{
		private var _Stage:Stage;
		private var _GameData:HomeGameData;
		
		private var _Preloader:UIPreloader;
		
		private var _PreloadedAssets:Dictionary;
		
		public function Game( stage:Stage, preloadedAssets:Dictionary = null )
		{
			_PreloadedAssets = preloadedAssets;
			
			_Stage = stage;
			_Stage.scaleMode = StageScaleMode.NO_SCALE;
			_Stage.align = StageAlign.TOP_LEFT;
			
			initialize();
		}	
		
		private function initialize():void
		{
			// We want debug reporting from the get-go
			Debug.setup( _Stage );
			
			_GameData = new HomeGameData();
			
			_GameData.Scene = new SceneManager( _Stage );
			_GameData.EngineEvents = new EventBus();
			
			if ( _PreloadedAssets == null )
			{
				_Preloader = new UIPreloader( _GameData, _Stage );
				_Preloader.setLoading( UIPreloader.ASSET_SYSTEM );
			}
			
			_GameData.DebugSystem = new DebugManager;
			_GameData.DebugSystem.initialize( _Stage );
			_GameData.DebugSystem.Config = new HomeDebugConfig( _GameData );
			_GameData.Scene.addTickable( _GameData.DebugSystem.PerfHUD );

			_GameData.Properties = new EngineProperties();
			setupFlashVars();
			
			CONFIG::kong
			{
				Debug.debugLog( GameLogChannels.DEBUG, "Initializing Kongregate API" );
				Kongregate.setup( _Stage );
				
			}
			
			Debug.debugLog( GameLogChannels.DEBUG, "Beginning engine initialization" );
			
			Debug.perfMon = _GameData.DebugSystem.PerfMon;
			
			_GameData.ActiveCamera = new Camera( _GameData, _Stage.stageWidth, _Stage.stageHeight );
			_GameData.Scene.addTickable( _GameData.ActiveCamera );
			
			_GameData.DebugSystem.toggleDebugPanel();
			_GameData.DebugSystem.togglePerfHud();
			
			_GameData.AssetManager = new AdvancedAssetManager( _GameData, _PreloadedAssets );
			
			if ( _PreloadedAssets == null )
			{
				_GameData.AssetManager.registerSwiftRiverCallback( _Preloader.extractionListener )
				_GameData.AssetManager.addEventListener( Event.COMPLETE, onAssetsLoaded );
				_GameData.AssetManager.loadPreloadAssets();
			}
			else
			{
				onAssetsLoaded( null );
			}
			
			_GameData.Scene.addTickable( _GameData.AssetManager );
		}
		
		private function setupFlashVars():void
		{
			var parameters:Object = _Stage.loaderInfo.parameters;
			
			_GameData.Properties.resourcePath = parameters.resourcePath;
			_GameData.Properties.buildNumber = parameters.buildnumber;
			
			validateProperty( "resourcePath", _GameData.Properties.resourcePath );
			validateProperty( "buildnumber", _GameData.Properties.buildNumber );
		}
		
		private function validateProperty( property:String, value:String ):void
		{
			if ( !property )
			{
				Debug.errorLog( GameLogChannels.DATA, "Rejecting property " + property + " because it was not found in the FlashVars!" );
				return;
			}
			
			Debug.debugLog( GameLogChannels.DEBUG, "Assigning property " + property + " to " + value );
		}
		
		private function onAssetsLoaded( e:Event ):void
		{
			_GameData.AssetManager.removeEventListener( Event.COMPLETE, onAssetsLoaded );
			
			_GameData.SoundSystem = new SoundManager( _GameData );
			_GameData.SoundSystem.deserialize( _GameData.AssetManager.getXML( "audio_config" ) );
			
			_GameData.Scene.addTickable( _GameData.SoundSystem, false );
			
			_GameData.AnimationSystem = new AnimationManager( _GameData );
			_GameData.AnimationSystem.deserialize( _GameData.AssetManager.getXML( "animations" ) );
			
			if ( _PreloadedAssets == null )
			{
				_Preloader.setLoading( UIPreloader.ANIMATION_SYSTEM );

				_Preloader.addEventListener( Event.COMPLETE, onStartPlay );
				_Preloader.setLoading( UIPreloader.DONE );
			}
			else
			{
				onStartPlay( null );
			}
		}
		
		private function onStartPlay( e:Event ):void
		{
			if ( _PreloadedAssets == null )
			{
				_Preloader.removeEventListener( Event.COMPLETE, onStartPlay );
				_Preloader.destroy();
				_Preloader = null;
			}
			
			loadGame();
		}
		
		private function loadGame():void
		{		
			var gameXML:XML = _GameData.AssetManager.getXML( "game_config" );
			
			_GameData.InputMap = new Dictionary();
			_GameData.GameConfigData = new Dictionary();
			
			// Populate input codes
			for each ( var input:XML in gameXML.controls.* )
			{
				_GameData.InputMap[ String( input.@type ) ] = InputCodes.getCode( String( input.@value ) );
			}
			
			// Populate game configs
			for each ( var config:XML in gameXML.configs.* )
			{
				_GameData.GameConfigData[ String( config.@type ) ] = String( config.@value );
			}
			
			const worldExtremes:int = _GameData.GameConfigData[ "world_extremes" ];
			
			_GameData.Scene.setupQuadTree( worldExtremes * 2, worldExtremes * 2, 100 );
			
			setupDebugChannels();
						
			_GameData.Pools = new PoolManager( _GameData, new HomePoolConfig() );
			
			_GameData.Spawner = new ShipSpawner( _GameData );
			_GameData.Spawner.Builder = new ShipBuilder;
			
			_GameData.InfoCatalog = new InfoCatalogue( _GameData );
			_GameData.ShipCompCatalog = new ShipComponentCatalogue( _GameData );
			_GameData.InfoCatalog.DifficultyClassCatalog.initialize();
			
			_GameData.XMLValidators = new XMLValidatorManager();
			_GameData.XMLValidators.deserialize( _GameData.AssetManager.getXML( "validators" ), _GameData );
			
			_GameData.LocalDatabase = new LocalStorage();
			
			_GameData.Scene.isGameReady = true;
			
			_GameData.GameEvents = new EventBus();
			
			var fbc:int = _GameData.GameConfigData[ "starting_fbc" ];
			var salvage:int = _GameData.GameConfigData[ "starting_salvage" ];
			
			_GameData.ClientData = new PlayerData( _GameData );
			
			_GameData.ClientData.salvage = salvage;
			_GameData.ClientData.fbc = fbc;
			
			var startLevel:String = String( gameXML.@startLevel );
			
			if ( startLevel )
			{
				_GameData.CurrentScenario = new Scenario( _GameData );
				_GameData.CurrentScenario.deserialize( _GameData.AssetManager.getXML( startLevel ) );
				_GameData.CurrentScenario.setup();
			}
			
			/*
			_GameData.CurrentScenario = new Scenario( _GameData );
			_GameData.CurrentScenario.deserialize( _GameData.AssetManager.getXML(  ) );
			_GameData.CurrentScenario.setup();
			*/
			
			_GameData.Scene.addTickable( new UIInteractionManager( _GameData ) );
			
			// Out of bounds indicator
			_GameData.Scene.addTickable( new OutOfBoundsManager( _GameData ) );
			
			_GameData.WorldFX = new WorldFXManager( _GameData );
			
			// _GameData.PhysActorManager = new PhysicsActorManager( _GameData );
			// _GameData.PhysActorManager.Spawner = new PhysicsActorBuilder( _GameData );
			
			// _GameData.Scene.addTickable( _GameData.PhysActorManager );
			
			// Set our audio attenuation distance
			_GameData.SoundSystem.setAttenuationDistance( _GameData.GameConfigData[ "max_audio_distance" ] );
			
			// Full-screen hotkey
			_Stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp, false, 0, true );
			
			_GameData.DebugSystem.sortDebugSpriteToTop();
			
			setupResizeListener();
			
			if ( !startLevel )
			{
				// UIMainMenu now has control!
				_GameData.Scene.addTickable( new UIMenuContainer( _GameData ) );
			}
		}
		
		private function setupResizeListener():void
		{
			_Stage.addEventListener( Event.RESIZE, onResize );
		}
		
		private function onResize( e:Event ):void
		{
			_GameData.GameEvents.dispatchEvent( EngineEvents.WINDOW_RESIZE, null );
		}
		
		// @TODO: improve this through injection
		private function setupDebugChannels():void
		{
			CONFIG::release
			{
				if ( _GameData.GameConfigData[ "show_error_screen" ] == "false" )
				{
					var removeIndex:int = 0;
					var channels:Vector.<IDebugChannel> = Debug.debugReporter.errorChannels;
					
					for ( var i:int = 0; i < channels.length; i++ )
					{
						var channel:IDebugChannel = channels[ i ];
						
						if ( channel is ErrorScreen )
						{
							removeIndex = i;
							break;
						}
					}
					
					channels.splice( removeIndex, 1 );
				}
			}
		}
		
		private function testStuff():void
		{

		}
		
		private function onKeyUp( e:KeyboardEvent ):void
		{
			if ( e.keyCode == _GameData.InputMap[ "full_screen" ] )
			{
				_GameData.Scene.setFullScreen( true );
			}
			else if ( e.keyCode == 192 )	// debug panel
			{
				_GameData.IsInputEnabled = !_GameData.DebugSystem.IsDebugPanelVisible;
			}
			else if ( e.keyCode == Keyboard.P && _GameData.IsInputEnabled )
			{
				if ( _GameData.Scene.IsPaused && _GameData.Scene.ActivePopup )
				{
					_GameData.Scene.clearPopup();
				}
				
				_GameData.Scene.IsPaused = !_GameData.Scene.IsPaused;
				
				if ( _GameData.Scene.IsPaused )
				{
					_GameData.Scene.addPopup( new UIGenericPopupSlim( _GameData, "GAME PAUSED", "PRESS P TO UNPAUSE", null, null, null, null, false, FontSystem.PIRULEN ) );
				}
			}
			else if ( e.keyCode == Keyboard.ESCAPE && _GameData.CurrentScenario && _GameData.CurrentScenario.isMarkedForDestroy == false )
			{
				if ( _GameData.Scene.ActivePopup != null && _GameData.Scene.ActivePopup.name == "OPTIONS" )
				{					
					_GameData.Scene.clearPopup();
					
					if ( _GameData.Scene.IsPaused )
					{
						_GameData.Scene.IsPaused = false;
					}
				}
				else if ( _GameData.Scene.ActivePopup == null )
				{
					_GameData.Scene.addPopup( new UIGenericPopupSlim( _GameData, "OPTIONS", "What would you like to do?", "Main Menu", "Retry", onMenu, onRetry ) );
				}
			}
			else if ( e.keyCode == Keyboard.F && _GameData.IsInputEnabled )
			{
				if ( !_GameData.PlayerShip )
				{
					for each ( var tickableCam:ITickable in tickables )
						{
							if ( tickableCam is RoamingCameraController)
							{
								return;
							}
						}
					
					var controller:RoamingCameraController = new RoamingCameraController( _GameData );
				}
				else
				{
					if ( _GameData.PlayerController )
					{
						_GameData.PlayerController.destroy();
						_GameData.PlayerController = null;
						
						// Adds itself to the scene currently
						var newController:RoamingCameraController = new RoamingCameraController( _GameData );
					}
					else
					{
						var tickables:Vector.<ITickable> = _GameData.Scene.Tickables;
						
						for each ( var tickable:ITickable in tickables )
						{
							if ( tickable is RoamingCameraController)
							{
								var roaming:RoamingCameraController = tickable as RoamingCameraController;
								
								roaming.destroy();
							}
						}
						
						if ( _GameData.PlayerShip )
						{
							_GameData.PlayerController = new PlayerSpaceShipController( _GameData.PlayerShip, _GameData );
							
							_GameData.ActiveCamera.centerAt( _GameData.PlayerShip.X, _GameData.PlayerShip.Y );
						}
					}				
				}
			}
		}
		
		private function onMenu():void
		{
			_GameData.CurrentScenario.breakDown();
			_GameData.CurrentScenario.destroy();
			
			_GameData.Scene.addTickable( new UIMenuContainer( _GameData ) );
		}
		
		private function onRetry():void
		{
			var scenarioName:String = _GameData.CurrentScenario.name;
			
			var xml:XML = _GameData.AssetManager.getXML( scenarioName );
			
			_GameData.CurrentScenario.breakDown();
			_GameData.CurrentScenario.destroy();
			
			_GameData.CurrentScenario = new Scenario( _GameData );
			_GameData.CurrentScenario.deserialize( xml );
			_GameData.CurrentScenario.setup();
		}
	}
}