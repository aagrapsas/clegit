package game.renderer 
{
	import engine.debug.Debug;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneLayer;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class RepeatingSceneLayer extends SceneLayer
	{
		private var _GameData:HomeGameData;
		
		public function RepeatingSceneLayer( name:String, gameData:IGameData, movementPercent:Number, imageData:BitmapData ) 
		{
			super( name, movementPercent );
			
			_GameData = gameData as HomeGameData;
			
			generate( imageData );
		}	
		
		private function generate( imageData:BitmapData ):void
		{
			for ( var i:int = 0; i < 3; i++ )
			{
				for ( var j:int = 0; j < 3; j++ )
				{
					var newImage:Bitmap = new Bitmap( imageData.clone(), (_GameData as HomeGameData).GameConfigData[ "subpixel_support" ], true );
					newImage.scaleX = 0.99;
					newImage.scaleY = 0.99;
					newImage.x = i * newImage.width;
					newImage.y = j * newImage.height;
					
					this.Display.addChild( newImage );
				}		
			}
		}
	}
}