package game.movement 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CameraLagMovement implements ITickable
	{
		private var _GameData:IGameData;
		private var _Ship:SpaceShip;
		
		private var _VelocityX:int;
		private var _VelocityY:int;
		
		//#Jeff I added these for investigating the quality of my camera snapping. uncomment them for use yourself
		//private var hits:int = 0;
		//private var misses:int = 0;
		
		public function CameraLagMovement( spaceShip:SpaceShip, gameData:IGameData ) 
		{
			// Temporarily build everything
			//_CollisionArea = new RectCollider( 0, 0, 100, 100, CollisionFlags.ACTORS );
			
			_Ship = spaceShip;
			_GameData = gameData;
			
			if ( !_GameData.ActiveCamera.IsInitialized )
				_GameData.ActiveCamera.centerAt( _Ship.X, _Ship.Y );
		}
		
		public function tick( deltaTime:Number ):void
		{			
			if ( _Ship.deltaX != 0 || _Ship.deltaY != 0 )
			{				
				_GameData.ActiveCamera.scrollXY( -_Ship.deltaX, -_Ship.deltaY );
				_Ship.clearDeltaMovement();
			}
		}
	}
}