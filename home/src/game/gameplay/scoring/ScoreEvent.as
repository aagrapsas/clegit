package game.gameplay.scoring 
{
	import adobe.utils.CustomActions;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ScoreEvent 
	{
		public var key:String;
		public var maxAmount:int;
		public var minAmount:int;
		public var maxTime:Number;
		public var minTime:Number;
		public var delay:Number;
		public var score:int;
		
		private var _Accumulator:Number = 0;
		private var _IsDelayed:Boolean = false;
		
		public function ScoreEvent( key:String, maxAmount:int, minAmount:int, maxTime:Number, minTime:Number, delay:Number, score:int ) 
		{
			this.key = key;
			this.maxAmount = maxAmount;
			this.minAmount = minAmount;
			this.maxTime = maxTime;
			this.minTime = minTime;
			this.delay = delay;
			this.score = score;
		}
		
		public function update( deltaTime:Number ):void
		{
			_Accumulator += deltaTime;
			
			if ( _IsDelayed )
			{
				if ( _Accumulator >= delay )
				{
					_IsDelayed = false;
					_Accumulator = 0;
				}
			}
		}
		
		public function getScore():int
		{
			if ( maxTime > 0 && _Accumulator >= maxTime )
			{
				// Has a max time, and has exceeded that max time, return minimum
				return minAmount;
			}
			
			if ( minTime > 0 && _Accumulator <= minTime )
			{
				// Has a minimum time for tracking score, 
				// if under, return max amount immediately
				return maxAmount;
			}
			
			if ( maxTime > 0 )
			{
				var alpha:Number = ( _Accumulator - minTime ) / ( maxTime - minTime );
				
				if ( minAmount > 0 )
				{
					// Linear scale between min and max amount
					return Math.max( alpha * maxAmount, minAmount );
				}
				else
				{
					// Linear scale between 0 and max amount
					return ( alpha * maxAmount );
				}
			}
			else
			{
				// No max time, no min time
				if ( score > 0 )
				{
					return score;	// flat score, no if's, and's, or but's
				}
			}
			
			return 0;
		}
	}
}