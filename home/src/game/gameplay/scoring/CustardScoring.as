package game.gameplay.scoring 
{
	import engine.Actor;
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.utils.Dictionary;
	import game.actors.ShipEventTypes;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.fx.WorldNumber;
	import game.HomeGameData;
	import game.misc.HomePoolConfig;
	import game.ui.hud.UIPlayerPanel;
	import game.ui.misc.UIScreenText;
	import game.ui.UIScoreDisplay;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CustardScoring implements ITickable, IDestroyable
	{	
		//public static const 
		
		private var _GameData:HomeGameData;
		
		private var _UI:UIPlayerPanel;
		
		private var _Score:int;
		
		private var _ShipScores:Dictionary;
		private var _ScoreEvents:Dictionary;
		
		private var _IsWavedBased:Boolean = false;
		private var _IsTimerBased:Boolean = false;
		
		private var _WaveCount:int = 0;
		private var _TimeAccumulator:Number = 0;
		
		private var _ScoreEventHistory:Dictionary;
		private var _ScoreFromKills:int;
		
		public function CustardScoring( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			_GameData.GameEvents.register( ShipEventTypes.SHIP_DESTROYED, handleShipDeath );
			
			_UI = new UIPlayerPanel( _GameData );
			
			// Add to UI scene
			_GameData.Scene.UILayer.addChild( _UI );
			
			_ShipScores = new Dictionary();
			_ScoreEvents = new Dictionary();
			_ScoreEventHistory = new Dictionary();
			
			var xml:XML = _GameData.AssetManager.getXML( "ship_scores" );
			
			// Populate score table
			for each ( var scoreXML:XML in xml.score )
			{
				_ShipScores[ String( scoreXML.@key ) ] = int( scoreXML.@value );
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			_UI.tick( deltaTime );
			
			
			for each ( var scoreEvent:ScoreEvent in _ScoreEvents )
			{
				scoreEvent.update( deltaTime );
			}
			
			if ( _IsTimerBased )
			{
				_TimeAccumulator += deltaTime;
				
				_UI.setLevelInfoValue( getFormattedTime( _TimeAccumulator ) );
			}
		}
		
		private function getFormattedTime( seconds:int ):String
		{
			var minutes:int = seconds / 60;
			var remainingSeconds:int = seconds - ( minutes * 60 );
			
			var secondsString:String = String( remainingSeconds );
			var minutesString:String = String( minutes );
			
			if ( secondsString.length < 2 )
			{
				var zerosToAdd:int = 2 - secondsString.length;
				
				for ( var i:int = 0; i < zerosToAdd; i++ )
				{
					secondsString = "0" + secondsString;
				}
			}
			
			if ( minutesString.length < 2 )
			{
				var zeros:int = 2 - minutesString.length;
				
				for ( var j:int = 0; j < zeros; j++ )
				{
					minutesString = "0" + minutesString;
				}
			}
			
			return minutesString + ":" + secondsString;
		}
		
		public function setScoringType( isWaveBased:Boolean ):void
		{
			_IsWavedBased = isWaveBased;
			_IsTimerBased = !isWaveBased;
			
			if ( _IsWavedBased )
			{
				_UI.setLevelInfo( "wave:" );
				_WaveCount = 0;
			}
			else
			{
				_UI.setLevelInfo( "time:" );
				_TimeAccumulator = 0;
			}
		}
		
		public function notifyWave():void
		{
			_WaveCount++;
			
			_UI.setLevelInfoValue( String( _WaveCount ) );
		}
		
		public function registerScoreEvent( event:ScoreEvent ):void
		{
			_ScoreEvents[ event.key ] = event;
		}
		
		public function triggerScoreEvent( key:String ):void
		{
			if ( !_ScoreEvents[ key ] )
			{
				Debug.errorLog( GameLogChannels.SCRIPT, "CustardScoring: attempted to trigger a score event that doesn't exist! Tried key: " + key );
				return;
			}
			
			var scoreEvent:ScoreEvent = _ScoreEvents[ key ];
			
			var score:int = scoreEvent.getScore();
			
			// Spawn in-world element
			if ( _GameData.PlayerShip )
			{
				//var worldNumber:WorldNumber = _GameData.Pools.getPool( HomePoolConfig.WORLD_NUMBER ).pop() as WorldNumber;
				//worldNumber.spawn( "+" + score + " XP " + key, _GameData.PlayerShip.X, -_GameData.PlayerShip.Y, 0x99FF00, 3.0 );
				
				var uiNumber:UIScreenText = new UIScreenText( _GameData, "+" + score + " XP " + key, _GameData.Scene.GameStage.stageWidth - 195, 65, 0x99FF00, 3.0 );
			}
			
			if ( !_ScoreEventHistory[ key ] )
			{
				_ScoreEventHistory[ key ] = score;
			}
			else
			{
				_ScoreEventHistory[ key ] += score;
			}
			
			addScore( score );
			
			delete _ScoreEvents[ key ];
		}
		
		private function addScore( amount:int ):void
		{
			_UI.addScore( amount );
			
			_Score += amount;
		}
		
		private function handleShipDeath( data:Object ):void
		{
			if ( !_GameData.PlayerShip )
			{
				return;
			}
			
			var destroyed:SpaceShip = data.destroyed;
			var destroyer:SpaceShip = data.destroyer;
			
			if ( destroyer != _GameData.PlayerShip )
			{
				return;
			}
			
			var score:int = _ShipScores[ destroyed.Info.Key ];
			
			// Add score to display
			addScore( score );
			
			_ScoreFromKills += score;
			
			var worldNumber:WorldNumber = _GameData.Pools.getPool( HomePoolConfig.WORLD_NUMBER ).pop() as WorldNumber;
			
			// Spawn FX
			worldNumber.spawn( "+" + score + " XP", destroyed.X, -destroyed.Y, 0xFFFF00, 1.5, true );	// -y for flipped y-axis
			
			// Salvage
			var salvagePercent:Number = _GameData.GameConfigData[ "kill_salvage_perc" ];
			
			var cost:int = _GameData.InfoCatalog.getShipCost( destroyed ) * salvagePercent;
			
			if ( cost > 0 )
			{
				_UI.addSalvage( cost );
				
				//var salvageNumber:WorldNumber = _GameData.Pools.getPool( HomePoolConfig.WORLD_NUMBER ).pop() as WorldNumber;
				
				//salvageNumber.spawn( "+" + cost + " Salvage", destroyed.X, -destroyed.Y - 10, 0xFF3333 );
			}
		}
		
		public function getMainScore():int
		{
			return _ScoreFromKills;
		}
		
		public function get scoreEventHistory():Dictionary { return _ScoreEventHistory };
		
		public function destroy():void
		{
			_GameData.GameEvents.unregister( ShipEventTypes.SHIP_DESTROYED, handleShipDeath );
			
			_GameData.Scene.UILayer.removeChild( _UI );
			
			_UI.destroy();
			
			_ScoreEvents = null;
			_ShipScores = null;
		}
	}
}