package game.gameplay 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class GameSystemTypes 
	{
		public static const WEAPON_SYSTEM:String = "weapon";	// active
		public static const ABILITY_SYSTEM:String = "ability";	// active/passive
	}
}