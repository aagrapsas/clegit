package game.gameplay.waypoint 
{
	import engine.Actor;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.geom.Point;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class WaypointDir extends Actor implements ITickable
	{		
		private var _GameData:HomeGameData;
		private static const DISTANCE_FROM_CENTER:int = 64;
		
		private var _Dir:Point;
		private var _PrevShipX:int;
		private var _PrevShipY:int;
		private var _PrevWaypointX:int;
		private var _PrevWaypointY:int;
		
		public function WaypointDir() 
		{
			_Dir = new Point;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !_GameData.PlayerWaypoint || !_GameData.PlayerShip )
			{
				return;
			}
			
			// If values haven't changed, do not modify the indicator
			if ( _PrevShipX == _GameData.PlayerShip.X && _PrevShipY == _GameData.PlayerShip.Y &&
				 _PrevWaypointX == _GameData.PlayerWaypoint.X && _PrevWaypointY == _GameData.PlayerWaypoint.Y )
			 {
				 return;
			 }
			
			this.X = _GameData.PlayerShip.X;
			this.Y = _GameData.PlayerShip.Y;
			
			_Dir.x = _GameData.PlayerWaypoint.X - this.X;
			_Dir.y = _GameData.PlayerWaypoint.Y - this.Y;
			_Dir.normalize( 1 );
			
			// Face toward the waypoint
			this.faceToward( _Dir.x, _Dir.y );
			
			// Move along the dir we're facing so we're on a circle-ish thing
			this.X += _Dir.x * DISTANCE_FROM_CENTER;
			this.Y += _Dir.y * DISTANCE_FROM_CENTER;
			
			_PrevShipX = _GameData.PlayerShip.X;
			_PrevShipY = _GameData.PlayerShip.Y;
			_PrevWaypointX = _GameData.PlayerWaypoint.X;
			_PrevWaypointY = _GameData.PlayerWaypoint.Y;
		}
		
		override public function set Data( value:IGameData ):void
		{
			super.Data = value;
			
			_GameData = HomeGameData( Data );
		}
		
		override public function destroy():void
		{
			super.destroy();
			
			_GameData = null;
		}
	}
}