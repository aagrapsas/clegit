package game.gameplay.waypoint 
{
	import engine.Actor;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Sprite;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Waypoint extends Actor implements ITickable
	{			
		private var _AlphaDir:int = 1;
		private static const ALPHA_MAX:Number = 0.45;
		private static const ALPHA_MIN:Number = 0.20;
		private static const ALPHA_STEP:Number = 0.01;
		
		private var _Target:SpaceShip;
		
		public function Waypoint() 
		{
			
		}
		
		public function set Target( value:SpaceShip ):void
		{
			_Target = value;
		}
		
		public function tick( deltaTime:Number ):void
		{
			this.Display.alpha += _AlphaDir * ALPHA_STEP;
			
			if ( this.Display.alpha >= ALPHA_MAX )
			{
				this.Display.alpha = ALPHA_MAX;
				_AlphaDir = -1;
			}
			else if ( this.Display.alpha <= ALPHA_MIN )
			{
				this.Display.alpha = ALPHA_MIN;
				_AlphaDir = 1;
			}
			
			if ( _Target )
			{
				this.X = _Target.X;
				this.Y = _Target.Y;
			}
		}
	}
}