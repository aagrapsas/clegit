package game.gameplay.weapons 
{
	import engine.Actor;
	import engine.interfaces.IDestroyable;
	import flash.geom.Point;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IWeaponType extends IDestroyable
	{
		// @TODO: add damage class here
		function fire( instigator:SpaceShip, facing:Point, x1:int, y1:int, x2:int, y2:int ):void;
	}
}