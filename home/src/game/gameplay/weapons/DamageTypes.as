package game.gameplay.weapons 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DamageTypes 
	{
		public static const MISSILE:String = "missile";
		public static const PROJECTILE:String = "proj";
		public static const MISC:String = "misc";
		public static const INSTANT:String = "inst";
	}
}