package game.gameplay.weapons 
{
	import engine.debug.Debug;
	import engine.interfaces.IGameData;
	import game.debug.GameLogChannels;
	import game.gameplay.GameExecuteDelay;
	import game.gameplay.IGameExecuteDelay;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class WeaponExecuteBurst implements IGameExecuteDelay
	{
		private var _Delay:Number;
		private var _BurstStart:Number = 0;
		private var _CooldownStart:Number = 0;
		private var _IsExecutingBurst:Boolean = false;
		private var _BurstDuration:Number;
		private var _BurstCooldown:Number;
		
		private var _LastExecution:Number = 0;
		
		private var _GameData:IGameData;
		
		public function WeaponExecuteBurst( gameData:IGameData, delay:Number, burstDuration:Number, burstCooldown:Number ) 
		{
			_Delay = delay;
			_BurstDuration = burstDuration;
			_BurstCooldown = burstCooldown;
			_GameData = gameData;
		}
		
		public function canExecute():Boolean 
		{	
			const elapsedBurst:Number = _GameData.Scene.GameTime - _BurstStart;
			const elapsedCoolDown:Number = _GameData.Scene.GameTime - _CooldownStart;
			
			if ( _IsExecutingBurst && elapsedBurst >= _BurstDuration )
			{
				// End burst & start cool down phase
				_BurstStart = 0;
				_CooldownStart = _GameData.Scene.GameTime;
				_IsExecutingBurst = false;
				
				return false;
			}
			
			if ( _CooldownStart > 0 && elapsedCoolDown < _BurstCooldown )
			{
				return false;
			}
			
			var elapsedTime:Number = _GameData.Scene.GameTime - _LastExecution;
			
			return ( elapsedTime >= _Delay );
		}
		
		public function markExecution():void 
		{
			_LastExecution = _GameData.Scene.GameTime;
			
			if ( _BurstStart == 0 )
			{
				_BurstStart = _LastExecution;
				_IsExecutingBurst = true;
			}
		}
		
		public function get RefreshTimeRemaining():Number
		{
			if ( _IsExecutingBurst )
			{
				return 0;
			}
			
			var elapsedTime:Number = _GameData.Scene.GameTime - _CooldownStart;
			var remainingTime:Number = Math.max( 0, _BurstCooldown - elapsedTime );
			
			return remainingTime;
		}
		
		public function get RefreshPercRemaining():Number
		{
			return 1 - Math.min( ( _GameData.Scene.GameTime - _CooldownStart ) / _BurstCooldown, 1.0 );
		}
		
		public function get TotalRefreshTime():Number { return _BurstCooldown; }
	}
}