package game.gameplay.weapons.projectiles 
{
	import engine.Actor;
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import engine.interfaces.IGameData;
	import engine.misc.MathUtility;
	import engine.render.scene.SceneNode;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Scene;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.HomeGameData;
	import game.gameplay.weapons.IWeaponType;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ProjectileLauncher implements IWeaponType
	{
		private var _GameData:IGameData;
		private var _WeaponInfo:CombatSystemInfo
		private var _Particle:String;
		private var _Deviation:Number;
		private var _Speed:Number;
		
		public function ProjectileLauncher( gameData:IGameData, weaponInfo:CombatSystemInfo ) 
		{
			_GameData = gameData;
			_WeaponInfo = weaponInfo;
			_Particle = String( weaponInfo.RawXML.@particle );
			_Deviation = Number( weaponInfo.RawXML.@deviation );
			_Speed = Number( weaponInfo.RawXML.@speed );
		}
		
		public function fire( instigator:SpaceShip, facing:Point, x1:int, y1:int, x2:int, y2:int ):void
		{
			var projectile:Projectile = _GameData.Pools.getPool( _Particle ).pop() as Projectile;
			projectile.setup( _WeaponInfo, instigator );
			
			projectile.Instigator = instigator;
			
			projectile.X = x1;
			projectile.Y = y1;
			
			//Debug.debugLog(GameLogChannels.DEBUG, "Projectile told to spawn at: [x: " + x1 + " / y: " + y1 + "] spawning at [x: " + projectile.X + " / y: " + projectile.Y + "]");
			
			var dir:Point = new Point( x2 - x1, y2 - y1 );
			dir.normalize( 1 );
			
			const angleToTarget:Number = Math.abs( MathUtility.getAngle( dir.x, dir.y, instigator.Dir.x, instigator.Dir.y ) );
			
			// If outside fire-angle, just set to random fire angle based on owner's direction
			if ( _WeaponInfo.FireAngle > 0 && _WeaponInfo.FireAngle < 360 && angleToTarget > _WeaponInfo.FireAngle )
			{
				dir = instigator.Dir;
			}
			
			// @BUG: this doesn't appear to deviate very well, possibly from Number-to-int conversion?
			var deviation:Number = Math.random() * _Deviation;
			deviation /= 2;	// we're supporting both negative and positive ranges
			deviation *= Math.random() > 0.5 ? 1 : -1;	// determine neg or pos
			deviation *= Math.PI / 180;	// convert to rads
			
			// Apply deviation to dir				
			dir.x = dir.x * Math.cos( deviation ) - dir.y * Math.sin( deviation );
			dir.y = dir.x * Math.sin( deviation ) + dir.y * Math.cos( deviation ); 
			
			// Apply speed				
			projectile.VelocityX = instigator.Dir.x * instigator.Speed + dir.x * _Speed;
			projectile.VelocityY = instigator.Dir.y * instigator.Speed + dir.y * _Speed;
			projectile.faceToward( dir.x, dir.y );
		}
		
		public function destroy():void
		{
			
		}
	}
}