package game.gameplay.weapons.projectiles 
{
	import engine.Actor;
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.interfaces.IGameData;
	import engine.render.scene.SceneNode;
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	// @deprecated -- this file is not to be used, instead, use GenericPool and PoolManager
	public class ProjectilePool 
	{
		private var _Pools:Dictionary;
		
		// @TODO: figure out how this is accessed and stored
		public function ProjectilePool() 
		{
			_Pools = new Dictionary;
		}
		
		public function createPool( type:String, gameData:IGameData, size:int ):void
		{
			// Don't repeat a new
			if ( _Pools[ type ] )
				return;
			
			_Pools[ type ] = new Vector.<Projectile>;
			
			var projectile:Projectile;
			for ( var i:int = 0; i < size; i++ )
			{
				projectile = new Projectile;
				const memDisplay:Bitmap = gameData.AssetManager.getImage( type ) as Bitmap;
				projectile.DrawingData = memDisplay.bitmapData.clone();
				projectile.Node = new SceneNode( new RectCollider( 0, 0, projectile.Display.width, projectile.Display.height, CollisionFlags.ACTORS ) );
				_Pools[ type ].push( projectile );
			}
		}
		
		public function getProjectile( type:String, instigator:SpaceShip, gameData:IGameData, weaponInfo:CombatSystemInfo ):Projectile
		{
			var projectile:Projectile;
			
			// If the pool exists and it has a free element, get it
			if ( _Pools[ type ] && _Pools[ type ].length != 0 )
			{
				projectile = _Pools[ type ].pop();
			}
			else	// Otherwise, create a new projectile
			{
				projectile = new Projectile();
				const memDisplay:Bitmap = gameData.AssetManager.getImage( String( weaponInfo.RawXML.@particle ) ) as Bitmap;
								
				projectile.DrawingData = memDisplay.bitmapData.clone();
				
				projectile.Node = new SceneNode( new RectCollider( 0, 0, projectile.Display.width, projectile.Display.height, CollisionFlags.ACTORS ) );
			}
			
			projectile.Instigator = instigator;
			projectile.WeapInfo = weaponInfo;
			projectile.Data = gameData;
			projectile.initialize();
			
			return projectile;
		}
		
		public function addProjectile( type:String, projectile:Projectile ):void
		{
			if ( !_Pools[ type ] )
				_Pools[ type ] = new Vector.<Projectile>;
				
			_Pools[ type ].push( projectile );
		}
		
		public function debugReportPool( type:String ):void
		{
			
		}
		
		public function getPoolSize( type:String ):int
		{
			if ( _Pools[ type ] )
				return _Pools[ type ].length;
			return 0;
		}
	}
}