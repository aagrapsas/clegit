package game.gameplay.weapons 
{
	import engine.Actor;
	import engine.debug.Debug;
	import engine.interfaces.IGameData;
	import engine.interfaces.ISceneNotifiable;
	import engine.interfaces.ITickable;
	import engine.render.scene.SceneNode;
	import flash.geom.Point;
	import game.actors.components.CombatSystemInfo;
	import game.actors.SpaceShip;
	import game.debug.GameLogChannels;
	import game.fx.ThrusterFX;
	import game.fx.ThrusterFXPulse;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Tracker extends Actor implements ITickable, ISceneNotifiable
	{	
		public var Instigator:SpaceShip;
		public var Target:SpaceShip;
		public var WeapInfo:CombatSystemInfo;
		
		public var VelocityX:Number;
		public var VelocityY:Number;
		
		private var _LifeTime:Number = 0;
		
		private var _TurnSpeed:Number;
		private var _Speed:Number;
		private var _LifeDuration:Number;
		
		private var _IsDestroyed:Boolean = false;
		
		// @TODO this can probably extend projectile
		public function Tracker( weapInfo:CombatSystemInfo ) 
		{
			WeapInfo = weapInfo;
			
			_TurnSpeed = Number( weapInfo.RawXML.@turnSpeed );
			_Speed = Number( weapInfo.RawXML.@speed );
			_LifeDuration = Number( weapInfo.RawXML.@lifeTime );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _IsDestroyed )
				return;
			
			var dir:Point = null;
			
			if ( Target && Target.IsDead == false )
			{
				dir = new Point( Target.X - this.X, Target.Y - this.Y );
			}
			else
			{
				dir = this.Dir;
			}
			 
			dir.normalize( 1 );
			
			this.faceToward( dir.x, dir.y, _TurnSpeed * deltaTime );
			
			VelocityX += ( this.Dir.x * _Speed );
			VelocityY += ( this.Dir.y * _Speed );
			
			const currentMagSquared:Number = VelocityX * VelocityX + VelocityY * VelocityY;
			const maxMagSquared:Number = _Speed * _Speed;
			
			if ( currentMagSquared > maxMagSquared )
			{
				// Can probably cache this
				var velocityNormal:Point = new Point( VelocityX, VelocityY );
				velocityNormal.normalize( 1 );
				
				VelocityX = velocityNormal.x * _Speed;
				VelocityY = velocityNormal.y * _Speed;
			}
			
			// Debug.debugLog( GameLogChannels.ACTOR, "Moving missile with speed: " + _Speed );
			
			// const speed:Number = Math.sqrt( VelocityY * VelocityY + VelocityX * VelocityX );
			
			// Debug.debugLog( GameLogChannels.ACTOR, "Speed: " + speed );
			
			const nextXPos:Number = this.X + ( VelocityX * deltaTime );//this.X + ( this.Dir.x * _Speed * deltaTime );
			const nextYPos:Number = this.Y + ( VelocityY * deltaTime );//this.Y + ( this.Dir.y * _Speed * deltaTime );
			
			var futureColliders:Vector.<SceneNode> = Data.Scene.rectCheck( nextXPos, nextYPos, Node.CollisionComponent.Width, Node.CollisionComponent.Height, Node.CollisionComponent.CollisionFlag );
			
			var shouldDestroy:Boolean = false;
			
			if ( futureColliders.length != 0 )
			{
				for each ( var node:SceneNode in futureColliders )
				{
					var actor:SpaceShip = node.InternalObject as SpaceShip;
					
					// Do damage to all actors not on the projectile's team!
					// @TODO: this is where we'd handle the case where we hit a team member or
					// where we hit ourselves. In the case of ourselves, ignore. In the case of a team member,
					// we possibly would just want to destroy the projectile.
					if ( actor && ( actor.Team != Instigator.Team ) && actor.Node.doesAccuratelyCollide( this.Node ) )
					{
						actor.DoDamage( DamageTypes.MISSILE, WeapInfo.Damage, Instigator, nextXPos, nextYPos );
						shouldDestroy = true;
						break;
					}
				}
			}
			
			_LifeTime += deltaTime;
			
			if ( _LifeTime >= _LifeDuration )
			{
				shouldDestroy = true;
			}
			
			if ( shouldDestroy )
			{
				destroy();
			}
			else
			{
				this.X = nextXPos;
				this.Y = nextYPos;
			}
		}
		
		public function handleSceneTransition():void
		{
			destroy();
		}
		
		override public function set Data( value:IGameData ):void
		{
			super.Data = value;
			
			Data.Scene.addActor( this );
			Data.Scene.addTickable( this );
		}
		
		override public function destroy():void
		{
			if ( _IsDestroyed )
				return;
				
			_IsDestroyed = true;
			
			Data.Scene.removeActor( this );
			Data.Scene.removeTickable( this );
			
			super.destroy();
			
			Instigator = null;
			WeapInfo = null;
		}
	}
}