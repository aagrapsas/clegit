package game.gameplay.weapons 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class WeaponTypes 
	{
		public static const INSTANT_HIT:String = "instant_hit";
		public static const TRACKING:String = "tracking";
		public static const PROJECTILE:String = "projectile";
		public static const AREA_EFFECT:String = "area";
		public static const TURRET:String = "turret";
		public static const SPAWNER:String = "spawner";
		public static const REPAIR:String = "repair";
	}
}