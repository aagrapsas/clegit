package game.gameplay 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IGameExecuteDelay 
	{
		function canExecute():Boolean;
		function markExecution():void;
		function get RefreshTimeRemaining():Number;
		function get RefreshPercRemaining():Number;
		function get TotalRefreshTime():Number;
	}
}