package game.gameplay.player 
{
	import engine.debug.Debug;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import game.debug.GameLogChannels;
	import game.gameplay.CombatSystem;
	import game.gameplay.GameSystemIcon;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ActiveSelector extends Sprite implements ITickable, IDestroyable
	{	
		private var _GameSystems:Vector.<CombatSystem>;
		private var _GameSystemIcons:Vector.<GameSystemIcon>;
		
		private var _SelectedIndicator:Bitmap;
		
		private var _CurrentSystem:CombatSystem;
		
		private var _GameData:HomeGameData;
		
		private var _IsDrawnFullScreen:Boolean = false;
		
		private static const SPACE_BUFFER:int = 16;
		
		public function ActiveSelector( gameData:HomeGameData, gameSystems:Vector.<CombatSystem> )
		{
			_GameSystems = gameSystems;
			_GameData = gameData;
			
			_GameSystemIcons = new Vector.<GameSystemIcon>();
			
			initialize();
		}
		
		private function initialize():void
		{			
			for each ( var system:CombatSystem in _GameSystems )
			{
				if ( system != null )
					addIcon( system );
			}
			
			_GameData.Scene.Display.stage.addChild( this );
			_GameData.Scene.addTickable( this );
			
			this.y = _GameData.Scene.Display.stage.stageHeight - 64;
			this.x = 0;
			
			var selectionBMP:Bitmap = _GameData.AssetManager.getImage( "selection_icon" ) as Bitmap;
			var map:Bitmap = new Bitmap( selectionBMP.bitmapData.clone(), "auto", false );
			map.transform.matrix.scale(0.999, 0.999);
			_SelectedIndicator = map;
			
			this.addChild( _SelectedIndicator );
			_SelectedIndicator.alpha = 0;
			
			if ( _GameSystems.length > 0 )
			{
				_CurrentSystem = _GameSystems[ 0 ];
				_SelectedIndicator.alpha = 1;
				_SelectedIndicator.x = _GameSystems[ 0 ].Icon.x;
			}
		}
		
		private function addIcon( gameSystem:CombatSystem ):void
		{
			var icon:GameSystemIcon = gameSystem.Icon;
			
			this.addChild( icon );
			
			_GameSystemIcons.push( icon );
			
			icon.x = _GameSystemIcons.length * ( SPACE_BUFFER + 32 ) + SPACE_BUFFER;
			
			icon.addEventListener( MouseEvent.CLICK, onSystemClicked, false, 0, true );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _GameData.Scene.IsFullScreen && !_IsDrawnFullScreen )
			{
				_IsDrawnFullScreen = true;
				
				this.x = 360 - _GameData.Scene.Display.stage.fullScreenWidth / 2;
				this.y = _GameData.Scene.Display.stage.stageHeight - 128;
			}
			else if ( !_GameData.Scene.IsFullScreen && _IsDrawnFullScreen )
			{
				_IsDrawnFullScreen = false;
				
				this.x = 0;
				this.y = _GameData.Scene.Display.stage.stageHeight - 64;
			}
		}
		
		private function onSystemClicked( e:MouseEvent ):void
		{			
			if ( !(e.target is GameSystemIcon) )
				return;
			
			var index:int = _GameSystemIcons.indexOf( e.target );
			
			if ( index > -1 )
			{
				_CurrentSystem = _GameSystems[ index ];
				_SelectedIndicator.x = _GameSystemIcons[ index ].x;
			}
			else
			{
				Debug.errorLog( GameLogChannels.ERROR, "WeaponSelector: managed to click a weapon that doesn't exist!" );
			}
		}
		
		public function cycleWeapons():void
		{
			var currentIndex:int = _GameSystems.indexOf( _CurrentSystem );
			
			if ( currentIndex > -1 )
			{
				currentIndex++;
				
				if ( currentIndex >= _GameSystems.length )
				{
					currentIndex = 0;
				}
				
				_CurrentSystem = _GameSystems[ currentIndex ];
				_SelectedIndicator.x = _GameSystemIcons[ currentIndex ].x;
			}
			else
			{
				Debug.errorLog( GameLogChannels.ERROR, "WeaponSelector: totally borked. Can't find weapon to cycle" );
			}
		}
		
		public function get CurrentSystem():CombatSystem { return _CurrentSystem; }
		
		public function destroy():void
		{
			for each ( var system:CombatSystem in _GameSystems )
			{
				if ( system.Icon )
					system.Icon.removeEventListener( MouseEvent.CLICK, onSystemClicked );
			}
			
			_GameData.Scene.Display.stage.removeChild( this );
			_GameData.Scene.removeTickable( this );
			
			_GameSystems = null;
			_GameSystemIcons = null;
			
			_CurrentSystem = null;
			
			_GameData = null;
		}
	}
}