package game.gameplay.player 
{
	import engine.collision.RectCollider;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import flash.geom.Point;
	import game.gameplay.weapons.DamageTypes;
	import game.HomeGameData;
	import game.ui.misc.UIOutOfBounds;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class OutOfBoundsManager implements ITickable, IDestroyable
	{
		private var _GameData:HomeGameData;
		
		private var _OutOfBoundsIndicator:UIOutOfBounds;
		
		private var _WorldBound:RectCollider;
		private var _OutAccumulator:Number = 0;
		private var _MaxOutOfBounds:Number = 0;
		
		public function OutOfBoundsManager( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			initialize();
		}
		
		private function initialize():void
		{
			_WorldBound = new RectCollider( _GameData.Scene.CollisionRoot.CollisionComponent.X, _GameData.Scene.CollisionRoot.CollisionComponent.Y, _GameData.Scene.CollisionRoot.CollisionComponent.Width / 2, _GameData.Scene.CollisionRoot.CollisionComponent.Height / 2, _GameData.Scene.CollisionRoot.CollisionComponent.CollisionFlag );
			
			_MaxOutOfBounds = _GameData.GameConfigData[ "max_out_of_bounds" ];
			
			_OutOfBoundsIndicator = new UIOutOfBounds( _GameData );
			
			_GameData.Scene.UILayer.addChild( _OutOfBoundsIndicator );
			
			_OutOfBoundsIndicator.visible = false;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _GameData.PlayerShip == null )
			{
				if ( _OutOfBoundsIndicator.visible )
				{
					_OutOfBoundsIndicator.visible = false;
				}
				
				return;
			}
			
			if ( _WorldBound.doesCollide( _GameData.PlayerShip.Node.CollisionComponent ) == false )
			{
				// Visibility
				if ( _OutOfBoundsIndicator.visible == false)
				{
					_OutOfBoundsIndicator.visible = true;
				}
				
				// Alignment
				_OutOfBoundsIndicator.x = ( _GameData.Scene.GameStage.stageWidth / 2 ) - ( _OutOfBoundsIndicator.width / 2  );
				_OutOfBoundsIndicator.y = ( _GameData.Scene.GameStage.stageHeight / 2 ) - ( _OutOfBoundsIndicator.height / 2  );
				
				_OutAccumulator += deltaTime;
				
				_OutOfBoundsIndicator.setTime( _MaxOutOfBounds - _OutAccumulator );
				
				var dir:Point = new Point( _GameData.PlayerShip.X, -_GameData.PlayerShip.Y );
				dir.normalize( 1 );
				
				var angle:Number = MathUtility.getAngle( 0, 1, dir.x, dir.y );
				
				_OutOfBoundsIndicator.rotateArrow( angle );
				
				if ( _OutAccumulator >= _MaxOutOfBounds )
				{
					// Destroy player!
					var shieldDamage:int = _GameData.PlayerShip.ShieldHealth ? _GameData.PlayerShip.ShieldHealth.current : 0;
					var healthDamage:int = _GameData.PlayerShip.ArmorHealth ? _GameData.PlayerShip.ArmorHealth.current : 0;
					
					_GameData.PlayerShip.DoDamage( DamageTypes.MISC, shieldDamage + healthDamage, null, 0, 0 );
					
					_OutOfBoundsIndicator.visible = false;
				}
			}
			else
			{
				_OutAccumulator = 0;
				
				if ( _OutOfBoundsIndicator.visible )
				{
					_OutOfBoundsIndicator.visible = false;
				}
			}
		}
		
		public function destroy():void
		{
			_GameData.Scene.UILayer.removeChild( _OutOfBoundsIndicator );
		}
	}
}