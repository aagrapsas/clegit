package game.catalogue 
{
	import flash.utils.Dictionary;
	import game.actors.SpaceShip;
	import game.actors.SpaceShipInfo;
	import game.gameplay.CombatSystem;
	import game.gameplay.weapons.IWeaponType;
	import game.HomeGameData;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class InfoCatalogue 
	{	
		private var _GameData:HomeGameData;
		private var _Catalogue:Dictionary;
		private var _ShipTemplateCosts:Dictionary;
		private var _DifficultyClassCatalogue:DifficultyClassCatalogue;
		
		public function InfoCatalogue( gameData:HomeGameData ) 
		{
			_GameData = gameData;
			
			_Catalogue = new Dictionary();
			_ShipTemplateCosts = new Dictionary();
			
			_DifficultyClassCatalogue = new DifficultyClassCatalogue();
			
			deserialize();
		}
		
		private function deserialize():void
		{
			var xml:XML = _GameData.AssetManager.getXML( "additional_config" );
			
			var info:InfoElement;
			
			for each ( var subXML:XML in xml.info )
			{
				info = new InfoElement();
				info.deserialize( subXML );
				
				// @TODO maybe add info.type to the hash for safety against collisions?
				_Catalogue[ info.target ] = info;
			}
		}
		
		public function getInfo( target:String ):InfoElement
		{
			return _Catalogue[ target ];
		}
		
		/**
		 * Generally returns the soft cost of an item, even if it has hard currency value.
		 * This is used for evaluating the difficulty class of various ships.
		 * @param	key
		 * @return
		 */
		public function getCost( key:String ):int
		{
			var target:InfoElement = _Catalogue[ key ];
			
			if (target == null)
				return 0;
				
			return target.softCost;
		}
		
		public function getShipInfoCost( info:SpaceShipInfo ):int
		{
			var value:int = 0;
			
			var toIterate:Array = new Array();
			toIterate.push( info.Armor.Key );
			toIterate.push( info.Chassis.Key );
			toIterate.push( info.Engine.Key );
			toIterate.push( info.Sensor.Key );
			
			// Shields are optional
			if ( info.Shield )
				toIterate.push( info.Shield.Key );
			
			var component:InfoElement = null;
			for each ( var key:String in toIterate )
			{
				component = _Catalogue[ key ];
				
				if (component == null)
					continue;
					
				value += component.softCost;
			}
			
			return value;
		}
		
		public function getShipCost( ship:SpaceShip ):int
		{
			var cost:int = getShipInfoCost( ship.Info );
			
			var component:InfoElement = null;
			for each ( var weapon:CombatSystem in ship.CombatSystems )
			{
				component = _Catalogue[ weapon.Info.Key ];
				
				if (component == null)
					continue;
					
				cost += component.softCost;
			}
			
			return cost;
		}
		
		public function getTemplateCost( template:String ):int
		{
			if ( _ShipTemplateCosts[ template ] != null)
				return _ShipTemplateCosts[ template ];
				
			return 0;
		}
		
		public function setTemplateCost( template:String, cost:int ):void
		{
			_ShipTemplateCosts[ template ] = cost;
			
			_DifficultyClassCatalogue.registerShip( template, cost );
		}
		
		public function get DifficultyClassCatalog():DifficultyClassCatalogue { return _DifficultyClassCatalogue; }
	}
}