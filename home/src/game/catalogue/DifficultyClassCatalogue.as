package game.catalogue 
{
	import engine.debug.Debug;
	import game.debug.GameLogChannels;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DifficultyClassCatalogue 
	{
		private var _DifficultyCatalogue:Vector.<DifficultyClassElement>;
		private var _IsInitialized:Boolean = false;
		
		public function DifficultyClassCatalogue() 
		{
			_DifficultyCatalogue = new Vector.<DifficultyClassElement>();
		}
		
		public function registerShip( key:String, cost:int ):void
		{
			_DifficultyCatalogue.push( new DifficultyClassElement( key, cost ) );
		}
		
		public function initialize():void
		{
			_IsInitialized = true;
			
			_DifficultyCatalogue.sort( compareDifficulty );
		}
		
		private function compareDifficulty( one:DifficultyClassElement, two:DifficultyClassElement ):int
		{
			if (one.Cost == two.Cost)
			{
				return 0;
			}
			else if (one.Cost < two.Cost)
			{
				return -1;
			}
			
			return 1;
		}
		
		public function getSmallestDifficultyRating():int
		{
			if ( _DifficultyCatalogue.length == 0 )
			{
				Debug.debugLog(GameLogChannels.ERROR, "DifficultyClassCatalogue has no valid ships!");
				return 0;
			}
			
			return _DifficultyCatalogue[ 0 ].Cost;
		}
		
		public function getSmallestClosestDifficulty( value:int ):Vector.<DifficultyClassElement>
		{
			var returnValue:Vector.<DifficultyClassElement> = new Vector.<DifficultyClassElement>();
			
			// Select low is true because we want to exclude larger values
			var index:int = getClosestDifficultyValue( value, 0, _DifficultyCatalogue.length - 1, true );
			
			// For iterating
			var targetCost:int = _DifficultyCatalogue[ index ].Cost;
			
			if ( index == - 1 )
				return returnValue;
			
			// If not the final element, continue to iterate backwards to see if there is a range that matches the same value we want to return 
			// (i.e., if value is 200, return all ships with value 200)
			if ( index > 0 )
			{
				while ( index > 0 )
				{
					if ( _DifficultyCatalogue[ index ].Cost == targetCost )
					{
						returnValue.push( _DifficultyCatalogue[ index ] );
					}
					else
					{
						break;
					}
					
					index--;
				}
			}
			
			return returnValue;
		}
		
		private function getClosestDifficultyValue( value:int, left:int, right:int, selectLow:Boolean ):int
		{
			var shouldLoop:Boolean = true;
			
			while ( shouldLoop )
			{
				if ( left > right )
					return -1;
					
				var middle:int = left + (right - left) / 2;
				var middleValue:int = _DifficultyCatalogue[ middle ].Cost;
				
				if ( middleValue == value )
				{
					return middle;
				}
				else if ( right - left == 1 )
				{
					if ( selectLow )
					{
						if (_DifficultyCatalogue[ right ].Cost > value )
							return left;
						return right;
					}
					else
					{
						if (_DifficultyCatalogue[ left ].Cost >= value )
							return left;
						return right;
					}
				}
				else if ( value > middleValue )
				{
					left = middle;
				}
				else if ( value < middleValue )
				{
					right = middle;
				}
			}
			
			return -1;
		}
		
		public function getDifficultyRange( low:int, high:int ):Vector.<String>
		{
			var returnValue:Vector.<String> = new Vector.<String>();
			
			var left:int = 0;
			var right:int = _DifficultyCatalogue.length - 1;
			
			var shouldLoop:Boolean = true;
			
			while ( shouldLoop )
			{
				if ( left > right )
					break;
					
				var middle:int = left + (right - left) / 2;
				var middleValue:int = _DifficultyCatalogue[ middle ].Cost;
				
				if ( low > middleValue )
				{
					// Drop left
					left = middle;
				}
				else if ( high < middleValue )
				{
					// Drop right
					right = middle;
				}
				else
				{
					// Iterate current
					const trueLeft:int = getClosestDifficultyValue( low, left, right, false );
					const trueRight:int = getClosestDifficultyValue( high, left, right, true );
					const amount:int = trueLeft + trueRight - trueLeft;
					
					for ( var i:int = trueLeft; i <= amount; i++ )
					{
						returnValue.push( _DifficultyCatalogue[ i ].Key );
					}
					
					break;
				}
			}
			
			return returnValue;
		}
		
		public function get isInitialized():Boolean { return _IsInitialized; }
	}
}