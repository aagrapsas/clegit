package  
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.utils.getDefinitionByName;
	import game.ui.misc.UILoadingBar;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class UnifiedPreloader extends MovieClip
	{
		[Embed(source = '../bin/swf/preloader.swf', symbol = "UILoadingBarSWF")]
		private var _ProgressBarClass:Class;
		
		[Embed(source = '../bin/swf/preloader.swf', symbol = "UIPreloaderBGSWF")]
		private var _BackgroundClass:Class;
		
		private var _ProgressBar:UILoadingBar;		
		private var _ProgressBarMovieClip:MovieClip;
		
		private var _Background:Bitmap;
		private var _BackgroundWidth:int;
		
		private var _Statuses:Array = [ "Retrieving Galaxy Charts",
										"Loading Navigation AI",
										"Sequencing Ship Schematics",
										"Preparing Command Deck",
										"Tuning Communications",
										"Initializing Diplomatic Options",
										"Arming Weapon Systems",
										"Encrypting Network",
										"Validating Credentials",
										"Determining Coordinates",
										"Booting Combat AI",
										"Evaluating Combat Systems",
										"Synchronizing Sensor Nodes",
										"Connecting to WaveNet",
										"You're being watched...",
										"Enemies are everywhere.",
										"Tell no one.",
										"Hope is not lost.",
										"Requesting Military Access",
										"Rerouting Secure Connection",
										"Establishing Secure Protocol",
										"Tracing Enemy Locations",
										"Running DNA Validation",
										"Contacting Fleet Command" ];
										
		private static const TEXT_CHANGE_PERCENTAGE:int = 10;
		
		public function UnifiedPreloader() 
		{
			// Prevent moving on to next frame
			stop();
			
			// Set stage properties
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			_ProgressBarMovieClip = new _ProgressBarClass();
			_ProgressBar = new UILoadingBar( _ProgressBarMovieClip );
			
			_Background = new _BackgroundClass() as Bitmap;
			_BackgroundWidth = _Background.width;
			
			this.addChild( _Background );
			this.addChild( _ProgressBarMovieClip );
			
			_ProgressBarMovieClip.x = ( stage.stageWidth / 2 ) - ( 275 / 2 );
			_ProgressBarMovieClip.y = ( stage.stageHeight / 2 ) - ( _ProgressBar.clip.height / 2 );
			
			addEventListener( Event.ENTER_FRAME, onEnterFrame );
		}
		
		private function onEnterFrame( e:Event ):void
		{
			if ( framesLoaded == totalFrames )
			{
				removeEventListener( Event.ENTER_FRAME, onEnterFrame );
				
				nextFrame();
				
				finishLoading();
			}
			else
			{
				var percent:Number = root.loaderInfo.bytesLoaded / root.loaderInfo.bytesTotal;
				
				var wholePercent:Number = percent * 100;
				
				var dividedPercent:Number = wholePercent / TEXT_CHANGE_PERCENTAGE;
				var integerPercent:int = wholePercent / TEXT_CHANGE_PERCENTAGE;
				
				if ( dividedPercent == integerPercent )
				{
					var index:int = int( Math.random() * _Statuses.length );
					var text:String = _Statuses[ index ];
					
					_ProgressBar.addLoaded( text );
					
					_Statuses.splice( index, 1 );
				}
				
				_ProgressBar.setPercentage( percent );
				
				_ProgressBarMovieClip.x = ( stage.stageWidth / 2 ) - ( 275 / 2 );
				_ProgressBarMovieClip.y = ( stage.stageHeight / 2 ) - ( _ProgressBar.clip.height / 2 );
				
				var diffWidth:int = 0;
			
				if ( stage.stageWidth > _BackgroundWidth )
				{
					diffWidth = stage.stageWidth - _BackgroundWidth;
				}
				
				_Background.x = 0;
				_Background.y = 0;
				
				_Background.width = stage.stageWidth;
				_Background.height = stage.stageHeight + diffWidth;
			}
		}
		
		private function finishLoading():void
		{
			var mainClass:Class = Class( getDefinitionByName( "Main" ) );
			
			var app:Object = new mainClass();
			
			addChild( app as DisplayObject );
		}
	}
}