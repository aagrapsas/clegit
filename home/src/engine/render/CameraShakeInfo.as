package engine.render 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CameraShakeInfo 
	{
		public var magnitude:int;
		public var duration:Number;
		public var totalCount:Number;
		
		public function CameraShakeInfo( mag:int, dur:Number, count:int ) 
		{
			magnitude = mag;
			duration = dur;
			totalCount = count;
		}
	}
}