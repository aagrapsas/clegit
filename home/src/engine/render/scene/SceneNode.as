package engine.render.scene 
{
	import engine.Actor;
	import engine.collision.RectCollider;
	import engine.interfaces.IGameData;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneNode 
	{	
		public var CollisionNode:QuadTreeNode;
		public var CollisionComponent:RectCollider;
		public var InternalObject:Actor;
		
		public function SceneNode( collision:RectCollider )
		{
			CollisionComponent = collision;
		}
		
		public function remove():void
		{
			CollisionNode.remove( this );
		}
		
		public function doesAccuratelyCollide( node:SceneNode ):Boolean
		{	
			return doesAccuratelyCollideByNAABB( node );
		}
		
		private function doesAccuratelyCollideByNAABB( node:SceneNode ):Boolean
		{
			this.updateNAABBCollision();
			node.updateNAABBCollision();
			
			return this.CollisionComponent.doesAccuratelyCollide( node.CollisionComponent );
		}
		
		public function updateNAABBCollision():void
		{			
			var corner:Point = new Point;
			corner.x = 0;
			corner.y = 0;
			
			corner = this.InternalObject.Display.transformPoint( corner );
			
			this.CollisionComponent.NonAxisX[ RectCollider.TOP_LEFT ] = corner.x;
			this.CollisionComponent.NonAxisY[ RectCollider.TOP_LEFT ] = -corner.y;
			
			corner.x = this.InternalObject.Width;
			corner.y = 0;
			
			corner = this.InternalObject.Display.transformPoint( corner );
			
			this.CollisionComponent.NonAxisX[ RectCollider.TOP_RIGHT ] = corner.x;
			this.CollisionComponent.NonAxisY[ RectCollider.TOP_RIGHT ] = -corner.y;
			
			corner.x = this.InternalObject.Width;
			corner.y = this.InternalObject.Height;
			
			corner = this.InternalObject.Display.transformPoint( corner );
			
			this.CollisionComponent.NonAxisX[ RectCollider.BOTTOM_RIGHT ] = corner.x;
			this.CollisionComponent.NonAxisY[ RectCollider.BOTTOM_RIGHT ] = -corner.y;
			
			corner.x = 0;
			corner.y = this.InternalObject.Height;
			
			corner = this.InternalObject.Display.transformPoint( corner );
			
			this.CollisionComponent.NonAxisX[ RectCollider.BOTTOM_LEFT ] = corner.x;
			this.CollisionComponent.NonAxisY[ RectCollider.BOTTOM_LEFT ] = -corner.y;
		}
		
		public function updatePosition():void
		{
			CollisionNode.updatePosition( this );
		}
	}
}