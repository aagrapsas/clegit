package engine.render.scene 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneBlind implements IDestroyable
	{	
		private var _Display:Sprite;
		private var _Speed:int;
		private var _Dir:int;
		private var _HiddenY:int;
		private var _VisibleY:int;
		private var _Height:int;
		private var _Width:int;
		
		private var _IsVisible:Boolean = false;
		
		public function SceneBlind( hiddenY:int, visibleY:int, height:int, width:int ) 
		{
			_HiddenY = hiddenY;
			_VisibleY = visibleY;
			_Height = height;
			_Width = width;
			
			_Display = new Sprite;
			
			_Display.alpha = 0;
			
			draw();
		}
		
		public function reDraw( left:int, hiddenY:int, visibleY:int, height:int, width:int ):void
		{
			_HiddenY = hiddenY;
			_VisibleY = visibleY;
			_Height = height;
			_Width = width;
			
			_Display.x = left;
			
			_Display.graphics.clear();
			draw();
		}
		
		private function draw():void
		{
			_Display.graphics.beginFill( 0x000000, 1 );
			_Display.graphics.drawRect( 0, 0, _Width, _Height );
			_Display.graphics.endFill();
			
			_Display.y = _HiddenY;
		}
		
		public function showBlind( speed:int ):void
		{
			_Dir = _VisibleY > _HiddenY ? 1 : -1;
			_Speed = speed;
			_Display.addEventListener( Event.ENTER_FRAME, onUpdate, false, 0, true );
			_IsVisible = true;
			_Display.alpha = 1;
		}
		
		public function hideBlind( speed:int ):void
		{
			_Dir = _VisibleY > _HiddenY ? -1 : 1;
			_Speed = speed;
			_Display.addEventListener( Event.ENTER_FRAME, onUpdate, false, 0, true );
			_IsVisible = false;
		}
		
		private function clampMax( current:int, max:int ):int
		{
			return ( current < max ? current : max );
		}
		
		private function clampMin( current:int, min:int ):int
		{
			return ( current > min ? current : min );
		}
		
		private function onUpdate( e:Event ):void
		{
			const bIsTopBlind:Boolean = _HiddenY < _VisibleY;
			
			if ( bIsTopBlind )	// Top blind
			{
				if ( _Dir > 0 )	// Going down (to visible)
				{
					_Display.y = clampMax( _Display.y + _Dir * _Speed, _VisibleY );
				}
				else // hiding
				{
					_Display.y = clampMin( _Display.y + _Dir * _Speed, _HiddenY );
				}
			}
			else	// Bottom blind
			{
				if ( _Dir < 0 ) // Going up (to visible)
				{
					_Display.y = clampMin( _Display.y + _Dir * _Speed, _VisibleY );
				}
				else	// hiding
				{
					_Display.y = clampMax( _Display.y + _Dir * _Speed, _HiddenY );
				}
			}
			
			if ( _Display.y == _VisibleY || _Display.y == _HiddenY )
			{
				_Display.removeEventListener( Event.ENTER_FRAME, onUpdate );
				
				if ( !_IsVisible )
					_Display.alpha = 0;
			}
		}
		
		public function get Display():Sprite { return _Display; }
		public function get IsVisible():Boolean { return _IsVisible; }
		
		public function destroy():void
		{
			_Display.removeEventListener( Event.ENTER_FRAME, onUpdate );
			
			_Display = null;
		}
	}
}