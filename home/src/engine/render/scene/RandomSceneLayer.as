package engine.render.scene 
{
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class RandomSceneLayer extends SceneLayer
	{
		public var Density:Number;
		public var Images:Vector.<BitmapData>;
		
		public function RandomSceneLayer( name:String, gameData:IGameData, sceneDisplay:Sprite, movementPercent:Number, density:Number, images:Vector.<BitmapData> ) 
		{
			super( name, movementPercent );
			
			Density = density;
			Images = images;
			
			generate( gameData );
		}
		
		private function generate( gameData:IGameData ):void
		{
			
		}
	}
}