package engine.render.scene 
{
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.Stage;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneFade extends Sprite implements ITickable
	{	
		private var _Direction:int;
		private var _Duration:Number;
		private var _Alpha:Number;
		
		private var _Scene:SceneManager;
		private var _Stage:Stage;
		
		private var _Accumulator:Number;
		
		public function SceneFade( stage:Stage, scene:SceneManager ) 
		{
			_Stage = stage;
			_Scene = scene;
			
			this.mouseEnabled = false;
			
			drawRectangle();
		}
		
		private function drawRectangle():void
		{
			this.graphics.clear();
			this.graphics.beginFill( 0x000000, 1 );
			this.graphics.drawRect( -( _Stage.fullScreenWidth - _Stage.stageWidth ) / 2, -( _Stage.fullScreenHeight - _Stage.stageHeight ) / 2, _Stage.fullScreenWidth, _Stage.fullScreenHeight );
			this.graphics.endFill();
			
			this.alpha = 0;
		}
		
		public function startFade( direction:int, duration:Number ):void
		{
			_Direction = direction > 0 ? 1 : -1;
			_Alpha = _Direction > 0 ? 0 : 1;
			_Accumulator = 0;
			
			_Duration = duration;
			
			_Scene.addTickable( this );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _Accumulator >= _Duration )
			{
				_Scene.removeTickable( this );
			}
			
			this.alpha = _Direction > 0 ? _Accumulator / _Duration : 1 - ( _Accumulator / _Duration );
			
			_Accumulator += deltaTime;
		}
		
		public function get IsBlackedOut():Boolean
		{
			return ( _Alpha >= 1 );
		}
	}
}