package engine.render.scene 
{
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IDestroyable;
	import flash.display.Graphics;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	 /**
	  * QuadTreeNode
	  * An implementation of a quad tree for use in spacial partitioning.
	  * Best used in a world/game where there many colliders and large segments of the
	  * map that contain no colliders. For even distributions, consider a fixed grid.
	  */
	public class QuadTreeNode implements IDestroyable
	{
		/* Constants */
		private const NORTH_EAST:uint = 0;
		private const SOUTH_EAST:uint = 1;
		private const SOUTH_WEST:uint = 2;
		private const NORTH_WEST:uint = 3;
		private const MIN_DIMENSIONS:uint = 10;
		private const MAX_CONTENTS:uint = 1;
		
		/* Containers */
		public var Children:Vector.<QuadTreeNode>;
		private var _Contents:Vector.<SceneNode>;
		private var _NodePool:QuadTreeNodePool;
		
		public var CollisionComponent:RectCollider;
		public var Parent:QuadTreeNode;	// node's parent (null indicates root)
		public var Level:uint;			// depth in the tree
		
		private var _CanSubdivide:Boolean;
		private var _IsLeaf:Boolean;
		
		/**
		 * Constructor for QuadTreeNode
		 * @param	collision	collision to use for this partition of space
		 * @param	parent		parent to this node
		 * @param	level		level within the overall tree
		 * @param	nodePool	reference to the game's node pool
		 */
		public function QuadTreeNode( collision:RectCollider, parent:QuadTreeNode, level:uint, nodePool:QuadTreeNodePool ) 
		{
			// Initialize (may want to handle this through the node pool)
			Children = new Vector.<QuadTreeNode>;
			_Contents = new Vector.<SceneNode>;
			Children.length = 4;
			
			initialize( collision, parent, level, nodePool );
		}
		
		/**
		 * Method for initializing internal elements of the node.
		 * Public because this is used via the QuadTreeNodePool to ready a free node.
		 * @param	collision	collision to use for this partition of space
		 * @param	parent		parent to this node
		 * @param	level		level within the overall tree
		 * @param	nodePool	reference to the game's node pool
		 */
		public function initialize( collision:RectCollider, parent:QuadTreeNode, level:uint, nodePool:QuadTreeNodePool ):void
		{
			_CanSubdivide = ( collision.Width / 4 ) >= MIN_DIMENSIONS && ( collision.Height / 4 ) >= MIN_DIMENSIONS;
			
			// Assign
			CollisionComponent = collision;
			Parent = parent;
			Level = level;
			_NodePool = nodePool;
			
			_IsLeaf = true;
		}
		
		/**
		 * Finds all colliders within this quad that intersect the specified SceneNode
		 * @param	node	object to check for intersection against this quad's contents
		 * @param	isAccurate	should this use very accurate collision (OBB's)
		 * @return	returns a vector containing all colliders
		 */
		public function getColliders( node:SceneNode, isAccurate:Boolean = false ):Vector.<SceneNode>
		{
			var returnSet:Vector.<SceneNode> = new Vector.<SceneNode>;
			
			// Base case
			if ( _Contents.length > 0 )
			{
				for each ( var potential:SceneNode in _Contents )
				{
					if ( potential == node )
						continue;
						
					if ( potential.CollisionComponent.doesCollide( node.CollisionComponent ) )
					{
						if ( isAccurate )
						{
							if ( node.doesAccuratelyCollide( potential ) )
								returnSet.push( potential );
						}
						else
						{
							returnSet.push( potential );
						}
					}
				}
			}
			
			// Find colliders with children
			for each ( var child:QuadTreeNode in Children )
			{
				if ( child && child.CollisionComponent.doesCollide( node.CollisionComponent ) )
				{
					var colliders:Vector.<SceneNode> = child.getColliders( node, isAccurate );
					
					for each ( var collider:SceneNode in colliders )
					{
						returnSet.push( collider );
					}
				}
			}
			
			return returnSet;
		}
		
		/**
		 * Find all objects within the quad that collide with the specified line
		 * @param	x1
		 * @param	y1
		 * @param	x2
		 * @param	y2
		 * @param	collisionFlag	collision filtering flag
		 * @param	requestNode	requesting object (to prevent adding it to the return vector)
		 * @return	vector of colliders
		 */
		public function getLineColliders( x1:int, y1:int, x2:int, y2:int, collisionFlag:uint, requestNode:SceneNode ):Vector.<SceneNode>
		{
			var returnSet:Vector.<SceneNode> = new Vector.<SceneNode>;
			
			if ( _Contents.length > 0 )
			{
				for each ( var potential:SceneNode in _Contents )
				{
					if ( requestNode && potential == requestNode )
						continue;
					
					if ( potential.CollisionComponent.doesLineIntersect( x1, y1, x2, y2, collisionFlag ) )
					{
						returnSet.push( potential );
					}
				}
			}
			
			for each ( var child:QuadTreeNode in Children )
			{				
				if ( child.CollisionComponent.doesLineIntersect( x1, y1, x2, y2, collisionFlag ) )
				{
					var colliders:Vector.<SceneNode> = child.getLineColliders( x1, y1, x2, y2, collisionFlag, requestNode );
					
					for each ( var collider:SceneNode in colliders )
					{
						returnSet.push( collider );
					}
				}
			}
			
			return returnSet;
		}
		
		/**
		 * Find all colliders that intersect a point in the world
		 * @param	x
		 * @param	y
		 * @param	collisionFlag	collision filtering flag
		 * @return	vector of colliders
		 */
		public function getPointColliders( x:int, y:int, collisionFlag:uint ):Vector.<SceneNode>
		{
			var colliders:Vector.<SceneNode> = new Vector.<SceneNode>;
			
			if ( _Contents.length > 0 )
			{				
				for each ( var collider:SceneNode in _Contents )
				{
					if ( collider.CollisionComponent.containsPoint( x, y, collisionFlag ) )
					{
						colliders.push( collider );
					}
				}
			}
			
			for each ( var node:QuadTreeNode in Children )
			{
				if ( node && node.CollisionComponent.containsPoint( x, y, collisionFlag ) )
				{
					var subColliders:Vector.<SceneNode> = node.getPointColliders( x, y, collisionFlag );
					
					for each ( var element:SceneNode in subColliders )
					{
						colliders.push( element );
					}
				}
			}
			
			return colliders;
		}
		
		/**
		 * Adds a SceneNode to the appropriate subnode
		 * @param	node	node to add
		 */
		public function add( node:SceneNode ):void
		{
			// Divide if necessary
			if ( _Contents.length >= MAX_CONTENTS && _CanSubdivide )
				divide();
				
			// If leaf and no division is necessary, add right here
			if ( _IsLeaf )
			{
				_Contents.push( node );
				node.CollisionNode = this;
			}
			else	// Otherwise, find appropriate node to add to
			{
				var overlapIndex:uint = 0;
				var overlapCount:uint = 0;
				
				for ( var index:uint = 0; index < Children.length; index++ )
				{
					if ( Children[ index ] && Children[ index ].CollisionComponent.doesCollide( node.CollisionComponent ) )
					{
						overlapIndex = index;
						overlapCount++;
					}
				}
				
				// If intersecting with mutliple nodes, add here
				if ( overlapCount > 1 )
				{
					_Contents.push( node );
					node.CollisionNode = this;
				}
				else if ( overlapCount == 1 )	// Otherwise, add to appropriate child
				{
					// @TODO: remove recursion and replace with iteration
					Children[ overlapIndex ].add( node );
				}
			}
		}
		
		/**
		 * Creates children quads
		 */
		private function divide():void
		{
			if ( !_CanSubdivide || !_IsLeaf )
				return;
				
			const quarterWidth:uint = CollisionComponent.Width / 4;
			const quarterHeight:uint = CollisionComponent.Height / 4;
			const halfWidth:uint = CollisionComponent.Width / 2;
			const halfHeight:uint = CollisionComponent.Height / 2;
			
			// Utilize node pool to fill the four quadrants represented by the children
			Children[ NORTH_EAST ] = _NodePool.getNode( CollisionComponent.X + quarterWidth, CollisionComponent.Y + quarterHeight, halfWidth, halfHeight, this, Level + 1 );
			Children[ SOUTH_EAST ] = _NodePool.getNode( CollisionComponent.X + quarterWidth, CollisionComponent.Y - quarterHeight, halfWidth, halfHeight, this, Level + 1 );
			Children[ SOUTH_WEST ] = _NodePool.getNode( CollisionComponent.X - quarterWidth, CollisionComponent.Y - quarterHeight, halfWidth, halfHeight, this, Level + 1 );
			Children[ NORTH_WEST ] = _NodePool.getNode( CollisionComponent.X - quarterWidth, CollisionComponent.Y + quarterHeight, halfWidth, halfHeight, this, Level + 1 );
			
			var currentContent:Vector.<SceneNode> = _Contents;
			
			_Contents = new Vector.<SceneNode>;
			
			_IsLeaf = false;
			
			for each ( var node:SceneNode in currentContent )
			{
				_Contents.push( node );
			}
		}
		
		/**
		 * Update an object's position within the quad tree
		 * @param	node	object to update
		 */
		public function updatePosition( node:SceneNode ):void
		{			
			var index:int = _Contents.indexOf( node );
			
			// If this node contains the object
			if ( index > -1 )
			{
				_Contents.splice( index, 1 );
				
				var isNodeAdded:Boolean = false;
				
				// Re-add if it still fits within (we re-add because it may better fit in one of the sub-nodes)
				if ( node.CollisionComponent.doesFitIn( CollisionComponent ) )
				{
					add( node );
					isNodeAdded = true;
				}
				else
				{					
					// Iterate upwards attempting to add node
					var prevQuadNode:QuadTreeNode = Parent;
					
					while ( prevQuadNode )
					{
						if ( node.CollisionComponent.doesFitIn( prevQuadNode.CollisionComponent ) )
						{
							prevQuadNode.add( node );
							isNodeAdded = true;
							break;
						}
						
						prevQuadNode = prevQuadNode.Parent;
					}
					
					if ( isNodeAdded == false )
					{
						Debug.errorLog( EngineLogChannels.ERROR, "An actor has moved beyond the maximum world coordinates!" );
					}
					
					// Iterate upwards again, this time freeing nodes as they're available
					prevQuadNode = this;
					
					while ( prevQuadNode )
					{
						if ( prevQuadNode.childrenContentCount == 0 )
							prevQuadNode.freeChildren();
						else
							break;
							
						prevQuadNode = prevQuadNode.Parent;
					}
				}
			}
		}
		
		/**
		 * Removes a node (requires knowledge of where node already exists)
		 * @param	node	object to remove
		 */
		public function remove( node:SceneNode ):void
		{
			var index:int = _Contents.indexOf( node );
			
			if ( index > -1 )
				_Contents.splice( index, 1 );
		}
		
		/**
		 * Draws debug lines for this quad and all children quads
		 * @param	graphics	graphics reference to draw on
		 * @param	color		color to draw lines in
		 */
		public function drawDebug( graphics:Graphics, color:uint ):void
		{
			if ( _IsLeaf )
			{
				CollisionComponent.drawDebug( graphics, new Point( 1, -1 ), color );
			}
			
			for each ( var child:QuadTreeNode in Children )
			{
				if ( child )
					child.drawDebug( graphics, color );
			}
		}
		
		/**
		 * Returns the number of objects within this specific quad (not including children)
		 */
		public function get contentCount():int { return _Contents.length; }
		
		/**
		 * Returns the number of objects within this quad (including children)
		 */
		public function get childrenContentCount():int
		{
			var count:int = _Contents.length;
			
			for each ( var child:QuadTreeNode in Children )
			{
				if ( child )
					count += child.childrenContentCount;
			}
			
			return count;
		}
		
		/**
		 * Frees children to the pool for reuse
		 */
		public function freeChildren():void
		{
			_IsLeaf = true;
			
			for ( var index:int = 0; index < Children.length; index++ )
			{
				if ( Children[ index ] )
						Children[ index ].free();
						
				Children[ index ] = null;
			}
		}
		
		/**
		 * Frees this quad to the pool for reuse
		 */
		public function free():void
		{
			_NodePool.addNode( this );
		}
		
		public function freeRecursive():void
		{
			for ( var index:int = 0; index < Children.length; index++ )
			{
				if ( Children[ index ] )
						Children[ index ].freeRecursive();
						
				Children[ index ] = null;
			}
			
			free();
		}
		
		/**
		 * Nulls any references
		 */
		public function destroy():void
		{
			Children = null;
			_Contents = null;
			_NodePool = null;
			CollisionComponent = null;
		}
	}
}