package engine.render.scene 
{
	import flash.display.Bitmap;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneObjectLayer extends SceneLayer
	{
		public function SceneObjectLayer( name:String, movementPercent:Number ) 
		{
			super( name, movementPercent );
		}	
	}
}