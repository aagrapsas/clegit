package engine.render.scene 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import flash.display.Sprite;
	import flash.display.Stage;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SceneFlash extends Sprite implements IDestroyable, ITickable
	{
		private var _Stage:Stage;
		private var _Dir:int;
		private var _Manager:SceneManager;
		
		private var _TransitionLength:Number = 0;
		private var _TransitionStart:Number = 0;
		
		public function SceneFlash( stage:Stage, manager:SceneManager ) 
		{
			_Stage = stage;
			_Manager = manager;
			
			initialize();
		}
		
		private function initialize():void
		{
			this.graphics.beginFill( 0xFFFFFF, 1 );
			this.graphics.drawRect( 0, 0, _Stage.fullScreenWidth, _Stage.fullScreenHeight );
			this.graphics.endFill();
			
			this.x = -( _Stage.fullScreenWidth - 730 ) / 2;
			this.y = -( _Stage.fullScreenHeight - 600 ) / 2;
			
			this.alpha = 0;
			
			this.mouseEnabled = false;
			
			_Stage.addChild( this );
		}
		
		public function flash( duration:Number ):void
		{
			_TransitionLength = duration / 2;
			
			_Dir = 1;
			
			_TransitionStart = _Manager.GameTime ;
			
			_Manager.addTickable( this );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( _Dir > 0 )
			{
				var alpha:Number = ( _Manager.GameTime - _TransitionStart ) / _TransitionLength;
				alpha = Math.min( MathUtility.getEaseOutAlpha( alpha ), 1 );
				
				this.alpha = alpha;
				
				if ( alpha == 1 )
				{
					_Dir = -1;
					_TransitionStart = _Manager.GameTime;
				}
			}
			else
			{
				var alphaN:Number = Math.max( 1 - ( _Manager.GameTime - _TransitionStart ) / _TransitionLength, 0 );
				alphaN = MathUtility.getEaseInAlpha( alphaN );
				
				this.alpha = alphaN;
				
				if ( alphaN == 0 )
				{
					_Manager.removeTickable( this );
				}
			}
		}
		
		public function destroy():void
		{
			_Stage.removeChild( this );
		}
	}
}