package engine.render.scene 
{
	import engine.Actor;
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.render.Camera;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.Stage;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class QuadBlitter 
	{
		private var _Stage:Stage;
		private var _Bitmap:Bitmap;
		
		private var _Layers:Vector.<SceneLayer>;
		private var _QuadTree:QuadTreeNode;
		
		private var _ObjectLayer:SceneObjectLayer;
		
		private var _ScreenNode:SceneNode;
		
		private var _BlendMode:String = "true";
		
		private var _CachedWidth:int;
		private var _CachedHeight:int;
		private var _ScreenRect:Rectangle;
		private var _TranslateMatrix:Matrix;
		
		private var _Camera:Camera;
		
		public function QuadBlitter( stage:Stage, quad:QuadTreeNode ) 
		{
			_Stage = stage;
			_QuadTree = quad;
			
			_Layers = new Vector.<SceneLayer>();
			
			_ScreenNode = new SceneNode( new RectCollider( 0, 0, 0, 0, CollisionFlags.ALL ) );
			_ScreenRect = new Rectangle( 0, 0, 0, 0 );
			
			_CachedWidth = _Stage.stageWidth;
			_CachedHeight = _Stage.stageHeight;
			
			_Bitmap = new Bitmap( new BitmapData( _CachedWidth, _CachedHeight, true, 0x00ffffff ), "auto", true );
			
			_Stage.addChild( _Bitmap );
			
			_TranslateMatrix = new Matrix();
		}
		
		public function setObjectLayer( layer:SceneObjectLayer ):void
		{
			_ObjectLayer = layer;
		}
		
		public function addLayer( layer:SceneLayer ):void
		{
			_Layers.push( layer );
		}
		
		public function removeLayer( layer:SceneLayer ):void
		{
			var index:int = _Layers.indexOf( layer );
			
			if ( index > -1 )
			{
				_Layers.splice( index, 1 );
			}
		}
		
		public function clearLayers():void
		{
			_Layers = new Vector.<SceneLayer>();
		}
		
		private function drawQuad():void
		{			
			// Calculate screen bounds
			const left:int = -_Camera.X;
			const top:int = _Camera.Y;
			
			// Update screen collision
			_ScreenNode.CollisionComponent.X = left;
			_ScreenNode.CollisionComponent.Y = top;
			_ScreenNode.CollisionComponent.Width = _Stage.stageWidth;
			_ScreenNode.CollisionComponent.Height = _Stage.stageHeight;
			
			// Find visible actors
			var visible:Vector.<SceneNode> = _QuadTree.getColliders( _ScreenNode );
			
			Debug.perfHud.setValue( "Drawing actors", visible.length );
			
			// Draw each visible actor's visible pixels
			for each ( var node:SceneNode in visible )
			{
				if ( node.InternalObject.Display )
				{
					//node.InternalObject.Display.drawBlitData( null, _ObjectLayer.X, _ObjectLayer.Y, _Bitmap, _ScreenRect );
				}
			}
		}
		
		private function drawBitmaps():void
		{
			// Screen bounds is already updated at this point
			const left:int = _ScreenNode.CollisionComponent.Left;
			const right:int = _ScreenNode.CollisionComponent.Right;
			const top:int = -_ScreenNode.CollisionComponent.Top;
			const bottom:int = -_ScreenNode.CollisionComponent.Bottom;
			
			var count:int = 0;
			
			/*
			for each ( var bitmap:Bitmap in _ObjectLayer.bitmaps )
			{
				if ( !bitmap.bitmapData || bitmap.visible == false || bitmap.alpha == 0 )
					continue;
				
				// Early cull if not on screen
 				if ( bitmap.x >= right || bitmap.x + bitmap.width <= left 
				  || bitmap.y >= bottom || bitmap.y + bitmap.height <= top )
				  {
					  continue;
				  }
				  
				var matrix:Matrix = bitmap.transform.matrix.clone();
				
				matrix.translate( _ObjectLayer.X, _ObjectLayer.Y );
				  
				//_Bitmap.bitmapData.draw( bitmap.bitmapData, matrix, null, null, _ScreenRect, true );
				
				count++;
			}
			*/
			
			Debug.perfHud.setValue( "Drawing bitmaps", count );
		}
		
		public function draw():void
		{		
			if ( !_ObjectLayer || !_Camera )
				return
			
			_ScreenRect.width = _Stage.stageWidth;
			_ScreenRect.height = _Stage.stageHeight;
			
			// Create new bitmap data width/height if stage has changed sizes
			if ( _ScreenRect.width != _CachedWidth || _ScreenRect.height != _CachedHeight )
			{
				_Bitmap.bitmapData = new BitmapData( _ScreenRect.width, _ScreenRect.height, true, 0x00ffffff );
				_CachedHeight = _ScreenRect.height;
				_CachedWidth = _ScreenRect.width;
			}
			
			// Clear buffer to black
			_Bitmap.bitmapData.fillRect( _ScreenRect, 0x000000 );
			
			// Draw each layer
			for each ( var layer:SceneLayer in _Layers )
			{
				//layer.blitToBitmap( _Bitmap, _ScreenRect );
			}
			
			// Draw actors
			//drawQuad();
			
			// Draw FX & such
			//drawBitmaps();
		}
		
		public function get objectLayer():SceneObjectLayer { return _ObjectLayer; }
		
		public function set camera( value:Camera ):void { _Camera = value; }
	}
}