package engine.render.scene 
{
	import engine.collision.CollisionFlags;
	import engine.collision.RectCollider;
	import engine.interfaces.IDestroyable;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class QuadTreeNodePool implements IDestroyable
	{	
		private var _Pool:Vector.<QuadTreeNode>;
		
		public function QuadTreeNodePool( size:uint = 10 ) 
		{
			_Pool = new Vector.<QuadTreeNode>;
			
			for ( var index:uint = 0; index < size; index++ )
			{
				_Pool.push( new QuadTreeNode( new RectCollider( 0, 0, 10, 10, CollisionFlags.ALL ), null, 0, this ) );
			}
		}
		
		public function getNode( x:int, y:int, width:uint, height:uint, parent:QuadTreeNode, level:uint ):QuadTreeNode
		{			
			var returnNode:QuadTreeNode;
			
			// Get node either from pool or by growing the pool
			if ( _Pool.length > 0 )
			{
				returnNode = _Pool.pop();
			}
			else
			{
				returnNode = new QuadTreeNode( new RectCollider( 0, 0, 10, 10, CollisionFlags.ALL ), null, 0, this );
			}
			
			// Setup node with proper values
			returnNode.CollisionComponent.X = x;
			returnNode.CollisionComponent.Y = y;
			returnNode.CollisionComponent.Width = width;
			returnNode.CollisionComponent.Height = height;
			
			returnNode.initialize( returnNode.CollisionComponent, parent, level, this );

			return returnNode;
		}
		
		public function addNode( node:QuadTreeNode ):void
		{
			if ( node != null )
				_Pool.push( node );
		}
		
		public function get size():uint { return _Pool.length; }
		
		public function destroy():void
		{
			_Pool = null;
		}
	}
}