package engine.render 
{
	import engine.interfaces.IBlittable;
	import engine.render.scene.SceneLayer;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Drawable
	{				
		public var Display:Sprite;
		
		private var _X:Number;	// this is center of image based
		private var _Y:Number;	// this is center of image based
		
		private var _Parent:Drawable;
		
		private var _Rotation:int;
		private var _Matrix:Matrix;
		
		private var _DebugSprite:Sprite;
		
		private var _RawData:BitmapData;
		
		private var _Children:Vector.<Bitmap>;
		private var _DrawableChildren:Vector.<Drawable>;	// @TODO consider making this simpler/better/more integrated
		
		private var _Center:Point;
		
		private var _Normal:Point;
		private var _NormalCacheRotation:int;
		
		private var _Subpixel:String = "never";
		
		public var visible:Boolean = true;
		private var _Alpha:Number = 1.0;
		public var scale:Number = 1.0;
		
		private var _Container:Sprite;
		private var _Bitmap:Bitmap;
		
		private var _Layer:SceneLayer;
		
		public function Drawable() 
		{
			_Children = new Vector.<Bitmap>();
			_DrawableChildren = new Vector.<Drawable>();
			_Center = new Point( 0, 0 );
			_Normal = new Point( 0, 1 );
			_Matrix = new Matrix();
			
			Display = new Sprite();
			_Container = new Sprite();
			_DebugSprite = new Sprite();
			
			Display.addChild( _Container );
		}
		
		public function reset():void
		{
			_Matrix.identity();
			
			_Rotation = 0;
			_X = 0;
			_Y = 0;
			
			rotate( 0 );
			
			scale = 1.0;
			alpha = 1.0;
			visible = true;
			
			_Normal.x = 0;
			_Normal.y = 1;
		}
		
		public function rotate( amount:int ):void
		{
			if ( amount == 0 )
				return;
			
			_Rotation += amount;
			
			_Matrix.identity();
			_Matrix.translate( -_Center.x, -_Center.y );
			_Matrix.rotate( _Rotation * ( Math.PI / 180 ) );
			_Matrix.translate( _Center.x, _Center.y );
			
			_Container.transform.matrix = _Matrix;
		}
		
		private function forceUpdateMatrices():void
		{
			_Matrix.identity();
			_Matrix.translate( -_Center.x, -_Center.y );
			_Matrix.rotate( _Rotation * ( Math.PI / 180 ) );
			_Matrix.translate( _Center.x, _Center.y );
			
			_Container.transform.matrix = _Matrix;
		}
		
		public function addDrawable( drawable:Drawable ):void
		{
			addChild( drawable.Display );
			
			drawable._Parent = this;
		}
		
		public function removeDrawable( drawable:Drawable ):void
		{
			removeChild( drawable.Display );
			
			drawable._Parent = null;
		}
		
		public function setCenter( x:int, y:int ):void
		{
			_Center.x = x;
			_Center.y = y;
		}
		
		public function addChild( child:DisplayObject ):void
		{
			_Container.addChild( child );
		}
		
		public function removeChild( child:DisplayObject ):void
		{
			_Container.removeChild( child );
		}
		
		// Mutators
		public function set RawData( value:BitmapData ):void
		{ 
			_RawData = value;

			_Center.x = _RawData.width / 2;
			_Center.y = _RawData.height / 2;
			
			_Bitmap = new Bitmap( value.clone(), "auto", true );
			
			_Container.addChild( _Bitmap );
			_Container.addChild( _DebugSprite );
			
			forceUpdateMatrices();
		}
		
		public function set worldX( value:Number ):void
		{
			_X = value;
			
			Display.x = _X - _Center.x;
		}
		
		public function set worldY( value:Number ):void
		{
			_Y = value;
			
			Display.y = -_Y - _Center.y;	// flipped world coordinate system in y-dir
		}
		
		
		// Accessors
		public function get sourceWidth():int { return _RawData.width; }
		public function get sourceHeight():int { return _RawData.height; }
		
		public function get worldX():Number
		{			
			return _X;
		}
		
		public function get worldY():Number
		{
			return _Y;
		}
		
		public function transformPoint( point:Point ):Point
		{
			if ( !_Layer )
				return point;
			
			point = _Container.localToGlobal( point );
			
			point.x -= _Layer.X;
			point.y -= _Layer.Y;
			
			return point;
		}
		
		public function get normal():Point
		{
			if ( _Rotation == _NormalCacheRotation )
				return _Normal;
				
			_Normal.x = _RawData.width / 2;
			_Normal.y = 0;
			
			_Normal = _Container.localToGlobal( _Normal );
			
			var transformedCenter:Point = new Point( _RawData.width / 2, _RawData.height / 2 );
			
			transformedCenter = _Container.localToGlobal( transformedCenter );
			
			_Normal.x -= transformedCenter.x;
			_Normal.y -= transformedCenter.y;
			
			_Normal.y *= -1;
			
			_Normal.normalize( 1 );
			
			_NormalCacheRotation = _Rotation;
			
			return _Normal;
		}
		
		public function set displayParent( value:Drawable ):void { _Parent = value; }
		
		public function set subPixel( value:String ):void { _Subpixel = value; }
		
		public function get center():Point { return _Center; }
		
		public function get cachedWidth():int { return _RawData.width; }
		public function get cachedHeight():int { return _RawData.height; }
		
		public function get width():int { return Display.width; }
		public function get height():int { return Display.height; }
		
		public function get layer():SceneLayer { return _Layer; }
		public function set layer( value:SceneLayer ):void { _Layer = value; }
		
		public function set alpha( value:Number ):void
		{
			Display.alpha = value;
		}
		
		public function get alpha():Number { return Display.alpha; }
		
		public function destroy():void
		{
			
		}
	}
}