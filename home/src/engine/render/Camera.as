package engine.render 
{
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import engine.misc.MathUtility;
	import engine.render.scene.SceneLayer;
	import flash.events.Event;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Camera implements ITickable
	{
		private var _GameData:IGameData;
		private var _SceneWidth:uint;
		private var _SceneHeight:uint;
		
		private var _X:Number;
		private var _Y:Number;
		
		private var _IsInitialized:Boolean = false;
		
		/* CAMERA SHAKE */
		private var _MaxMagnitude:int;
		
		private var _DesiredMagnitudeX:int;
		private var _DesiredMagnitudeY:int;
		
		private var _DesiredDuration:Number;
		private var _ShakeStart:Number = 0;
		
		private var _IsAwayFromCenter:Boolean;
		
		private var _IsShaking:Boolean = false;
		
		private var _ActiveShakeChain:Vector.<CameraShakeInfo>;
		private var _ActiveShakeCount:int = 0;
		
		private var _PreviousStageWidth:int = 0;
		private var _PreviousStageHeight:int = 0;
		
		public function Camera( gameData:IGameData, sceneWidth:uint, sceneHeight:uint ) 
		{
			_GameData = gameData;
			_SceneWidth = sceneWidth;
			_SceneHeight = sceneHeight;
			
			_PreviousStageWidth = _GameData.Scene.GameStage.stageWidth;
			_PreviousStageHeight = _GameData.Scene.GameStage.stageHeight;
			
			_GameData.Scene.GameStage.addEventListener( Event.RESIZE, onWindowResize );
		}
		
		private function onWindowResize( e:Event ):void
		{			
			if ( !_GameData.Scene.isGameReady )
			{
				return;
			}
			
			var diffX:int = ( _GameData.Scene.GameStage.stageWidth - _PreviousStageWidth ) / 2;
			var diffY:int = ( _GameData.Scene.GameStage.stageHeight - _PreviousStageHeight ) / 2;
			
			scrollXY( diffX, diffY );
			
			_PreviousStageHeight = _GameData.Scene.GameStage.stageHeight;
			_PreviousStageWidth = _GameData.Scene.GameStage.stageWidth;
		}
		
		// @TODO: write an ease-in function for when time is specified
		public function centerAt( x:int, y:int, time:uint = 0 ):void
		{
			var layer:SceneLayer = _GameData.Scene.Layers[ "objects" ];
			
			// Position object layer 0, 0 relative to middle of screen
			layer.X = x + _GameData.Scene.GameStage.stageWidth / 2;
			layer.Y = -y + _GameData.Scene.GameStage.stageHeight / 2;
			
			// Center other layers
			for ( var key:String in _GameData.Scene.Layers )
			{
				if ( key == "objects" )
					continue;
				
				layer = _GameData.Scene.Layers[ key ];
				
				layer.X = ( _GameData.Scene.GameStage.stageWidth / 2 ) - ( layer.width / 2 );
				layer.Y = ( _GameData.Scene.GameStage.stageHeight / 2 ) - ( layer.height / 2 );
			}
			
			_X = x;
			_Y = y;
			
			_GameData.DebugSystem.DebugSprite.x = _GameData.Scene.Layers[ "objects" ].X;
			_GameData.DebugSystem.DebugSprite.y = _GameData.Scene.Layers[ "objects" ].Y;
			
			_IsInitialized = true;
		}
		
		public function setCameraPosition( positionX:Number, positionY:Number):void
		{			
			_X = positionX;
			_Y = positionY;
			
			_GameData.DebugSystem.DebugSprite.x = _GameData.Scene.Layers[ "objects" ].X;
			_GameData.DebugSystem.DebugSprite.y = _GameData.Scene.Layers[ "objects" ].Y;
		}
		
		public function scrollXY( xAmount:Number, yAmount:Number ):void
		{			
			for ( var key:String in _GameData.Scene.Layers )
			{
				var layer:SceneLayer = _GameData.Scene.Layers[ key ];
				
				layer.X += xAmount * layer.MovementPercent;
				layer.Y += yAmount * layer.MovementPercent;

				_GameData.DebugSystem.PerfHUD.setValue( layer.Name, "(x: " + layer.X + " y: " + layer.Y + ")");
			}
			
			_GameData.DebugSystem.PerfHUD.setValue( "Camera", "(x: " + _X + " y: " + _Y + ")" );
			
			_X += xAmount;
			_Y += yAmount;
			
		}
		
		/* SHAKE */
		public function multiShake( info:Vector.<CameraShakeInfo> ):void
		{
			// @TODO make this better
			if ( _DesiredMagnitudeX != 0 || _DesiredMagnitudeY != 0 )
				return;
			
			_ActiveShakeChain = info;
		}
		
		private function processMultiShake():void
		{
			if ( !_ActiveShakeChain || _IsShaking )
				return;
				
			var activeShake:CameraShakeInfo = _ActiveShakeChain[ 0 ];
				
			if ( _ActiveShakeCount >= activeShake.totalCount )
			{
				_ActiveShakeChain.shift();
				
				if ( _ActiveShakeChain.length == 0 )
					_ActiveShakeChain = null;
					
				_ActiveShakeCount = 0;
			}
			else
			{
				_ActiveShakeCount++;
				
				shake( activeShake.magnitude, activeShake.duration, null );
			}
		}
		
		public function shake( max:int, duration:Number, hitDir:Point ):void
		{
			// No stacking shakes!
			if ( _DesiredMagnitudeX != 0 || _DesiredMagnitudeY != 0 )
				return;
				
			_MaxMagnitude = max;
			_DesiredDuration = duration;
			
			var dir:Point = new Point;
			
			if ( hitDir )
			{
				dir = hitDir;
			}
			else
			{
				dir.x = ( Math.random() > 0.5 ? -1 : 1 ) * Math.random() * 100;
				dir.y = ( Math.random() > 0.5 ? -1 : 1 ) * Math.random() * 100;
				dir.normalize( 1 );
			}
			
			dir.x *= _MaxMagnitude;
			dir.y *= _MaxMagnitude;
			
			_DesiredMagnitudeX = dir.x;
			_DesiredMagnitudeY = dir.y;

			_ShakeStart = _GameData.Scene.GameTime;
			
			_IsAwayFromCenter = true;
			_IsShaking = true;
		}
		
		public function tick( deltaTime:Number ):void
		{
			var shakeX:int = 0;
			var shakeY:int = 0;
			
			if ( _DesiredMagnitudeX != 0 || _DesiredMagnitudeY != 0 )
			{
				var alpha:Number = Math.min( ( _GameData.Scene.GameTime - _ShakeStart ) / _DesiredDuration, 1 );
				
				shakeX = MathUtility.easeInInt( 0, _DesiredMagnitudeX, alpha );
				shakeY = MathUtility.easeInInt( 0, _DesiredMagnitudeY, alpha );
				
				if ( alpha == 1 && _IsAwayFromCenter )	// at max
				{
					_IsAwayFromCenter = false;
					_ShakeStart = _GameData.Scene.GameTime;
					_DesiredMagnitudeX *= -1;
					_DesiredMagnitudeY *= -1;
				}
				else if ( alpha == 1 && !_IsAwayFromCenter )	// at origin
				{
					_IsAwayFromCenter = true;
					_ShakeStart = _GameData.Scene.GameTime;
					
					// go to half max
					_DesiredMagnitudeX *= -0.5;
					_DesiredMagnitudeY *= -0.5;
				}
				
				if ( shakeX != 0 || shakeY != 0 )
					_GameData.ActiveCamera.scrollXY( shakeX, shakeY );
			}
			else if ( _IsShaking )
			{
				_IsShaking = false;
			}
			
			processMultiShake();
		}
		
		public function get IsShaking():Boolean { return _IsShaking; }
		
		public function get X():Number { return _X; }
		public function get Y():Number { return _Y; }
		public function get IsInitialized():Boolean { return _IsInitialized; }
		public function set IsInitialized( value:Boolean ):void { _IsInitialized = value; }
	}
}