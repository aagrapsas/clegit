package engine.assets 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AssetEvent
	{
		public static const ASSET_EVENT_NAME:String = "asset_event";
		
		public static const XML_TYPE:String = "xml";
		public static const IMAGE_TYPE:String = "img";
		public static const ANIMATION_TYPE:String = "anim";
		public static const AUDIO_TYPE:String = "audio";
		public static const SWF_TYPE:String = "swf";
		
		public var assetName:String;
		public var assetType:String;
		
		public function AssetEvent( assetName:String, assetType:String ) 
		{
			this.assetName = assetName;
			this.assetType = assetType;
		}
	}
}