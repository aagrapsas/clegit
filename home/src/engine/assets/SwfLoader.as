package engine.assets 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SwfLoader implements IAsset
	{
		private var _Name:String;	// uniquely keyed name
		private var _Loc:String;	// location of the swf
		
		private var _RawXML:XML;
		
		private var _ClassMap:Dictionary;
		
		private var _IsLoaded:Boolean = false;
		
		private var _Loader:Loader;
		private var _LoadedCallback:Function;
		
		public function SwfLoader( name:String, loc:String, xml:XML ) 
		{
			_ClassMap = new Dictionary();
			
			_Name = name;
			_Loc = loc;
			
			_RawXML = xml;
		}
		
		public function load( callback:Function ):void
		{
			_LoadedCallback = callback;
			
			_Loader = new Loader();
			_Loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onSwfLoaded );
			_Loader.load( new URLRequest( _Loc ) );
		}
		
		private function onSwfLoaded( e:Event ):void
		{			
			_Loader.contentLoaderInfo.removeEventListener( Event.COMPLETE, onSwfLoaded );
			
			_IsLoaded = true;
			
			deserialize();
			
			_LoadedCallback.apply( null, [ this ] );
		}
		
		private function deserialize():void
		{
			if ( !_IsLoaded )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Attempted to deserialize swf classes without swf being fully loaded!" );
				return;
			}
			
			for each ( var classXML:XML in _RawXML.export )
			{
				const name:String = classXML.@name;
				const domain:String = classXML.@domain;
				
				var type:Class = _Loader.contentLoaderInfo.applicationDomain.getDefinition( domain ) as Class;
				
				_ClassMap[ name ] = type;
			}
		}
		
		public function get classMap():Dictionary { return _ClassMap; }
		public function get name():String { return _Name; }
		public function get isDoneLoading():Boolean { return _IsLoaded; }
	}
}