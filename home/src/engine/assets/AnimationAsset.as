package engine.assets 
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class AnimationAsset implements IAsset
	{
		private var _Name:String;
		private var _Locations:Vector.<String>;
		private var _Callback:Function;
		private var _Loader:ImageAsset;
		
		private var _Images:Vector.<Bitmap>;
		
		public function AnimationAsset( name:String, locations:Vector.<String> ) 
		{
			_Name = name;
			_Locations = locations;
		}
		
		public function load( callback:Function ):void
		{
			_Callback = callback;
			
			_Images = new Vector.<Bitmap>();
			
			_Loader = new ImageAsset( _Name + "0", _Locations[ 0 ] );
			_Loader.load( loadNext );
		}
		
		private function loadNext( previous:ImageAsset ):void
		{
			_Images.push( _Loader.data );
			
			if ( _Images.length < _Locations.length )
			{
				_Loader = new ImageAsset( _Name + _Images.length, _Locations[ _Images.length ] );
				_Loader.load( loadNext );
			}
			else
			{
				_Callback.apply( null, [ this ] );
			}
		}
		
		public function get data():Vector.<Bitmap> { return _Images; }
		public function get name():String { return _Name; }
		public function get locations():Vector.<String> { return _Locations; }
		public function get isDoneLoading():Boolean { return _Images && _Images.length == _Locations.length; }
	}
}