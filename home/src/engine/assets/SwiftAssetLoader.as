package engine.assets 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SwiftAssetLoader implements IAsset
	{
		private var _Name:String;
		private var _Location:String;
		private var _Files:Dictionary;
		private var _IsLoaded:Boolean = false;
		
		private var _Loader:Loader;
		private var _LoadedCallback:Function;
		
		private var _RawXML:XML;
		
		private var _XML:Dictionary;
		private var _Images:Dictionary;
		private var _Audio:Dictionary;
		private var _Animations:Dictionary;
		
		private var _ExtractingCallback:Function;
		
		public function SwiftAssetLoader( name:String, location:String, xml:XML ) 
		{
			// Allocation
			_XML = new Dictionary();
			_Images = new Dictionary();
			_Audio = new Dictionary();
			_Animations = new Dictionary();
			
			// Assignment
			_RawXML = xml;
			_Location = location;
			_Name = name;
		}
		
		public function registerExtractingCallback( callback:Function ):void
		{
			_ExtractingCallback = callback;
		}
		
		public function getXML( name:String ):XML
		{
			if ( !_XML[ name ] )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Unable to find XML " + name + " in " + _Name );
				return null;
			}
			
			return _XML[ name ];
		}
		
		public function getImage( name:String ):Bitmap
		{
			if ( !_Images[ name ] )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Unable to find Image " + name + " in " + _Name );
				return null;
			}
			
			return _Images[ name ];
		}
		
		public function getAudio( name:String ):Sound
		{
			if ( !_Audio[ name ] )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Unable to find Audio " + name + " in " + _Name );
				return null;
			}
			
			return _Audio[ name ];
		}
		
		public function getAnimationFrames():Vector.<Bitmap>
		{
			if ( !_Animations[ name ] )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Unable to find Animation " + name + " in " + _Name );
				return null;
			}
			
			return _Animations[ name ];
		}
		
		public function load( callback:Function ):void
		{
			_LoadedCallback = callback;
			
			_Loader = new Loader();
			_Loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onLoaded );
			_Loader.load( new URLRequest( _Location ) );
		}
		
		private function onLoaded( e:Event ):void
		{
			_IsLoaded = true;
			
			_Loader.contentLoaderInfo.removeEventListener( Event.COMPLETE, onLoaded );
			
			deserialize();
		}
		
		private function deserialize():void
		{
			if ( !_IsLoaded )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Attempted to deserialize packed information without swf being fully loaded!" );
				return;
			}
			
			if ( _ExtractingCallback != null )
			{
				_ExtractingCallback.apply( null, [ _Name ] );
			}
			
			for each ( var fileXML:XML in _RawXML.file )
			{
				const name:String = String( fileXML.@name );
				const domainName:String = _Name + "_" + name;
				const count:int = int( fileXML.@count );
				const type:String = String( fileXML.@type );
				
				Debug.debugLog( EngineLogChannels.ASSETS, "Extracting " + name + " of type " + type + " via swift river file " + _Name );
				
				if ( count > 1 && type == "Animation" )	// Animation
				{
					var rawBitmaps:Vector.<Bitmap> = new Vector.<Bitmap>();
					
					for ( var iAnimIndex:int = 0; iAnimIndex < count; iAnimIndex++ )
					{
						var animBitmap:Class = _Loader.contentLoaderInfo.applicationDomain.getDefinition( String( domainName + iAnimIndex.toString() ) ) as Class;
						
						rawBitmaps.push( new animBitmap() );
					}
					
					_Animations[ name ] = rawBitmaps;
				}
				else if ( type == "Image" )
				{
					var rawBitmap:Class = _Loader.contentLoaderInfo.applicationDomain.getDefinition( domainName ) as Class;
					
					_Images[ name ] = new rawBitmap();
				}
				else if ( type == "Audio" )
				{
					var rawSound:Class = _Loader.contentLoaderInfo.applicationDomain.getDefinition( domainName ) as Class;
					
					_Audio[ name ] = new rawSound();
				}
				else if ( type == "XML" )
				{
					var rawXML:Class = _Loader.contentLoaderInfo.applicationDomain.getDefinition( domainName ) as Class;
					
					_XML[ name ] = new XML( new rawXML() );
				}
				else
				{
					Debug.errorLog( EngineLogChannels.ASSETS, "Unable to find type of " + type + " for asset " + name + " in " + _Name );
				}
			}
			
			_LoadedCallback.apply( null, [ this ] );
		}
		
		public function get allXML():Dictionary { return _XML; }
		public function get allAudio():Dictionary { return _Audio; }
		public function get allImages():Dictionary { return _Images; }
		public function get allAnimations():Dictionary { return _Animations; }
		
		public function get name():String { return _Name; }
		
		public function get isDoneLoading():Boolean { return _IsLoaded; }
	}
}