package engine.assets 
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class ImageAsset implements IAsset
	{
		private var _Name:String;
		private var _Location:String;
		private var _Loader:Loader;
		private var _Image:Bitmap;
		private var _Callback:Function;
		
		public function ImageAsset( name:String, location:String ) 
		{
			_Name = name;
			_Location = location;
		}
		
		public function load( callback:Function ):void
		{
			_Loader = new Loader();
			_Loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onComplete );
			_Loader.load( new URLRequest( _Location ) );
		}
		
		private function onComplete( e:Event ):void
		{
			_Loader.contentLoaderInfo.removeEventListener( Event.COMPLETE, onComplete );
			
			_Image = _Loader.content as Bitmap;
			
			_Callback.apply( null, [ this ] );
		}
		
		public function get data():Bitmap { return _Image; }
		public function get name():String { return _Name; }
		public function get location():String { return _Location; }
		public function get isDoneLoading():Boolean { return _Image != null; }
	}
}