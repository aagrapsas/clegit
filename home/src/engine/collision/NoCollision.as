package engine.collision 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class NoCollision 
	{	
		private var _Location;
		
		public function NoCollision() 
		{
			
		}
		
		public function get Location():Point { return _Location; }
		public function set Location( value:Point ):void { _Location = value; }
	}
}