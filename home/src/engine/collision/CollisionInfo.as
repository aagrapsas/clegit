package engine.collision 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CollisionInfo 
	{
		public var X:Number;
		public var Y:Number;
		public var DidCollide:Boolean;
		
		public function CollisionInfo( x:Number, y:Number, didCollide:Boolean ) 
		{
			X = x;
			Y = y;
			DidCollide = didCollide;
		}		
	}
}