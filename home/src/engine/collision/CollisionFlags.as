package engine.collision 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class CollisionFlags 
	{
		public static const WORLD_UI:uint = 1;
		public static const ACTORS:uint = 2;
		public static const NON_COLLIDERS:uint = 4;
		public static const ALL:uint = ACTORS | NON_COLLIDERS | WORLD_UI;
	}
}