package engine.misc 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import flash.net.SharedObject;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class LocalStorage 
	{
		private static const STORAGE_NAME:String = "home_engine";
		
		public function LocalStorage() 
		{
			
		}
		
		public function writeField( field:String, data:Object ):void
		{
			var sharedObject:SharedObject = null;
			
			try
			{
				sharedObject = SharedObject.getLocal( STORAGE_NAME );
			}
			catch ( err : Error )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Unable to write to shared object!" );
			}	
			
			if ( !sharedObject )
			{
				return;
			}
			
			sharedObject.data[ field ] = data;
				
			sharedObject.flush();
			
			sharedObject.close();
		}
		
		public function readField( field:String ):Object
		{
			var sharedObject:SharedObject = null;
			
			try
			{
				SharedObject.getLocal( STORAGE_NAME );
			}
			catch ( err : Error )
			{
				Debug.debugLog( EngineLogChannels.ERROR, "Unable to read from shared object!" );
			}
			
			if ( !sharedObject )
			{
				return null;
			}
			
			var obj:Object = sharedObject.data[ field ];
			
			sharedObject.close();
			
			return obj;
		}
		
		public function clearAll():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal( STORAGE_NAME );
			
			if ( !sharedObject )
			{
				return;
			}
			
			sharedObject.clear();
			sharedObject.flush();
		}
	}
}