package engine.misc 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class EngineProperties 
	{
		public var resourcePath:String;	// path to the data folder on the remote host
		public var buildNumber:String;
		
		public function EngineProperties() 
		{
			
		}
	}
}