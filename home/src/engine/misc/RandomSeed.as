package engine.misc 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class RandomSeed 
	{
		private var _Seed:int;
		private var _CurrentSeed:int;
		
		public function RandomSeed( seed:int = 0 )
		{
			_Seed = seed;
			_CurrentSeed = seed;
			
			if ( seed == 0 )
			{
				_Seed = Math.random();
				_CurrentSeed = _Seed;
			}
		}
		
		public function getNext():Number
		{
			return ( _CurrentSeed = ( _CurrentSeed * 16807 ) % 2147483647 ) / 0x7FFFFFFF + 0.000000000233;
		}
		
		public function getCurrent():Number
		{
			return ( ( ( _CurrentSeed * 16807 ) % 2147483647 ) / 0x7FFFFFFF + 0.000000000233 );
		}
		
		public function get Seed():int { return _Seed; }
		public function set Seed( value:int ):void { _Seed = value; }
	}
}