package engine.misc 
{
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class EventBus 
	{
		private var _EventMap:Dictionary;	// maps event names to function pointers
		
		public function EventBus() 
		{
			_EventMap = new Dictionary();
		}
		
		public function register( event:String, callback:Function ):void
		{
			if ( _EventMap[ event ] == null )
				_EventMap[ event ] = new Array();
			
			_EventMap[ event ].push( callback );
		}
		
		public function dispatchEvent( event:String, data:Object ):void
		{
			if ( _EventMap[ event ] == null )
			{
				return;
			}
			
			var callbacks:Array = _EventMap[ event ];
			
			for each ( var callback:Function in callbacks )
			{
				callback.apply( null, [ data ] );
			}
		}
		
		public function unregister( event:String, callback:Function ):void
		{
			var callbacks:Array = _EventMap[ event ];
			
			if ( !callbacks )
			{
				return;
			}
			
			var index:int = callbacks.indexOf( callback );
			
			if ( index > -1 )
			{
				callbacks.splice( index, 1 );
			}
		}
	}
}