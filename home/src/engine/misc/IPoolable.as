package engine.misc 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IPoolable extends IDestroyable
	{
		function resurrect():void;
		function suspend():void;
		function set pool( value:GenericPool ):void;
	}
	
}