package engine.misc 
{
	import engine.interfaces.IGameData;
	import flash.utils.Dictionary;
	public class PoolManager
	{
		private var _Pools:Dictionary;
		private var _GameData:IGameData;
		
		private var _Config:IPoolConfig;	// need to keep a reference so it's not collected
		
		public function PoolManager( gameData:IGameData, config:IPoolConfig ) 
		{
			_Pools = new Dictionary();
			
			_Config = config;
			
			_GameData = gameData;
			
			config.execute( gameData, this );
		}
		
		public function getPool( type:String ):GenericPool
		{
			return _Pools[ type ];
		}
		
		public function createPool( creationMethod:Function, type:String, size:int, cap:int ):void
		{
			_Pools[ type ] = new GenericPool( creationMethod, type, _GameData, size, cap );
		}
		
		public function destroyPool( type:String ):void
		{
			_Pools[ type ].destroy();
			
			delete _Pools[ type ];
		}
		
		/**
		 * Force frees all pools within the engine (happens when you transition a scene, etc.)
		 */
		public function forceFreeAll():void
		{
			for each ( var pool:GenericPool in _Pools )
			{
				pool.freeAll();
			}
		}
		
		public function destroy():void
		{
			for each ( var pool:GenericPool in _Pools )
			{
				pool.destroy();
			}
		}
	}
}