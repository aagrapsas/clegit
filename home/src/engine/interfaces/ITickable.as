package engine.interfaces 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface ITickable 
	{
		function tick( deltaTime:Number ):void;
	}
}