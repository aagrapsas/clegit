package engine.interfaces 
{
	import flash.display.Bitmap;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IBlittable 
	{
		function getBlitData():Bitmap;
	}
}