package engine.interfaces 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface ISceneNotifiable 
	{
		function handleSceneTransition():void;	// function called on scene transition
	}
}