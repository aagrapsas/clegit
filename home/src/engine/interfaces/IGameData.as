package engine.interfaces 
{
	import engine.assets.AdvancedAssetManager;
	import engine.assets.AnimationManager;
	import engine.audio.SoundManager;
	import engine.debug.DebugManager;
	import engine.misc.EngineProperties;
	import engine.misc.EventBus;
	import engine.misc.PoolManager;
	import engine.render.Camera;
	import engine.render.scene.QuadTreeNode;
	import engine.render.scene.SceneManager;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IGameData 
	{
		function get AssetManager():AdvancedAssetManager;
		function set AssetManager( value:AdvancedAssetManager ):void;
		
		function get SoundSystem():SoundManager;
		function set SoundSystem( value:SoundManager ):void;
		
		function get DebugSystem():DebugManager;
		function set DebugSystem( value:DebugManager ):void;
		
		function get Scene():SceneManager;
		function set Scene( value:SceneManager ):void;
		
		function get ActiveCamera():Camera;
		function set ActiveCamera( value:Camera ):void;
		
		function get IsInputEnabled():Boolean;
		function set IsInputEnabled( value:Boolean ):void;
		
		function get InputMap():Dictionary;
		function set InputMap( value:Dictionary ):void;
		
		function get AnimationSystem():AnimationManager;
		function set AnimationSystem( value:AnimationManager ):void;
		
		function get Pools():PoolManager;
		function set Pools( value:PoolManager ):void;
		
		function get Properties():EngineProperties;
		function set Properties( value:EngineProperties ):void;
		
		function get EngineEvents():EventBus;
		function set EngineEvents( value:EventBus ):void;
	}	
}