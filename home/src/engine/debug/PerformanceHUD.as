package engine.debug 
{
	import engine.interfaces.IDestroyable;
	import engine.interfaces.ITickable;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PerformanceHUD implements IDestroyable, ITickable
	{
		private var _Display:Sprite;
		private var _TextDictionary:Dictionary;
		private var _TextField:TextField;
		private var _Stage:Stage
		
		private var _UpdateAccumulator:uint;
		private var _LastUpdate:Number;
		private var _FPS:int;
		
		private const WIDTH:uint = 300;
		private const HEIGHT:uint = 400;
		private const UPDATE_TEXT_COUNT:uint = 10;
		
		public function PerformanceHUD( stage:Stage ) 
		{
			_Stage = stage;
			
			_Display = new Sprite;
			_TextField = new TextField;
			_TextDictionary = new Dictionary;
			_TextField.textColor = 0xCCFF00;
			
			draw();
			
			_Display.x = _Stage.stageWidth - WIDTH;
			_Display.addChild( _TextField );
			_TextField.width = WIDTH;
			_TextField.height = HEIGHT;
		}
		
		private function draw():void
		{
			var graphics:Graphics = _Display.graphics;
			graphics.beginFill( 0x000000, 0.8 );
			graphics.drawRect( 0, 0, WIDTH, HEIGHT );
			graphics.endFill();
		}
		
		public function tick( deltaTime:Number ):void
		{			
			_FPS = ( _FPS * 0.60 ) + ( 1 / deltaTime ) * 0.40;
			
			if ( _Display.visible && _UpdateAccumulator >= UPDATE_TEXT_COUNT )
			{
				_TextDictionary[ "FPS" ] = _FPS;
				_TextField.text = "";
				
				for ( var key:String in _TextDictionary )
				{
					_TextField.appendText( key + ": " + String( _TextDictionary[ key ] ) + "\n" );
				}
				
				_UpdateAccumulator = 0;
			}
			
			_UpdateAccumulator++;
		}
		
		public function toggleVisible():void
		{
			_Display.visible = !_Display.visible;
		}
		
		public function setValue( key:String, value:* ):void
		{
			_TextDictionary[ key ] = value;
		}
		
		public function get Display():Sprite { return _Display; }
		
		public function destroy():void
		{
			
		}
	}

}