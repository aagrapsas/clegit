package engine.debug 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DebugReporter 
	{
		private var _DebugChannels:Vector.<IDebugChannel>;
		private var _ErrorChannels:Vector.<IDebugChannel>;
		
		public function DebugReporter() 
		{
			initialize();
		}
		
		private function initialize():void
		{
			_DebugChannels = new Vector.<IDebugChannel>();
			_ErrorChannels = new Vector.<IDebugChannel>();
		}
		
		public function addDebugChannel( channel:IDebugChannel ):void
		{
			_DebugChannels.push( channel );
		}
		
		public function addErrorChannel( channel:IDebugChannel ):void
		{
			_ErrorChannels.push( channel );
		}
		
		public function debugLog( type:String, message:String ):void
		{
			for each ( var channel:IDebugChannel in _DebugChannels )
			{
				channel.report( type, message );
			}
		}
		
		public function errorLog( type:String, message:String ):void
		{
			for each ( var channel:IDebugChannel in _ErrorChannels )
			{
				channel.report( type, message );
			}
		}
		
		public function get errorChannels():Vector.<IDebugChannel> { return _ErrorChannels; }
		public function get debugChannels():Vector.<IDebugChannel> { return _DebugChannels; }
		
		public function destroy():void
		{
			// @TODO destroy
		}
	}
}