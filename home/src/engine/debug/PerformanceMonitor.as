package engine.debug 
{
	import engine.interfaces.ITickable;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class PerformanceMonitor
	{		
		private var _CurrentFrameStarts:Dictionary;
		
		private var _RunningAverages:Dictionary;
		private var _NumberOfDataPoints:Dictionary;
		
		private var _Maximums:Dictionary;
		private var _CallsPerFrame:Dictionary;
		
		private var _MaximumCalls:Dictionary;
		private var _RunningAverageCalls:Dictionary;
		private var _NumFrames:int;
		
		private var _Display:Sprite;
		private var _TextDisplay:TextField;
		
		private var _UpdateAccumulator:int;
		private const FRAMES_BEFORE_UPDATE:int = 10;
		
		public function PerformanceMonitor() 
		{
			_Display = new Sprite();
			_Display.visible = false;
			
			CONFIG::debug
			{
				_CurrentFrameStarts = new Dictionary();
				_RunningAverages = new Dictionary();
				_NumberOfDataPoints = new Dictionary();
				_Maximums = new Dictionary();
				_CallsPerFrame = new Dictionary();
				_MaximumCalls = new Dictionary();
				_RunningAverageCalls = new Dictionary();
			}
			
			draw();
		}
		
		public function frameStart():void
		{			
			CONFIG::debug
			{
				recordStart( "frame" );
			}
		}
		
		public function recordStart( event:String ):void
		{
			CONFIG::debug
			{
				const time:int = getTimer();
			
				_CurrentFrameStarts[ event ] = time;
			}
		}
		
		public function recordEnd( event:String ):void
		{
			CONFIG::debug
			{
				if ( !_RunningAverageCalls[ event ] )
					_RunningAverageCalls[ event ] = 0;
					
				if ( !_RunningAverages[ event ] )
					_RunningAverages[ event ] = 0;
					
				if ( !_MaximumCalls[ event ] )
					_MaximumCalls[ event ] = 0;
					
				if ( !_Maximums[ event ] )
					_Maximums[ event ] = 0;
					
				if ( !_NumberOfDataPoints[ event ] )
					_NumberOfDataPoints[ event ] = 0;
					
				if ( !_CallsPerFrame[ event ] )
					_CallsPerFrame[ event ] = 0;
				
				const time:int = getTimer();
				const elapsed:int = time - _CurrentFrameStarts[ event ];
				
				const runningAverage:int = _RunningAverages[ event ];
				const numDataPoints:int = _NumberOfDataPoints[ event ] + 1;
				
				const newAverage:int = runningAverage + ( elapsed - runningAverage ) / numDataPoints;
				
				if ( newAverage > 0 )
				{
					// Average time it takes per frame
					_RunningAverages[ event ] = newAverage;
					_NumberOfDataPoints[ event ] = numDataPoints;					
				}
				
				// Maximum duration
				if ( _Maximums[ event ] < elapsed )
				{
					_Maximums[ event ] = elapsed;
				}
				
				// Calls per frame				
				const callsPerFrame:Number = _CallsPerFrame[ event ] + 1;
					
				_CallsPerFrame[ event ] = callsPerFrame;
				
				// Overflow protection
				if ( numDataPoints == int.MAX_VALUE )
					_NumberOfDataPoints[ event ] = 2;
			}
		}
		
		public function frameEnd():void
		{
			CONFIG::debug
			{
				recordEnd( "frame" );				
				
				_NumFrames++;
				
				var keys:Vector.<String> = new Vector.<String>();
				
				// Handle updating cross-call data
				for ( var key:String in _CallsPerFrame )
				{
					if ( !_MaximumCalls[ key ] )
						_MaximumCalls[ key ] = 0;
						
					if ( !_RunningAverageCalls )
						_RunningAverageCalls[ key ] = 0;
					
					const callsThisFrame:int = _CallsPerFrame[ key ];
					
					if ( _MaximumCalls[ key ] < callsThisFrame )
						_MaximumCalls[ key ] = callsThisFrame;
						
					const runningAverage:int = _RunningAverageCalls[ key ];
					
					_RunningAverageCalls[ key ] = runningAverage + ( callsThisFrame - runningAverage ) / _NumFrames;
					
					keys.push( key );
				}
				
				// Reset frame call count
				for each ( var keyToFree:String in keys )
				{
					_CallsPerFrame[ keyToFree ] = 0;
				}
				
				// Handle overflow
				if ( _NumFrames == int.MAX_VALUE )
					_NumFrames = 2;
					
				// Update text
				updateText();
			}
		}
		
		private function draw():void
		{
			const width:int = 450;
			const height:int = 600;
			const x:int = 250;
			const y:int = 0;
			
			const bgColor:uint = 0x330033;
			const fgColor:uint = 0xCCFF00;
			
			_TextDisplay = new TextField();
			
			_TextDisplay.width = width;
			_TextDisplay.height = height;
			_TextDisplay.border = true;
			_TextDisplay.textColor = fgColor;
			_TextDisplay.backgroundColor = bgColor;
			
			_Display.graphics.beginFill( bgColor, 0.85 );
			_Display.graphics.drawRect( 0, 0, width, height );
			_Display.graphics.endFill();
			
			_Display.addChild( _TextDisplay );
			
			_Display.x = x;
			_Display.y = y;
		}
		
		public function toggleVisible():void
		{
			_Display.visible = !_Display.visible;
		}
		
		private function updateText():void
		{			
			_UpdateAccumulator++;
			
			if ( !_Display.visible || _UpdateAccumulator < FRAMES_BEFORE_UPDATE )
				return;
				
			_UpdateAccumulator = 0;
			
			_TextDisplay.text = "Performance [ perf MON~ster ] Profiler";
			
			for ( var key:String in _RunningAverages )
			{
				_TextDisplay.appendText( "\n" + key + "\n" );
				_TextDisplay.appendText( "\t" + " (MaxTime: " + ( _Maximums[ key ] / 1000 ) + " / AvgTime: " + ( _RunningAverages[ key ] / 1000 ) + ")" );
				_TextDisplay.appendText( "\t" + " (MaxCalls: " + _MaximumCalls[ key ] + " / AvgCalls: " + _RunningAverageCalls[ key ] + ")");
			}
		}
		
		public function get display():Sprite { return _Display; }
	}
}