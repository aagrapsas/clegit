package engine.debug
{
	import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class DebugLogger implements IDebugChannel
	{
		public function DebugLogger()
		{
			var date:Date = new Date();
			
			trace("*****DebugLogger initialized*****");
			trace("Started: " + date.toLocaleString());
			trace("Let's get this party started.");
			trace("*********************************");
		}
		
		public function report( type:String, message:String ):void
		{
			var time:Number = getTimer() / 1000;
			
			trace ( "[" + time + "] " + type + ": " + message );
		}
	}
}