package engine.debug 
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class EngineLogChannels 
	{
		public static const ERROR:String = "error";
		public static const WARNING:String = "warning";
		public static const DEBUG:String = "debug";
		public static const ASSETS:String = "assets";
		public static const ANIMATION:String = "animation";
		public static const AUDIO:String = "audio";
	}
}