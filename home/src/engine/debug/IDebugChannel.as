package engine.debug 
{
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public interface IDebugChannel 
	{
		function report( type:String, message:String ):void;
	}
}