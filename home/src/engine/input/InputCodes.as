package engine.input 
{
	import flash.ui.Keyboard;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class InputCodes 
	{
		public static function getCode( key:String ):uint
		{
			// 0-9 and a-z
			if ( key.length == 1 )
			{
				if ( key.charCodeAt( 0 ) < 65 )
				{
					// ActionScript codes for numbers are the same as ASCII
					return key.charCodeAt( 0 );
				}
				
				// Codes for ActionScript are 32 less than ASCII for letters
				return ( key.charCodeAt( 0 ) - 32 )as uint;
			}
			
			// F keys
			if ( key.substr( 0, 1 ).toLocaleLowerCase() == "f" )
			{
				var numeric:int = int( key.substr( 1 ) );
				
				numeric += 111;
				
				return numeric;
			}
			
			// Special cases
			switch( key )
			{
				case "left":
					return Keyboard.LEFT;
				case "right":
					return Keyboard.RIGHT;
				case "up":
					return Keyboard.UP;
				case "down":
					return Keyboard.DOWN;
				case "space":
					return Keyboard.SPACE;
				case "tab":
					return Keyboard.TAB;
				case "ctrl":
					return Keyboard.CONTROL;
			}
			
			return 0;
		}
		
		public static function getName( code:uint ):String
		{
			switch( code )
			{
				case Keyboard.LEFT:
					return "left";
				case Keyboard.RIGHT:
					return "right";
				case Keyboard.UP:
					return "up";
				case Keyboard.DOWN:
					return "down";
			}
			
			return String.fromCharCode( code );
		}
	}
}