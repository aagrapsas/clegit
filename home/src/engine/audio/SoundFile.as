package engine.audio 
{
	import engine.Actor;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IDestroyable;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SoundFile implements IDestroyable, ITickable
	{
		private var _FreeInstances:Vector.<SoundObject>;
		private var _PlayingInstances:Vector.<SoundObject>;
		private var _Sound:Sound;
		private var _IsLooping:Boolean;
		private var _Name:String;
		
		private var _AttenuationDistance:int;
		private var _AttenuationDistanceSquared:int;
		private var _GameData:IGameData;
		
		public var MaxInstances:int;
		
		private var _Volume:Number;
		
		public function SoundFile( gameData:IGameData, name:String, sound:Sound, isLooping:Boolean, maxInstances:int, volume:Number ) 
		{
			_Sound = sound;
			_IsLooping = isLooping;
			MaxInstances = maxInstances;
			_Name = name;
			_Volume = volume;
			_GameData = gameData;
			
			_FreeInstances = new Vector.<SoundObject>;
			_PlayingInstances = new Vector.<SoundObject>;
		}
		
		public function play( start:Number = 0, attenuationActor:Actor = null ):SoundObject
		{
			if ( _PlayingInstances.length < MaxInstances )
			{
				var channel:SoundObject;
				
				if ( _FreeInstances.length != 0 )
				{
					channel = _FreeInstances.pop();
				}
				else
				{
					channel = new SoundObject( _GameData, _Name, _Sound, _IsLooping, _AttenuationDistance, _AttenuationDistanceSquared );
				}
				
				if ( !channel )
				{
					Debug.errorLog( EngineLogChannels.AUDIO, "No channel!" );
				}
				
				channel.AttenuationActor = attenuationActor;
				
				channel.Transform = new SoundTransform( _Volume, 0 );
				
				channel.MaxVolume = _Volume;
				
				channel.play( start );
				 
				channel.addEventListener( Event.SOUND_COMPLETE, onSoundComplete, false, 0, true );
				 
				_PlayingInstances.push( channel );
				
				return channel;
			}
			
			return null;
		}
		
		private function onSoundComplete( e:Event ):void
		{
			SoundObject( e.target ).removeEventListener( Event.SOUND_COMPLETE, onSoundComplete );
			
			var index:int = _PlayingInstances.indexOf( e.target );
			
			if ( index > -1 )
			{
				_FreeInstances.push( e.target as SoundObject );
				_PlayingInstances.splice( index, 1 );
			}
			else
			{
				Debug.errorLog( EngineLogChannels.AUDIO, "Coundn't find returning sound object, uh oh." );
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			for each ( var channel:SoundObject in _PlayingInstances )
			{
				channel.tick( deltaTime );
			}
		}
		
		public function stop():void
		{
			for each ( var instance:SoundObject in _PlayingInstances )
			{
				instance.stop();
				
				// Add instance to free
				_FreeInstances.push( instance );
			}
			
			// Clear playing instances
			_PlayingInstances.splice( 0, _PlayingInstances.length );
		}
		
		public function setVolume( volume:Number ):void
		{
			for each ( var object:SoundObject in _PlayingInstances )
			{
				object.setVolume( volume );
			}
		}
		
		public function getVolume():Number
		{
			if ( _PlayingInstances.length > 0 )
				return _PlayingInstances[ 0 ].Transform.volume;
			return 0;
		}
		
		public function get ActiveSound():Sound { return _Sound; }
		public function get IsLooping():Boolean { return _IsLooping; }
		
		public function set AttenuationDistance( value:int ):void { _AttenuationDistance = value; }
		public function set AttenuationDistanceSquared( value:int ):void { _AttenuationDistanceSquared = value; }
		
		public function destroy():void
		{
			for each ( var channel:SoundChannel in _PlayingInstances )
			{
				channel.removeEventListener( Event.SOUND_COMPLETE, onSoundComplete );
			}
			
			_PlayingInstances = null;
			_FreeInstances = null;
			_Sound = null;
		}
	}
}