package engine.audio 
{
	import engine.Actor;
	import engine.debug.Debug;
	import engine.debug.EngineLogChannels;
	import engine.interfaces.IGameData;
	import engine.interfaces.ITickable;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class SoundObject extends EventDispatcher implements ITickable
	{	
		public var IsLooping:Boolean;
		public var Transform:SoundTransform;
		
		private var _Channels:Vector.<SoundChannel>;
		private var _Sound:Sound;
		
		private var _IsPlaying:Boolean = false;
		private var _TimeAccumulator:Number = 0;
		private var _StartStamp:Number = 0;
		
		private var _Name:String;
		
		private var _Current:int = 0;
		
		private var _AttenuationActor:Actor;
		private var _AttenuationDistance:int;
		private var _AttenuationDistanceSquared:int;
		private var _GameData:IGameData;
		
		private var _FrameAccumulator:int = 0;
		private var _MaxVolume:Number = 1;
		
		public function SoundObject( gameData:IGameData, name:String, sound:Sound, isLooping:Boolean, attenuationDistance:int = 0, attenuationDistanceSquared:int = 0, attenuationActor:Actor = null, maxVolume:Number = 1 ) 
		{
			_Sound = sound;
			_AttenuationActor = attenuationActor;
			_GameData = gameData;
			_AttenuationDistance = attenuationDistance;
			_AttenuationDistanceSquared = attenuationDistanceSquared;
			
			_Channels = new Vector.<SoundChannel>;
			
			_Channels.push( new SoundChannel );
			_Channels.push( new SoundChannel );
			
			_Name = name;
			
			_MaxVolume = maxVolume;
			
			IsLooping = isLooping;
			
			Transform = new SoundTransform( 1, 0 );
		}
		
		public function play( start:Number = 0 ):void
		{
			_IsPlaying = true;
			_TimeAccumulator = start;
			_Current = 0;
			
			_Channels[ 0 ] = _Sound.play( start, 0, Transform );
			
			// @HACK: find out why this is happening!
			if ( !_Channels[ 0 ] )
				return;
			
			if ( !IsLooping )
				_Channels[ 0 ].addEventListener( Event.SOUND_COMPLETE, onSoundComplete );
				
			_StartStamp = _GameData.Scene.GameTime;
		}
		
		private function onSoundComplete( e:Event ):void
		{	
			_IsPlaying = false;
			
			if ( !IsLooping )
				this.dispatchEvent( new Event( Event.SOUND_COMPLETE ) );
		}
		
		public function stop():void
		{
			_IsPlaying = false;
			
			for each ( var channel:SoundChannel in _Channels )
			{
				if ( channel )
					channel.stop();
			}
			
			this.dispatchEvent( new Event( Event.SOUND_COMPLETE ) );
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !_IsPlaying )
				return;
				
			// @HACK: stopping issue with sounds crashing
			if ( _Channels[ _Current ] == null )
			{
				stop();
				return;
			}
				
			if ( IsLooping && IsBlendFinished )
			{
				if ( _Current == 0 )
				{
					_Current++;
				}
				else
				{
					_Current--;
				}
				
				_TimeAccumulator = 0;
				_Channels[ _Current ] = _Sound.play( 0, 0, Transform );
				
				// @HACK: remove this eventually and have a max number of playing channels or something
				if ( !_Channels[ _Current ] )
				{
					stop();
					return;
				}
				
				_StartStamp = this.Current;
			}
			
			// Don't update attenuation every frame
			if ( _AttenuationActor && _FrameAccumulator++ > 2 )
			{
				const distX:int = _AttenuationActor.X + _GameData.ActiveCamera.X;
				const distY:int = _AttenuationActor.Y - _GameData.ActiveCamera.Y;
				
				const distSquared:int = distX * distX + distY * distY;
				
				if ( distSquared <= _AttenuationDistanceSquared )
				{
					const distance:int = Math.sqrt( distSquared );
					const alpha:Number = Math.min( 1 - distance / _AttenuationDistance, _MaxVolume );
					
					Transform.volume = alpha;
					
					_Channels[ _Current ].soundTransform = Transform;
				}
				
				_FrameAccumulator = 0;
			}
			
			_TimeAccumulator = Math.min( _TimeAccumulator + deltaTime, _Sound.length / 1000 );
		}
		
		public function setVolume( volume:Number ):void
		{
			Transform.volume = volume;
			
			_Channels[ _Current ].soundTransform = Transform;
		}
		
		public function set AttenuationActor( value:Actor ):void { _AttenuationActor = value; }
		public function set MaxVolume( value:Number ):void { _MaxVolume = value; }
		
		public function get Length():Number { return _Sound.length / 1000; }
		public function get PlayedLength():Number { return ( Current - _StartStamp ); }
		public function get Current():Number { return ( getTimer() / 1000 ); }
		public function get IsBlendFinished():Boolean { return ( PlayedLength >= ( Length - 0.18 ) ) || !_IsPlaying; }
		public function get IsFinished():Boolean { return PlayedLength >= Length || !_IsPlaying; }
	}
}