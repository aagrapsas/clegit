package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.Dictionary;
	import game.Game;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	
	[Frame(factoryClass="UnifiedPreloader")]
	public class Main extends Sprite 
	{
		private var _Game:Game;
		
		private var _PreloadedAssets:Dictionary;
		private var _MainInclude:MainInclude;
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			
			
			CONFIG::ship
			{
				_MainInclude = new MainInclude();
				
				_MainInclude.addEventListener( Event.COMPLETE, onPreloadingDone );
				
				this.addChild( _MainInclude );
			}
			
			if ( CONFIG::ship == false )
			{
				_Game = new Game( this.stage, null );
			}
		}
		
		private function onPreloadingDone( e:Event ):void
		{
			var preloadedAssets:Dictionary = _MainInclude.getAssets();
			
			this.removeChild( _MainInclude );
			
			_Game = new Game( this.stage, preloadedAssets );
		}
	}
}