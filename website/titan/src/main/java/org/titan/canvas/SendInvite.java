package org.titan.canvas;

import java.util.List;
import java.io.Console;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.social.RevokedAuthorizationException;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;


@Controller
public class SendInvite {
	private final Facebook facebook;

	@Inject
	private ConnectionRepository usersConnectionRepository;
	
	@Inject
	public SendInvite(Facebook facebook) {
		this.facebook = facebook;
	}

	@RequestMapping(value = "/SendInvite", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody String DoStuff(HttpServletRequest request, @RequestParam String friendName) {
		/*List<Reference> friends = facebook.friendOperations().getFriends();
		model.addAttribute("friends", friends);*/
		System.out.println("WallPost: Got request " + request.getContentLength());
		System.out.println("WallPost: received post: " + friendName);
		//Then read body with request.getInputStream() or something.
		//this.facebook.post(arg0, arg1, arg2)
		System.out.println("Access token:" + usersConnectionRepository.findPrimaryConnection(Facebook.class).createData().getAccessToken());
		//this.facebook.fetchObject(appRequestId, AppRequest.class);
	
		return "SendInvite";
	}
	
	@ExceptionHandler(RevokedAuthorizationException.class)
	public String handleException() {
		return "signin";
	}
}
