package org.titan.canvas;

import java.util.List;
import java.io.Console;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.social.RevokedAuthorizationException;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;

@Controller
public class FlashTest {
	private final Facebook facebook;

	@Inject
	public FlashTest(Facebook facebook) {
		this.facebook = facebook;
	}

	@RequestMapping(value = "/FlashTest", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody String DoStuff(HttpServletRequest request){//(@RequestParam String foo) {
		/*List<Reference> friends = facebook.friendOperations().getFriends();
		model.addAttribute("friends", friends);*/
		System.out.println("FlashTest: Got request " + request.getContentLength());
		//Then read body with request.getInputStream() or something. 
		return "FlashTest";
	}
	
	@ExceptionHandler(RevokedAuthorizationException.class)
	public String handleException() {
		return "signin";
	}
}
