/*
 * Copyright 2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.titan.canvas.config;

import java.io.Console;
import java.util.*;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;
import org.titan.canvas.user.UserInterceptor;

/**
 * Spring MVC Configuration.
 * @author Keith Donald
 */
@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new UserInterceptor(usersConnectionRepository));
	}

	public void addViewControllers(ViewControllerRegistry registry) {
//		registry.addViewController("/signin");
		//registry.addViewController("/FlashTest");
	}

	@Bean
	public ViewResolver viewResolver(){
		List<ViewResolver> viewResolvers = new ArrayList<ViewResolver>();
		List<View> defaultViews = new ArrayList<View>();
		
		ContentNegotiatingViewResolver contentViewResolver = new ContentNegotiatingViewResolver();
		
		Map<String, String> mediaTypes = new HashMap<String, String>();
		mediaTypes.put("json", "application/json");
		mediaTypes.put("html", "text/html");
		contentViewResolver.setMediaTypes(mediaTypes);
		
		InternalResourceViewResolver jspViewResolver = new InternalResourceViewResolver();
		jspViewResolver.setPrefix("/WEB-INF/views/");
		jspViewResolver.setSuffix(".jsp");
		viewResolvers.add(jspViewResolver);
		
		//BeanNameViewResolver beanViewResolver = new BeanNameViewResolver();
		//viewResolvers.add(beanViewResolver);
		
		MappingJacksonJsonView jacksonJsonView = new MappingJacksonJsonView();
		defaultViews.add(jacksonJsonView);
		
		contentViewResolver.setDefaultViews(defaultViews);
		contentViewResolver.setViewResolvers(viewResolvers);
		
		
		return contentViewResolver;
	}
	
	
	
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
	
	private @Inject UsersConnectionRepository usersConnectionRepository;

}
