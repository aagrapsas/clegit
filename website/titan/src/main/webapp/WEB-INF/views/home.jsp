<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html xmlns="http://www.w3.org/1999/xhtml"
  xmlns:fb="https://www.facebook.com/2008/fbml">
  <head>
    <title>Request Example</title>
  </head>

  <body>
    <div id="fb-root"></div>
    <script src="http://connect.facebook.net/en_US/all.js"></script>
    <p>
      <input type="button"
        onclick="sendRequestToRecipients(); return false;"
        value="Send Request to Users Directly"
      />
      <input type="text" value="User ID" name="user_ids" />
      </p>
    <p>
    <input type="button"
      onclick="sendRequestViaMultiFriendSelector(); return false;"
      value="Send Request to Many Users with MFS"
    />
    </p>
    
    <script>
      FB.init({
        appId  : '123399791102861',
        status: false,
        cookie: false,
        xfbml: false,
      });

      function sendRequestToRecipients() {
        var user_ids = document.getElementsByName("user_ids")[0].value;
        FB.ui({method: 'apprequests',
          message: 'My Great Request',
          to: user_ids, 
        }, requestCallback);
      }

      function sendRequestViaMultiFriendSelector() {
    	  FB.ui({ 
    		  method: 'apprequests',
    		  display: 'iframe',
    		  access_token: '${access_token}',
    		  title: 'Sample Title',
    		  message: 'Sample Message',
    		  data: 'some data here',
    		  filters: ['all'],
    		},
    		function(response) {
    		  if (response && response.request_ids) {
    		    alert('Request was sent');
    		  } else {
    		    alert('Request was not sent');
    		  }
    		});
      }
    </script>
  </body>
</html>