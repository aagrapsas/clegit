<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="https://www.facebook.com/2008/fbml">
	<head>
		<title>Shadows of Siren</title>
	</head>
	<link type="text/css" rel="stylesheet" href="resources/images/XdvPy4PtQTU.css">
	<link type="text/css" rel="stylesheet" href="resources/images/8GBMQ7dNb7u.css">
	<link type="text/css" rel="stylesheet" href="resources/images/DmbX_xJb03R.css">
	<link type="text/css" rel="stylesheet" href="resources/images/sKs1dRgKcrM.css">
	<script src="resources/js/swfobject.js" type="text/javascript"></script>
	<script type="text/javascript">
		var flashvars = 
		{
			resourcePath: "resources",
			buildnumber: "${buildNumber0}"
		};
		var params = 
		{
			menu: "false",
			scale: "noScale",
			allowFullscreen: "true",
			allowScriptAccess: "always",
			bgcolor: "#000000",
			wmode: "direct"
		};
		var attributes =
		{
			id: "home"
		};
		swfobject.embedSWF("resources/home.swf?cachebreaker=${buildNumber0}", "altContent", "100%", "600", "10.0.0", "resources/expressInstall.swf", flashvars, params, attributes);
	</script>
	
<body>
<div id="fb-root"></div>
<script>
    var accessToken;
    
      // Load the SDK Asynchronously
      (function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js";
         ref.parentNode.insertBefore(js, ref);
       }(document));

      // Init the SDK upon load
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '181478818635345', // App ID
          channelUrl : '//'+window.location.hostname+'/sos_demo/resources/channel.html', // Path to your Channel File
          status     : false, // check login status
          cookie     : false, // enable cookies to allow the server to access the session
          xfbml      : false  // parse XFBML
        });
      };
      
      function sendRequestViaMultiFriendSelector() {
    	  
		          FB.ui({method: 'apprequests',
		            message: 'Invite your friends!',
		            display: 'iframe',
		            access_token: '${access_token}',
		          }, requestCallback);
    		  
        }
        
        function requestCallback(response) {
          // Handle callback here
        }
    </script>
	<div id="altContent">
		<h1>You don't have flash!</h1>
		<p><a href="http://www.adobe.com/go/getflashplayer">Get Adobe Flash player</a></p>
	</div>
	<hr>
	<p>
    <input type="button"
      onclick="sendRequestViaMultiFriendSelector(); return false;"
      value="Send Request to Many Users with MFS"
    />
    </p>
</body>
</html>