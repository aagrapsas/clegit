/*
 * Copyright 2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.social.canvas;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.env.Environment;
import org.springframework.social.RevokedAuthorizationException;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.CookieGenerator;

/**
 * Simple little @Controller that invokes Facebook and renders the result.
 * The injected {@link Facebook} reference is configured with the required authorization credentials for the current user behind the scenes.
 * @author Keith Donald
 */
@Controller
public class HomeController {

	@Inject
	private Environment environment;
	
	private final Facebook facebook;
	
	@Inject
	private ConnectionRepository usersConnectionRepository;
	
	@Inject
	public HomeController(Facebook facebook) {
		this.facebook = facebook;
	}

	@RequestMapping(value = "/" /*, method = RequestMethod.GET*/) // FB wants the canvas page to take POST requests (?)
	public String home(HttpServletRequest request, Model model, HttpServletResponse response) {
		/*boolean forbidden = true;
		Cookie[] cookies = request.getCookies();
		if (cookies == null) {

		}
		else
		{
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("secret_message")) {
					if (cookie.getValue().equals("wombat"))
					{
						forbidden = false;
					}
				}
			}
		}
		if (forbidden)
		{
			String secret = request.getParameter("secret");
			if (secret != null && secret.equals("wombat"))
			{
				forbidden = false;
				
				CookieGenerator secretCookieGenerator = new CookieGenerator();
				secretCookieGenerator.setCookieName("secret_message");
				secretCookieGenerator.setCookieMaxAge(-1);
				secretCookieGenerator.addCookie(response, secret);
			}
		}
		
		if (forbidden)
		{
			try {
				response.sendRedirect("forbidden.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		
		model.addAttribute("access_token", usersConnectionRepository.findPrimaryConnection(Facebook.class).createData().getAccessToken());
		model.addAttribute("buildNumber0", environment.getProperty("buildNumber0"));
		return "home";
	}
	
	@ExceptionHandler(RevokedAuthorizationException.class)
	public String handleException() {
		return "signin";
	}
	
}