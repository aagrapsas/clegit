﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;
using System.IO;

namespace SwiftRiver
{
    public class ExportFile
    {
        public String Type { get; set; }
        public List<String> Paths { get; set; }
        public String Name { get; set; }
        public Boolean DoesSkipFileCompress { get; set; }

        [XmlIgnore]
        public Double FileSize { get; set; }

        public ExportFile()
        {
            Paths = new List<string>();
        }

        public void UpdateFileSize()
        {
            double size = 0;

            foreach (String path in Paths)
            {
                FileInfo info = new FileInfo(path);
                size += (info.Length / 1024);
            }

            FileSize = size;
        }

        public override string ToString()
        {
            String size = FileSize.ToString();
            
            if (size.Contains("."))
            {
                size = size.Substring(0, Math.Min(size.IndexOf(".") + 2, size.Length));
            }

            return Name + " [" + size + " kb]";
        }
    }
}
