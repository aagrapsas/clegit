package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 4/11/2012 5:37:59 PM
	// -------------------------------------------

	public class test_manifest
	{
		// test_image
		// Image
		[Embed ( source="C:/Users/Andrew/Desktop/far.jpg" )]
		public var test_image:Class;

		// xml_test
		// XML
		[Embed ( source="../../../../../home/bin/data/additional_config.xml" )]
		public var xml_test:Class;

	}
}
// EOF
