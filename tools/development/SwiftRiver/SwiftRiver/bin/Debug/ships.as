package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 5/14/2012 10:24:13 PM
	// -------------------------------------------

	import flash.display.Sprite;

	public class ships extends Sprite
	{
		// argo_battleship_img
		// Image
		[Embed ( source="../../../../../../export/images/ships/argo_battleship.png" )]
		public var argo_battleship_img:Class;

	}
}
// EOF
