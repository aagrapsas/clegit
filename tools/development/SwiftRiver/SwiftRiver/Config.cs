﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml.Serialization;

using System.Windows.Forms;

namespace SwiftRiver
{
    public class Config
    {
        private static String Path = System.Environment.CurrentDirectory + "\\config.xml";

        public String FlexHome { get; set; }
        public String MasterManifestPath { get; set; }
        public int QuantSpeed { get; set; }

        [XmlIgnore]
        public Boolean DoesExist { get; private set; }

        public Config()
        {
            QuantSpeed = 10;
        }

        public static Config Load()
        {
            Config config = new Config();

            if (File.Exists(Path))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Config));

                    TextReader reader = new StreamReader(Path);

                    config = (Config)serializer.Deserialize(reader);

                    reader.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                config.DoesExist = true;
            }
            else
            {
                config.DoesExist = false;
            }

            return config;
        }

        public void Save()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Config));

                TextWriter writer = new StreamWriter(Path);

                serializer.Serialize(writer, this);

                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static String ConvertToRelative(String path)
        {
            String basePath = Application.StartupPath;

            Uri uri1 = new Uri(path, UriKind.RelativeOrAbsolute);

            if (uri1.IsAbsoluteUri)
            {
                Uri uri2 = new Uri(basePath + "/", UriKind.Absolute);

                Uri relativeUri = uri2.MakeRelativeUri(uri1);

                return Uri.UnescapeDataString(relativeUri.ToString()).Replace('/', System.IO.Path.DirectorySeparatorChar);
            }

            return path;
        }
    }
}
