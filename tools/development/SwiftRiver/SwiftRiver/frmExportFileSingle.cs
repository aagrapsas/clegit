﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SwiftRiver
{
    public partial class frmExportFileSingle : Form
    {
        public ExportFile File { get; set; }

        public frmExportFileSingle()
        {
            InitializeComponent();
        }

        private void frmExportFileSingle_Load(object sender, EventArgs e)
        {
            if (File == null)
            {
                File = new ExportFile();
            }
            else
            {
                txtName.Text = File.Name;

                if (File.Paths.Count > 0)
                {
                    txtPath.Text = File.Paths[0];
                }

                cboType.Text = File.Type;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            File.Name = txtName.Text;

            if (File.Paths.Count > 0)
            {
                File.Paths[0] = txtPath.Text;
            }
            else
            {
                File.Paths.Add(txtPath.Text);
            }

            File.Type = cboType.Text;

            this.Close();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Title = "Select a file to load into the swf";
            dialog.Multiselect = false;

            DialogResult result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            txtPath.Text = Config.ConvertToRelative(dialog.FileName);

            String format = txtPath.Text.Substring(txtPath.Text.LastIndexOf(".") + 1);
            format = format.ToLower();

            if (format == "png" || format == "jpg" || format == "jpeg")
            {
                cboType.Text = "Image";
            }
            else if (format == "mp3" || format == "wav")
            {
                cboType.Text = "Audio";
            }
            else if (format == "xml")
            {
                cboType.Text = "XML";
            }
            else if (format == "swf")
            {
                cboType.Text = "SWF";
            }
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
