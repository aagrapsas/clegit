﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

using System.IO;

namespace SwiftRiver
{
    public partial class frmMain : Form
    {
        private Config AppConfig;
        private ExportConfig Manifest;

        [DllImport("Kernel32.dll")]
        static extern Boolean AllocConsole();

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnAddManifest_Click(object sender, EventArgs e)
        {
            frmAddExporter exporter = new frmAddExporter();

            DialogResult result = exporter.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            Manifest.ExportRules.Add(exporter.Rule);

            lstConfigs.DataSource = null;
            lstConfigs.DataSource = Manifest.ExportRules;

            Manifest.Save();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (!AllocConsole())
            {
                MessageBox.Show("Failed to allocate console!");
            }

            AppConfig = Config.Load();
            Manifest = ExportConfig.Load();

            if (AppConfig.DoesExist == false)
            {
                SelectPaths();
            }

            lstConfigs.DataSource = Manifest.ExportRules;
        }

        private Boolean SelectPaths()
        {
            OpenFileDialog flex = new OpenFileDialog();

            flex.DefaultExt = "*.exe";
            flex.Title = "Select the mxmlc.exe location";
            flex.Filter = "Executable (*.exe)|*.exe";

            DialogResult result = flex.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return false;
            }

            AppConfig.FlexHome = flex.FileName;

            AppConfig.Save();

            return true;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            SelectPaths();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            ExportRule rule = lstConfigs.SelectedItem as ExportRule;

            Manifest.ExportRules.Remove(rule);

            Manifest.Save();

            lstConfigs.DataSource = null;
            lstConfigs.DataSource = Manifest.ExportRules;
        }

        private void lstConfigs_DoubleClick(object sender, EventArgs e)
        {
            if (lstConfigs.SelectedItem == null)
            {
                return;
            }

            ExportRule rule = lstConfigs.SelectedItem as ExportRule;

            frmAddExporter exporter = new frmAddExporter();

            exporter.Rule = rule;

            DialogResult result = exporter.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            lstConfigs.DataSource = null;
            lstConfigs.DataSource = Manifest.ExportRules;

            Manifest.Save();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (lstConfigs.SelectedItem == null)
            {
                return;
            }

            ExportRule rule = lstConfigs.SelectedItem as ExportRule;

            AS3FileWriter.WriteRule(rule, AppConfig.QuantSpeed);
            SWFCompiler.CompileSwf(AppConfig.FlexHome, Application.StartupPath + "\\" + rule.Name + ".as", Application.StartupPath + "\\" + rule.ExportPath);
        }

        private void btnExportAll_Click(object sender, EventArgs e)
        {
            foreach (ExportRule rule in Manifest.ExportRules)
            {
                AS3FileWriter.WriteRule(rule, AppConfig.QuantSpeed);
                SWFCompiler.CompileSwf(AppConfig.FlexHome, Application.StartupPath + "\\" + rule.Name + ".as", Application.StartupPath + "\\" + rule.ExportPath);
            }
        }

        private void btnExportMaster_Click(object sender, EventArgs e)
        {
            if (AppConfig.MasterManifestPath == null)
            {
                SaveFileDialog dialog = new SaveFileDialog();

                dialog.DefaultExt = "*.xml";
                dialog.Title = "Select the engine's manifest.xml location";
                dialog.Filter = "XML (*.xml)|*.xml";

                DialogResult result = dialog.ShowDialog();

                if (result != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                AppConfig.MasterManifestPath = dialog.FileName;

                AppConfig.Save();
            }

            StreamWriter writer = new StreamWriter(AppConfig.MasterManifestPath);

            writer.WriteLine("<manifests>");

            foreach (ExportRule rule in Manifest.ExportRules)
            {
                writer.WriteLine("\t<manifest name=\"" + rule.Name + "\">");

                foreach (ExportFile file in rule.Files)
                {
                    int count = file.Paths.Count;

                    if (file.DoesSkipFileCompress)
                    {
                        count = (int)Math.Ceiling((double)count / 2);
                    }

                    writer.WriteLine("\t\t<file name=\"" + file.Name + "\" type=\"" + file.Type + "\" count=\"" + count + "\"/>");
                }

                writer.WriteLine("\t</manifest>");
            }

            writer.WriteLine("</manifests>");

            writer.Close();
        }

        private void btnCompileGame_Click(object sender, EventArgs e)
        {
            String toRemove = "bin\\data\\generated\\manifest.xml";

            String topLevel = AppConfig.MasterManifestPath.Substring(0, AppConfig.MasterManifestPath.Length - toRemove.Length);

            String mainPath = topLevel + "src\\Main.as";

            String outPut = topLevel + "bin\\home.swf";

            String libPath = topLevel + "lib";

            String additionalOptions = "-load-config+=" + topLevel + "obj\\homeConfig.xml -define=CONFIG::ship,true";

            SWFCompiler.CompileSwf(AppConfig.FlexHome, mainPath, outPut, additionalOptions);
        }
    }
}
