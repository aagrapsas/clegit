package  
{
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Info 
	{
		public var key:String;
		public var type:String;
		public var cost:int;
		public var numVariants:int = 0;
		
		public function Info( key:String, type:String, cost:int, xml:XML ) 
		{
			this.key = key;
			this.type = type;
			this.cost = cost;
			
			if ( type == "chassis" )
			{
				for each ( var hardpoint:XML in xml.chassis.hardpointf )
				{
					numVariants++;
				}
			}
		}
	}
}