package  
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Catalogue 
	{
		private var xmlToLoad:Vector.<String>;
		private var currentXML:int = 0;
		private var xmlLoader:URLLoader;
		private var rawXML:Dictionary;
		
		private var catalogue:Dictionary;
		private var cost:Dictionary;
		
		private var difficultyMap:Dictionary;
		
		private const COST:String = "data/additional_config.xml";
		private const ARMOR:String = "data/ship_components/armor.xml";
		private const CHASSIS:String = "data/ship_components/chassis.xml";
		private const ENGINES:String = "data/ship_components/engines.xml";
		private const SENSORS:String = "data/ship_components/sensors.xml"
		private const SHIELDS:String = "data/ship_components/shields.xml"
		private const WEAPONS:String = "data/weapons/weapons.xml";
		
		public function Catalogue() 
		{
			xmlToLoad = new Vector.<String>();
			rawXML = new Dictionary();
			catalogue = new Dictionary();
			cost = new Dictionary();
			
			xmlToLoad.push( COST );
			xmlToLoad.push( ARMOR );
			xmlToLoad.push( CHASSIS );
			xmlToLoad.push( ENGINES );
			xmlToLoad.push( SENSORS );
			xmlToLoad.push( SHIELDS );
			xmlToLoad.push( WEAPONS );
			
			loadXMLFiles();
		}
		
		private function loadXMLFiles():void
		{
			loadXML( xmlToLoad[currentXML] );
		}
		
		private function loadXML( path:String ):void
		{
			xmlLoader = new URLLoader();
			xmlLoader.addEventListener( Event.COMPLETE, onXMLComplete );
			xmlLoader.load(new URLRequest( xmlToLoad[ currentXML ] ));
		}
		
		private function onXMLComplete( e:Event ):void
		{
			rawXML[ xmlToLoad[ currentXML ] ] = new XML( xmlLoader.data );
			
			currentXML++;
			
			if ( currentXML >= xmlToLoad.length )
			{
				parseXML();
			}
			else
			{
				loadXML( xmlToLoad[ currentXML ] );
			}
		}
		
		private function parseXML():void
		{
			var xml:XML = null;
			var info:Info = null;
			
			// costs
			xml = rawXML[ COST ];
			
			for each ( var costXML:XML in xml.info )
			{
				cost[ String( costXML.@target ) ] = int( costXML.@softCost );
			}
			
			// armor
			xml = rawXML[ ARMOR ];
			
			catalogue[ ARMOR ] = new Dictionary();
			
			for each ( var armorXML:XML in xml.armor )
			{
				info = new Info( armorXML.@key, "armor", cost[ String( armorXML.@key ) ] );
				catalogue[ ARMOR ][ info.key] = info;
			}
			
			// chassis
			xml = rawXML[ CHASSIS ];
			catalogue[ CHASSIS ] = new Dictionary();
			
			for each ( var chassisXML:XML in xml.chassis )
			{
				info = new Info( chassisXML.@key, "chassis", cost[ String( chassisXML.@key ) ] );
				catalogue[ CHASSIS ][ info.key ] = info;
			}
			
			// engines
			xml = rawXML[ ENGINES ];
			catalogue[ ENGINES ] = new Dictionary();
			
			for each ( var engineXML:XML in xml.engine )
			{
				info = new Info( engineXML.@key, "engine", cost[ String( engineXML.@key ) ] );
				catalogue[ ENGINES ][ info.key ] = info;
			}
			
			// sensors
			xml = rawXML[ SENSORS ];
			catalogue[ SENSORS ] = new Dictionary();
			
			for each ( var sensorsXML:XML in xml.sensor )
			{
				info = new Info( sensorsXML.@key, "sensor", cost[ String( sensorsXML.@key ) ] );
				catalogue[ SENSORS ][ info.key ] = info;
			}
			
			// shields
			xml = rawXML[ SHIELDS ];
			catalogue[ SHIELDS ] = new Dictionary();
			
			for each ( var shieldXML:XML in xml.shield )
			{
				info = new Info( shieldXML.@key, "shield", cost[ String( shieldXML.@key ) ] );
				catalogue[ SHIELDS ][ info.key ] = info;
			}
			
			// weapons
			xml = rawXML[ WEAPONS ];
			catalogue[ WEAPONS ] = new Dictionary();
			
			for each ( var weaponXML:XML in xml.weapon )
			{
				info = new Info( weaponXML.@name, "weapon", cost[ String( weaponXML.@name ) ] );
				catalogue[ WEAPONS ][ info.key ] = info;
			}
			
			difficultyMap = new Dictionary();
			
			for each ( var chassis:Info in catalogue[ CHASSIS ] )
			{
				var comboCost:int = chassis.cost;
				
				for ( var i:int = 0; i < chassis.numVariants; i++ )
				{
					for each ( var weapon:Info in catalogue[ WEAPONS ] )
					{
						
					}
				}
			}
		}
	}
}