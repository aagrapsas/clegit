﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlchemistStone
{
    class CSVHeading
    {
        public Dictionary<String, int> Mapping { get; private set; }

        public CSVHeading()
        {
            Mapping = new Dictionary<string, int>();
        }
    }
}
