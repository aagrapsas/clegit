﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace AlchemistStone
{
    class StupidSimpleCSV
    {
        public List<CSVEntry> Entries { get; private set; }
        public CSVHeading Heading { get; private set; }

        public StupidSimpleCSV()
        {

        }

        private String Sanitize(String input)
        {
            if (input == "")
                return input;

            if (input.Substring(0, 1) == "\"")
                input = input.Substring(1);

            if (input.Substring(input.Length - 1) == "\"")
                input = input.Substring(0, input.Length - 1);

            return input;
        }

        public void Load(String path)
        {
            StreamReader reader = new StreamReader(path);

            String line = null;

            Heading = new CSVHeading();

            Entries = new List<CSVEntry>();

            bool hasReadHeading = false;

            while ((line = reader.ReadLine()) != null)
            {
                String[] tokenized = line.Split(',');

                if (hasReadHeading == false)
                {
                    hasReadHeading = true;

                    int runningIndex = 0;

                    foreach (String segment in tokenized)
                    {
                        Heading.Mapping.Add(Sanitize(segment), runningIndex);

                        runningIndex++;
                    }
                }
                else
                {
                    CSVEntry entry = new CSVEntry();
                    entry.Fields = new List<string>();
                    entry.Heading = Heading;

                    foreach (String segment in tokenized)
                    {
                        entry.Fields.Add(Sanitize(segment));
                    }

                    Entries.Add(entry);
                }
            }

            reader.Close();
        }
    }
}
