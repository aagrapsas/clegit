﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlchemistStone
{
    class CSVEntry
    {
        public List<String> Fields { get; set; }
        public CSVHeading Heading { get; set; }

        public CSVEntry()
        {
            
        }

        public String GetField(String field)
        {
            if (Heading.Mapping.ContainsKey(field) == false)
                return null;

            int index = Heading.Mapping[field];

            return Fields[index];
        }
    }
}
