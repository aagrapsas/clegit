﻿namespace AlchemistStone
{
    partial class ConverterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShipTemplates = new System.Windows.Forms.Button();
            this.btnSelectNewPaths = new System.Windows.Forms.Button();
            this.btnAdditionalConfig = new System.Windows.Forms.Button();
            this.btnArmor = new System.Windows.Forms.Button();
            this.btnEngine = new System.Windows.Forms.Button();
            this.btnShield = new System.Windows.Forms.Button();
            this.btnSensor = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.btnWeapons = new System.Windows.Forms.Button();
            this.btnScores = new System.Windows.Forms.Button();
            this.btnPlatforms = new System.Windows.Forms.Button();
            this.btnEvasion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnShipTemplates
            // 
            this.btnShipTemplates.Location = new System.Drawing.Point(10, 59);
            this.btnShipTemplates.Name = "btnShipTemplates";
            this.btnShipTemplates.Size = new System.Drawing.Size(203, 33);
            this.btnShipTemplates.TabIndex = 0;
            this.btnShipTemplates.Text = "Convert Ship Templates";
            this.btnShipTemplates.UseVisualStyleBackColor = true;
            this.btnShipTemplates.Click += new System.EventHandler(this.btnShipTemplates_Click);
            // 
            // btnSelectNewPaths
            // 
            this.btnSelectNewPaths.Location = new System.Drawing.Point(10, 6);
            this.btnSelectNewPaths.Name = "btnSelectNewPaths";
            this.btnSelectNewPaths.Size = new System.Drawing.Size(203, 35);
            this.btnSelectNewPaths.TabIndex = 1;
            this.btnSelectNewPaths.Text = "Select New Paths";
            this.btnSelectNewPaths.UseVisualStyleBackColor = true;
            this.btnSelectNewPaths.Click += new System.EventHandler(this.btnSelectNewPaths_Click);
            // 
            // btnAdditionalConfig
            // 
            this.btnAdditionalConfig.Location = new System.Drawing.Point(10, 99);
            this.btnAdditionalConfig.Name = "btnAdditionalConfig";
            this.btnAdditionalConfig.Size = new System.Drawing.Size(203, 35);
            this.btnAdditionalConfig.TabIndex = 2;
            this.btnAdditionalConfig.Text = "Convert Additional Config";
            this.btnAdditionalConfig.UseVisualStyleBackColor = true;
            this.btnAdditionalConfig.Click += new System.EventHandler(this.btnAdditionalConfig_Click);
            // 
            // btnArmor
            // 
            this.btnArmor.Location = new System.Drawing.Point(10, 139);
            this.btnArmor.Name = "btnArmor";
            this.btnArmor.Size = new System.Drawing.Size(203, 35);
            this.btnArmor.TabIndex = 3;
            this.btnArmor.Text = "Convert Armor";
            this.btnArmor.UseVisualStyleBackColor = true;
            this.btnArmor.Click += new System.EventHandler(this.btnArmor_Click);
            // 
            // btnEngine
            // 
            this.btnEngine.Location = new System.Drawing.Point(10, 179);
            this.btnEngine.Name = "btnEngine";
            this.btnEngine.Size = new System.Drawing.Size(203, 36);
            this.btnEngine.TabIndex = 4;
            this.btnEngine.Text = "Convert Engines";
            this.btnEngine.UseVisualStyleBackColor = true;
            this.btnEngine.Click += new System.EventHandler(this.btnEngine_Click);
            // 
            // btnShield
            // 
            this.btnShield.Location = new System.Drawing.Point(10, 219);
            this.btnShield.Name = "btnShield";
            this.btnShield.Size = new System.Drawing.Size(203, 38);
            this.btnShield.TabIndex = 5;
            this.btnShield.Text = "Convert Shields";
            this.btnShield.UseVisualStyleBackColor = true;
            this.btnShield.Click += new System.EventHandler(this.btnShield_Click);
            // 
            // btnSensor
            // 
            this.btnSensor.Location = new System.Drawing.Point(10, 262);
            this.btnSensor.Name = "btnSensor";
            this.btnSensor.Size = new System.Drawing.Size(203, 32);
            this.btnSensor.TabIndex = 6;
            this.btnSensor.Text = "Convert Sensors";
            this.btnSensor.UseVisualStyleBackColor = true;
            this.btnSensor.Click += new System.EventHandler(this.btnSensor_Click);
            // 
            // btnAll
            // 
            this.btnAll.Location = new System.Drawing.Point(10, 485);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(203, 34);
            this.btnAll.TabIndex = 7;
            this.btnAll.Text = "Convert All";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnWeapons
            // 
            this.btnWeapons.Location = new System.Drawing.Point(10, 300);
            this.btnWeapons.Name = "btnWeapons";
            this.btnWeapons.Size = new System.Drawing.Size(203, 32);
            this.btnWeapons.TabIndex = 8;
            this.btnWeapons.Text = "Convert Weapons";
            this.btnWeapons.UseVisualStyleBackColor = true;
            this.btnWeapons.Click += new System.EventHandler(this.btnWeapons_Click);
            // 
            // btnScores
            // 
            this.btnScores.Location = new System.Drawing.Point(10, 338);
            this.btnScores.Name = "btnScores";
            this.btnScores.Size = new System.Drawing.Size(203, 32);
            this.btnScores.TabIndex = 9;
            this.btnScores.Text = "Convert Scoring";
            this.btnScores.UseVisualStyleBackColor = true;
            this.btnScores.Click += new System.EventHandler(this.btnScores_Click);
            // 
            // btnPlatforms
            // 
            this.btnPlatforms.Location = new System.Drawing.Point(10, 375);
            this.btnPlatforms.Name = "btnPlatforms";
            this.btnPlatforms.Size = new System.Drawing.Size(202, 29);
            this.btnPlatforms.TabIndex = 10;
            this.btnPlatforms.Text = "Convert Platforms";
            this.btnPlatforms.UseVisualStyleBackColor = true;
            this.btnPlatforms.Click += new System.EventHandler(this.btnPlatforms_Click);
            // 
            // btnEvasion
            // 
            this.btnEvasion.Location = new System.Drawing.Point(10, 410);
            this.btnEvasion.Name = "btnEvasion";
            this.btnEvasion.Size = new System.Drawing.Size(202, 29);
            this.btnEvasion.TabIndex = 11;
            this.btnEvasion.Text = "Evasion";
            this.btnEvasion.UseVisualStyleBackColor = true;
            this.btnEvasion.Click += new System.EventHandler(this.btnEvasion_Click);
            // 
            // ConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(225, 531);
            this.Controls.Add(this.btnEvasion);
            this.Controls.Add(this.btnPlatforms);
            this.Controls.Add(this.btnScores);
            this.Controls.Add(this.btnWeapons);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.btnSensor);
            this.Controls.Add(this.btnShield);
            this.Controls.Add(this.btnEngine);
            this.Controls.Add(this.btnArmor);
            this.Controls.Add(this.btnAdditionalConfig);
            this.Controls.Add(this.btnSelectNewPaths);
            this.Controls.Add(this.btnShipTemplates);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ConverterForm";
            this.Text = "Alchemist Stone";
            this.Load += new System.EventHandler(this.ConverterForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnShipTemplates;
        private System.Windows.Forms.Button btnSelectNewPaths;
        private System.Windows.Forms.Button btnAdditionalConfig;
        private System.Windows.Forms.Button btnArmor;
        private System.Windows.Forms.Button btnEngine;
        private System.Windows.Forms.Button btnShield;
        private System.Windows.Forms.Button btnSensor;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Button btnWeapons;
        private System.Windows.Forms.Button btnScores;
        private System.Windows.Forms.Button btnPlatforms;
        private System.Windows.Forms.Button btnEvasion;
    }
}

