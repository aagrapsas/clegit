﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlchemistStone
{
    class TemplateMapper
    {
        public Manifest Manifest { get; set; }
        public Config AppConfig { get; set; }

        public TemplateMapper(Manifest manifest, Config config)
        {
            Manifest = manifest;
            AppConfig = config;
        }

        public void Write(String template, StupidSimpleCSV csv)
        {
            Manifest.TemplateWriters[template].Write(AppConfig.ExportFolder + "\\" + template, csv);
        }
    }
}
