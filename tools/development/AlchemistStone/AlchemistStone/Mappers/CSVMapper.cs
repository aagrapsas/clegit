﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlchemistStone
{
    class CSVMapper
    {
        private Config AppConfig;

        public CSVMapper(Config config)
        {
            AppConfig = config;
        }

        public StupidSimpleCSV GetCSV(String template)
        {
            String sanitizedTemplate = template;
            int backSlash = template.LastIndexOf("\\");

            if (backSlash != -1)
            {
                sanitizedTemplate = template.Substring(backSlash + 1);
            }

            StupidSimpleCSV csv = new StupidSimpleCSV();
            csv.Load(AppConfig.ImportFolder + "\\" + sanitizedTemplate + ".csv");

            return csv;
        }
    }
}
