﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlchemistStone
{
    public class ManifestEntry
    {
        public String Name { get; set; }
        public String OuterName { get; set; }
        public String PropertyName { get; set; }
        public String ImportFrom { get; set; }
        public String ExportTo { get; set; }
        public String Type { get; set; }

        public ManifestEntry() { }
    }
}
