﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml.Serialization;

namespace AlchemistStone
{
    class Manifest
    {
        private static String Path = System.Environment.CurrentDirectory + "\\manifest.xml";

        [XmlIgnore]
        public Dictionary<String, ITemplateWriter> TemplateWriters { get; set; }

        public List<ManifestEntry> Entries { get; set; }

        public static String SHIP_TEMPLATES = "ship_templates";
        public static String ADDITIONAL_CONFIG = "additional_config";
        public static String ARMOR_COMPONENT = "ship_components\\armor";
        public static String ENGINE_COMPONENT = "ship_components\\engines";
        public static String SENSOR_COMPONENT = "ship_components\\sensors";
        public static String SHIELD_COMPONENT = "ship_components\\shields";
        public static String EVASION_COMPONENT = "ship_components\\evasion";
        public static String WEAPONS_COMPONENT = "weapons\\flat_weapons";
        public static String SCORING_COMPONENT = "ship_scores";
        public static String PLATFORMS = "platforms";

        public Manifest()
        {
            Initialize();
        }

        public static Manifest Load()
        {
            Manifest manifest = new Manifest();
            manifest.TemplateWriters = new Dictionary<string, ITemplateWriter>();

            if (File.Exists(Path))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Manifest));

                    TextReader reader = new StreamReader(Path);

                    manifest = (Manifest)serializer.Deserialize(reader);

                    reader.Close();

                    // Populate writers
                    foreach (ManifestEntry entry in manifest.Entries)
                    {
                        ITemplateWriter writer = null;

                        if (entry.Type == "xml")
                        {
                            writer = new GenericXMLWriter(entry.OuterName, entry.PropertyName);
                        }
                        else if (entry.Type == "json")
                        {
                            writer = new GenericJSONWriter();
                        }

                        if (writer != null)
                        {
                            manifest.TemplateWriters.Add(entry.Name, writer);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return manifest;
        }

        private void Initialize()
        {
            // Allocation
            TemplateWriters = new Dictionary<string, ITemplateWriter>();

            // Initialize
            TemplateWriters.Add(SHIP_TEMPLATES, new GenericXMLWriter("templates", "template"));
            TemplateWriters.Add(ADDITIONAL_CONFIG, new GenericXMLWriter("config", "info"));
            TemplateWriters.Add(ARMOR_COMPONENT, new GenericXMLWriter("armor_components", "armor"));
            TemplateWriters.Add(ENGINE_COMPONENT, new GenericXMLWriter("engine_components", "engine"));
            TemplateWriters.Add(SENSOR_COMPONENT, new GenericXMLWriter("sensor_components", "sensor"));
            TemplateWriters.Add(SHIELD_COMPONENT, new GenericXMLWriter("shield_component", "shields"));
            TemplateWriters.Add(WEAPONS_COMPONENT, new GenericXMLWriter("weapons", "weapon"));
            TemplateWriters.Add(SCORING_COMPONENT, new GenericXMLWriter("scores", "score"));
            TemplateWriters.Add(PLATFORMS, new GenericXMLWriter("platforms", "platform"));
            TemplateWriters.Add(EVASION_COMPONENT, new GenericXMLWriter("evasion_components", "evasion"));
        }
    }
}
