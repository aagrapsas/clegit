﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace AlchemistStone
{
    public partial class ConverterForm : Form
    {
        private Config AppConfig;
        private TemplateMapper Mapper;
        private CSVMapper CSV;

        public ConverterForm()
        {
            InitializeComponent();
        }

        private Boolean SelectPaths()
        {
            FolderBrowserDialog browser = new FolderBrowserDialog();

            browser.ShowNewFolderButton = true;
            browser.Description = "Select folder where your CSV files live";

            DialogResult result = browser.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return false;     
            }

            AppConfig.ImportFolder = browser.SelectedPath;

            browser.Description = "Select folder where your game files live";

            result = browser.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return false;
            }

            AppConfig.ExportFolder = browser.SelectedPath;

            AppConfig.Save();

            return true;
        }

        private void ConverterForm_Load(object sender, EventArgs e)
        {
            AppConfig = Config.Load();

            if (AppConfig.DoesExist == false)
            {
                SelectPaths();
            }

            Mapper = new TemplateMapper(new Manifest(), AppConfig);
            CSV = new CSVMapper(AppConfig);
        }

        private void btnSelectNewPaths_Click(object sender, EventArgs e)
        {
            SelectPaths();
        }

        private void export(String name)
        {
            String sanitizedName = name;

            int backslash = name.LastIndexOf("\\");

            if (backslash != -1)
            {
                sanitizedName = name.Substring(backslash + 1);
            }

            Mapper.Write(name, CSV.GetCSV(name));

            MessageBox.Show(sanitizedName + " exported!");
        }

        private void btnShipTemplates_Click(object sender, EventArgs e)
        {
            export(Manifest.SHIP_TEMPLATES);
        }

        private void btnAdditionalConfig_Click(object sender, EventArgs e)
        {
            export(Manifest.ADDITIONAL_CONFIG);
        }

        private void btnArmor_Click(object sender, EventArgs e)
        {
            export(Manifest.ARMOR_COMPONENT);
        }

        private void btnEngine_Click(object sender, EventArgs e)
        {
            export(Manifest.ENGINE_COMPONENT);
        }

        private void btnShield_Click(object sender, EventArgs e)
        {
            export(Manifest.SHIELD_COMPONENT);
        }

        private void btnSensor_Click(object sender, EventArgs e)
        {
            export(Manifest.SENSOR_COMPONENT);
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            List<String> toExport = new List<String>();
            toExport.Add(Manifest.SHIP_TEMPLATES);
            toExport.Add(Manifest.ADDITIONAL_CONFIG);
            toExport.Add(Manifest.ARMOR_COMPONENT);
            toExport.Add(Manifest.ENGINE_COMPONENT);
            toExport.Add(Manifest.SHIELD_COMPONENT);
            toExport.Add(Manifest.SENSOR_COMPONENT);
            toExport.Add(Manifest.WEAPONS_COMPONENT);
            toExport.Add(Manifest.SCORING_COMPONENT);
            toExport.Add(Manifest.PLATFORMS);
            toExport.Add(Manifest.EVASION_COMPONENT);

            foreach (String exportName in toExport)
            {
                export(exportName);
            }

            MessageBox.Show("Exported all configs!");
        }

        private void btnWeapons_Click(object sender, EventArgs e)
        {
            export(Manifest.WEAPONS_COMPONENT);
        }

        private void btnScores_Click(object sender, EventArgs e)
        {
            export(Manifest.SCORING_COMPONENT);
        }

        private void btnPlatforms_Click(object sender, EventArgs e)
        {
            export(Manifest.PLATFORMS);
        }

        private void btnEvasion_Click(object sender, EventArgs e)
        {
            export(Manifest.EVASION_COMPONENT);
        }
    }
}
