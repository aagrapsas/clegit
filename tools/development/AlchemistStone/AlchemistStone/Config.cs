﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml.Serialization;

namespace AlchemistStone
{
    public class Config
    {
        private static String Path = System.Environment.CurrentDirectory + "\\config.xml";

        public String ImportFolder { get; set; }
        public String ExportFolder { get; set; }

        [XmlIgnore]
        public Boolean DoesExist { get; private set; }

        public Config()
        {

        }

        public static Config Load()
        {
            Config config = new Config();

            if (File.Exists(Path))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Config));

                    TextReader reader = new StreamReader(Path);

                    config = (Config)serializer.Deserialize(reader);

                    reader.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                config.DoesExist = true;
            }
            else
            {
                config.DoesExist = false;
            }

            return config;
        }

        public void Save()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Config));

                TextWriter writer = new StreamWriter(Path);

                serializer.Serialize(writer, this);

                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
