﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace AlchemistStone
{
    class GenericXMLWriter : ITemplateWriter
    {
        private String PropertyName;
        private String OuterName;

        public GenericXMLWriter(String outerName, String propertyName)
        {
            PropertyName = propertyName;
            OuterName = outerName;
        }

        public void Write(String path, StupidSimpleCSV csv)
        {
            path += ".xml";

            StreamWriter writer = new StreamWriter(path);

            writer.WriteLine("<" + OuterName + ">");

            foreach (CSVEntry entry in csv.Entries)
            {
                writer.Write("\t<" + PropertyName);

                foreach (String heading in csv.Heading.Mapping.Keys)
                {
                    String fieldValue = entry.GetField(heading);

                    if (fieldValue != null && fieldValue != "")
                        writer.Write(" " + heading + "=\"" + fieldValue + "\"");
                }

                writer.Write("/>\n");
            }

            writer.WriteLine("</" + OuterName + ">");

            writer.Close();
        }
    }
}
