﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace AlchemistStone
{
    class GenericJSONWriter : ITemplateWriter
    {
        public void Write(String path, StupidSimpleCSV csv)
        {
            path += ".json";

            StreamWriter writer = new StreamWriter(path);

            writer.Write("{ \"data\" : [");

            bool isFirstEntry = true;

            foreach (CSVEntry entry in csv.Entries)
            {
                bool isFirst = true;

                if (isFirstEntry)
                {
                    isFirstEntry = false;
                    writer.Write("{ ");
                }
                else
                {
                    writer.Write(", { ");
                }
                
                foreach (String heading in csv.Heading.Mapping.Keys)
                {
                    if (isFirst)
                    {
                        isFirst = false;
                    }
                    else
                    {
                        writer.Write(" ,");
                    }

                    String fieldvalue = entry.GetField(heading);

                    if (fieldvalue != null && fieldvalue != "")
                        writer.Write(" \"" + heading + "\": \"" + fieldvalue + "\"");
                }
                writer.Write(" }");
            }

            writer.Write("] }");
        }
    }
}
