package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 5/14/2012 11:23:43 PM
	// -------------------------------------------

	import flash.display.Sprite;

	public class ships extends Sprite
	{
		// omen_img
		// Image
		[Embed ( source="converted/ships/omen_img.png" )]
		public var omen_img:Class;

		// gunship_img
		// Image
		[Embed ( source="converted/ships/gunship_img.png" )]
		public var gunship_img:Class;

		// bomber_img
		// Image
		[Embed ( source="converted/ships/bomber_img.png" )]
		public var bomber_img:Class;

		// corvette_img
		// Image
		[Embed ( source="converted/ships/corvette_img.png" )]
		public var corvette_img:Class;

		// frigate_img
		// Image
		[Embed ( source="converted/ships/frigate_img.png" )]
		public var frigate_img:Class;

		// destroyer_img
		// Image
		[Embed ( source="converted/ships/destroyer_img.png" )]
		public var destroyer_img:Class;

		// cruiser_img
		// Image
		[Embed ( source="converted/ships/cruiser_img.png" )]
		public var cruiser_img:Class;

		// battleship_img
		// Image
		[Embed ( source="converted/ships/battleship_img.png" )]
		public var battleship_img:Class;

		// carrier_img
		// Image
		[Embed ( source="converted/ships/carrier_img.png" )]
		public var carrier_img:Class;

		// ascendant_img
		// Image
		[Embed ( source="converted/ships/ascendant_img.png" )]
		public var ascendant_img:Class;

		// striker_img
		// Image
		[Embed ( source="converted/ships/striker_img.png" )]
		public var striker_img:Class;

		// harbinger_img
		// Image
		[Embed ( source="converted/ships/harbinger_img.png" )]
		public var harbinger_img:Class;

		// cleric_img
		// Image
		[Embed ( source="converted/ships/cleric_img.png" )]
		public var cleric_img:Class;

		// cavalier_img
		// Image
		[Embed ( source="converted/ships/cavalier_img.png" )]
		public var cavalier_img:Class;

		// dominion_img
		// Image
		[Embed ( source="converted/ships/dominion_img.png" )]
		public var dominion_img:Class;

		// regent_img
		// Image
		[Embed ( source="converted/ships/regent_img.png" )]
		public var regent_img:Class;

		// fighter_img
		// Image
		[Embed ( source="converted/ships/fighter_img.png" )]
		public var fighter_img:Class;

		// platform_img
		// Image
		[Embed ( source="converted/ships/platform_img.png" )]
		public var platform_img:Class;

	}
}
// EOF
