package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 6/2/2012 4:21:19 PM
	// -------------------------------------------

	import flash.display.Sprite;

	public class icons extends Sprite
	{
		// salvage_icon
		// Image
		[Embed ( source="converted/icons/salvage_icon.png" )]
		public var salvage_icon:Class;

		// fbc_icon
		// Image
		[Embed ( source="converted/icons/fbc_icon.png" )]
		public var fbc_icon:Class;

		// upgrade_icon
		// Image
		[Embed ( source="converted/icons/upgrade_icon.png" )]
		public var upgrade_icon:Class;

		// sell_icon
		// Image
		[Embed ( source="converted/icons/sell_icon.png" )]
		public var sell_icon:Class;

		// return_icon
		// Image
		[Embed ( source="converted/icons/return_icon.png" )]
		public var return_icon:Class;

		// abilities_backplate
		// Image
		[Embed ( source="converted/icons/abilities_backplate.png" )]
		public var abilities_backplate:Class;

		// abilities_gray
		// Image
		[Embed ( source="converted/icons/abilities_gray.png" )]
		public var abilities_gray:Class;

		// abilities_blue
		// Image
		[Embed ( source="converted/icons/abilities_blue.png" )]
		public var abilities_blue:Class;

		// abilities_ready
		// Image
		[Embed ( source="converted/icons/abilities_ready.png" )]
		public var abilities_ready:Class;

		// abilities_boostforward
		// Image
		[Embed ( source="converted/icons/abilities_boostforward.png" )]
		public var abilities_boostforward:Class;

		// abilities_evade_right
		// Image
		[Embed ( source="converted/icons/abilities_evade_right.png" )]
		public var abilities_evade_right:Class;

		// abilities_evade_left
		// Image
		[Embed ( source="converted/icons/abilities_evade_left.png" )]
		public var abilities_evade_left:Class;

		// abilities_slowdown
		// Image
		[Embed ( source="converted/icons/abilities_slowdown.png" )]
		public var abilities_slowdown:Class;

		// abilities_FMJ20mm
		// Image
		[Embed ( source="converted/icons/abilities_FMJ20mm.png" )]
		public var abilities_FMJ20mm:Class;

		// abilities_FMJ20mm_turret
		// Image
		[Embed ( source="converted/icons/abilities_FMJ20mm_turret.png" )]
		public var abilities_FMJ20mm_turret:Class;

		// abilities_missile
		// Image
		[Embed ( source="converted/icons/abilities_missile.png" )]
		public var abilities_missile:Class;

		// abilities_plasmacannon_turret
		// Image
		[Embed ( source="converted/icons/abilities_plasmacannon_turret.png" )]
		public var abilities_plasmacannon_turret:Class;

		// abilities_plasma_pulse_cannon
		// Image
		[Embed ( source="converted/icons/abilities_plasma_pulse_cannon.png" )]
		public var abilities_plasma_pulse_cannon:Class;

		// abilities_platform
		// Image
		[Embed ( source="converted/icons/abilities_platform.png" )]
		public var abilities_platform:Class;

		// abilities_reinforcements
		// Image
		[Embed ( source="converted/icons/abilities_reinforcements.png" )]
		public var abilities_reinforcements:Class;

	}
}
// EOF
