package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 5/14/2012 11:23:49 PM
	// -------------------------------------------

	import flash.display.Sprite;

	public class weapons extends Sprite
	{
		// wpn_artillery
		// Image
		[Embed ( source="converted/weapons/wpn_artillery.png" )]
		public var wpn_artillery:Class;

		// massdriver_img
		// Image
		[Embed ( source="converted/weapons/massdriver_img.png" )]
		public var massdriver_img:Class;

		// wpn_fighterbay
		// Image
		[Embed ( source="converted/weapons/wpn_fighterbay.png" )]
		public var wpn_fighterbay:Class;

		// wpn_20mmfmj_fix
		// Image
		[Embed ( source="converted/weapons/wpn_20mmfmj_fix.png" )]
		public var wpn_20mmfmj_fix:Class;

		// wpn_20mmfmj_tur
		// Image
		[Embed ( source="converted/weapons/wpn_20mmfmj_tur.png" )]
		public var wpn_20mmfmj_tur:Class;

		// wpn_40mm_fix
		// Image
		[Embed ( source="converted/weapons/wpn_40mm_fix.png" )]
		public var wpn_40mm_fix:Class;

		// wpn_40mm_tur
		// Image
		[Embed ( source="converted/weapons/wpn_40mm_tur.png" )]
		public var wpn_40mm_tur:Class;

		// wpn_80mm_fix
		// Image
		[Embed ( source="converted/weapons/wpn_80mm_fix.png" )]
		public var wpn_80mm_fix:Class;

		// wpn_massdriver
		// Image
		[Embed ( source="converted/weapons/wpn_massdriver.png" )]
		public var wpn_massdriver:Class;

		// wpn_plasart
		// Image
		[Embed ( source="converted/weapons/wpn_plasart.png" )]
		public var wpn_plasart:Class;

		// wpn_plascan_fix
		// Image
		[Embed ( source="converted/weapons/wpn_plascan_fix.png" )]
		public var wpn_plascan_fix:Class;

		// wpn_plascan_tur
		// Image
		[Embed ( source="converted/weapons/wpn_plascan_tur.png" )]
		public var wpn_plascan_tur:Class;

		// wpn_pulscan_fix
		// Image
		[Embed ( source="converted/weapons/wpn_pulscan_fix.png" )]
		public var wpn_pulscan_fix:Class;

		// wpn_pulscan_tur
		// Image
		[Embed ( source="converted/weapons/wpn_pulscan_tur.png" )]
		public var wpn_pulscan_tur:Class;

	}
}
// EOF
