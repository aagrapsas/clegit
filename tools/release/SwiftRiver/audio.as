package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 6/6/2012 12:23:49 AM
	// -------------------------------------------

	import flash.display.Sprite;
	import mx.core.SoundAsset;

	public class audio extends Sprite
	{
		// HyperspaceJumpSound
		// Audio
		[Embed ( source="../../../export/audio/HyperspaceJumpSound.mp3" )]
		public var HyperspaceJumpSound:Class;

		// MediumShipEnginesAcceleration
		// Audio
		[Embed ( source="../../../export/audio/MediumShipEnginesAcceleration.mp3" )]
		public var MediumShipEnginesAcceleration:Class;

		// MediumShipEnginesRelease
		// Audio
		[Embed ( source="../../../export/audio/MediumShipEnginesRelease.mp3" )]
		public var MediumShipEnginesRelease:Class;

		// MediumShipEnginesLoop
		// Audio
		[Embed ( source="../../../export/audio/MediumShipEnginesLoop.mp3" )]
		public var MediumShipEnginesLoop:Class;

		// ShieldDestruction
		// Audio
		[Embed ( source="../../../export/audio/ShieldDestruction.mp3" )]
		public var ShieldDestruction:Class;

		// ShieldImpact
		// Audio
		[Embed ( source="../../../export/audio/ShieldImpact.mp3" )]
		public var ShieldImpact:Class;

		// ShieldRecharged
		// Audio
		[Embed ( source="../../../export/audio/ShieldRecharged.mp3" )]
		public var ShieldRecharged:Class;

		// RadioChatterSingleSmallShip
		// Audio
		[Embed ( source="../../../export/audio/RadioChatterSingleSmallShip.mp3" )]
		public var RadioChatterSingleSmallShip:Class;

		// SmallShipDestroyed
		// Audio
		[Embed ( source="../../../export/audio/SmallShipDestroyed.mp3" )]
		public var SmallShipDestroyed:Class;

		// RadioChatterSingleMediumShip
		// Audio
		[Embed ( source="../../../export/audio/RadioChatterSingleMediumShip.mp3" )]
		public var RadioChatterSingleMediumShip:Class;

		// MediumShipDestroyed
		// Audio
		[Embed ( source="../../../export/audio/MediumShipDestroyed.mp3" )]
		public var MediumShipDestroyed:Class;

		// RadioChatterSingleLargeShip
		// Audio
		[Embed ( source="../../../export/audio/RadioChatterSingleLargeShip.mp3" )]
		public var RadioChatterSingleLargeShip:Class;

		// LargeShipDestroyed
		// Audio
		[Embed ( source="../../../export/audio/LargeShipDestroyed.mp3" )]
		public var LargeShipDestroyed:Class;

		// SmallWeaponFire
		// Audio
		[Embed ( source="../../../export/audio/SmallWeaponFire.mp3" )]
		public var SmallWeaponFire:Class;

		// ReefcasterKineticGuns
		// Audio
		[Embed ( source="../../../export/audio/ReefcasterKineticGuns.mp3" )]
		public var ReefcasterKineticGuns:Class;

		// MissileImpact
		// Audio
		[Embed ( source="../../../export/audio/MissileImpact.mp3" )]
		public var MissileImpact:Class;

		// FighterEnginesAcceleration
		// Audio
		[Embed ( source="../../../export/audio/FighterEnginesAcceleration.mp3" )]
		public var FighterEnginesAcceleration:Class;

		// FighterEnginesLoop
		// Audio
		[Embed ( source="../../../export/audio/FighterEnginesLoop.mp3" )]
		public var FighterEnginesLoop:Class;

		// FighterEnginesRelease
		// Audio
		[Embed ( source="../../../export/audio/FighterEnginesRelease.mp3" )]
		public var FighterEnginesRelease:Class;

		// MissileFired
		// Audio
		[Embed ( source="../../../export/audio/MissileFired.mp3" )]
		public var MissileFired:Class;

		// Mission_Onslaught_Wave_Start
		// Audio
		[Embed ( source="../../../export/audio/Mission_Onslaught_Wave_Start.mp3" )]
		public var Mission_Onslaught_Wave_Start:Class;

		// Mission_Onslaught_Wave_End
		// Audio
		[Embed ( source="../../../export/audio/Mission_Onslaught_Wave_End.mp3" )]
		public var Mission_Onslaught_Wave_End:Class;

		// Mission_Onslaught_Mode_Complete
		// Audio
		[Embed ( source="../../../export/audio/Mission_Onslaught_Mode_Complete.mp3" )]
		public var Mission_Onslaught_Mode_Complete:Class;

		// UI_Generic_Select
		// Audio
		[Embed ( source="../../../export/audio/UI_Generic_Select.mp3" )]
		public var UI_Generic_Select:Class;

		// UI_Generic_Cancel
		// Audio
		[Embed ( source="../../../export/audio/UI_Generic_Cancel.mp3" )]
		public var UI_Generic_Cancel:Class;

		// MediumShipImpact
		// Audio
		[Embed ( source="../../../export/audio/MediumShipImpact.mp3" )]
		public var MediumShipImpact:Class;

		// SmallShipImpact
		// Audio
		[Embed ( source="../../../export/audio/SmallShipImpact.mp3" )]
		public var SmallShipImpact:Class;

		// EvasionBoost
		// Audio
		[Embed ( source="../../../export/audio/EvasionBoost.mp3" )]
		public var EvasionBoost:Class;

		// LargeShipImpact
		// Audio
		[Embed ( source="../../../export/audio/LargeShipImpact.mp3" )]
		public var LargeShipImpact:Class;

		// FirstPlayableGameplayLoop02
		// Audio
		[Embed ( source="../../../export/audio/FirstPlayableGameplayLoop02.mp3" )]
		public var FirstPlayableGameplayLoop02:Class;

		// FirstPlayableMenuLoop02
		// Audio
		[Embed ( source="../../../export/audio/FirstPlayableMenuLoop02.mp3" )]
		public var FirstPlayableMenuLoop02:Class;

		// SmallWeaponFire3
		// Audio
		[Embed ( source="../../../export/audio/SmallWeaponFire3.mp3" )]
		public var SmallWeaponFire3:Class;

		// MassDrivers
		// Audio
		[Embed ( source="../../../export/audio/MassDrivers.mp3" )]
		public var MassDrivers:Class;

		// SmallWeaponFire2
		// Audio
		[Embed ( source="../../../export/audio/SmallWeaponFire2.mp3" )]
		public var SmallWeaponFire2:Class;

		// PlasmaWave
		// Audio
		[Embed ( source="../../../export/audio/PlasmaWave.mp3" )]
		public var PlasmaWave:Class;

	}
}
// EOF
