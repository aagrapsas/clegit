package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 6/3/2012 12:11:50 PM
	// -------------------------------------------

	import flash.display.Sprite;

	public class environment_and_fx extends Sprite
	{
		// spacebg
		// Image
		[Embed ( source="../../../export/images/spacebg.jpg" )]
		public var spacebg:Class;

		// smoke
		// Image
		[Embed ( source="converted/environment_and_fx/smoke.png" )]
		public var smoke:Class;

		// tracer
		// Image
		[Embed ( source="converted/environment_and_fx/tracer.png" )]
		public var tracer:Class;

		// proj_20mm
		// Image
		[Embed ( source="converted/environment_and_fx/proj_20mm.png" )]
		public var proj_20mm:Class;

		// proj_40mm
		// Image
		[Embed ( source="converted/environment_and_fx/proj_40mm.png" )]
		public var proj_40mm:Class;

		// proj_argo_artillery
		// Image
		[Embed ( source="converted/environment_and_fx/proj_argo_artillery.png" )]
		public var proj_argo_artillery:Class;

		// proj_human_artillery
		// Image
		[Embed ( source="converted/environment_and_fx/proj_human_artillery.png" )]
		public var proj_human_artillery:Class;

		// proj_massdriver
		// Image
		[Embed ( source="converted/environment_and_fx/proj_massdriver.png" )]
		public var proj_massdriver:Class;

		// proj_plasma_cannon
		// Image
		[Embed ( source="converted/environment_and_fx/proj_plasma_cannon.png" )]
		public var proj_plasma_cannon:Class;

		// proj_pulse_cannon
		// Image
		[Embed ( source="converted/environment_and_fx/proj_pulse_cannon.png" )]
		public var proj_pulse_cannon:Class;

		// waypoint
		// Image
		[Embed ( source="converted/environment_and_fx/waypoint.png" )]
		public var waypoint:Class;

		// dir_indicator
		// Image
		[Embed ( source="converted/environment_and_fx/dir_indicator.png" )]
		public var dir_indicator:Class;

		// starfieldbg
		// Image
		[Embed ( source="../../../export/images/starfieldbg.jpg" )]
		public var starfieldbg:Class;

		// target_rectangle
		// Image
		[Embed ( source="converted/environment_and_fx/target_rectangle.png" )]
		public var target_rectangle:Class;

		// offscreen_indicator
		// Image
		[Embed ( source="converted/environment_and_fx/offscreen_indicator.png" )]
		public var offscreen_indicator:Class;

		// argo_type1
		// Image
		[Embed ( source="converted/environment_and_fx/argo_type1.png" )]
		public var argo_type1:Class;

		// human_type1
		// Image
		[Embed ( source="converted/environment_and_fx/human_type1.png" )]
		public var human_type1:Class;

		// large_exhaust
		// Image
		[Embed ( source="converted/environment_and_fx/large_exhaust.png" )]
		public var large_exhaust:Class;

		// small_exhaust
		// Image
		[Embed ( source="converted/environment_and_fx/small_exhaust.png" )]
		public var small_exhaust:Class;

		// rock1
		// Image
		[Embed ( source="converted/environment_and_fx/rock1.png" )]
		public var rock1:Class;

		// rock2
		// Image
		[Embed ( source="converted/environment_and_fx/rock2.png" )]
		public var rock2:Class;

		// rock3
		// Image
		[Embed ( source="converted/environment_and_fx/rock3.png" )]
		public var rock3:Class;

		// rock4
		// Image
		[Embed ( source="converted/environment_and_fx/rock4.png" )]
		public var rock4:Class;

		// rock5
		// Image
		[Embed ( source="converted/environment_and_fx/rock5.png" )]
		public var rock5:Class;

		// asteroids
		// Image
		[Embed ( source="converted/environment_and_fx/asteroids.png" )]
		public var asteroids:Class;

		// asteroids2
		// Image
		[Embed ( source="converted/environment_and_fx/asteroids2.png" )]
		public var asteroids2:Class;

		// offscreen_indicator_friend
		// Image
		[Embed ( source="converted/environment_and_fx/offscreen_indicator_friend.png" )]
		public var offscreen_indicator_friend:Class;

	}
}
// EOF
