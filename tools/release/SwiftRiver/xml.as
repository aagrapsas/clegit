package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 9/20/2012 9:10:15 AM
	// -------------------------------------------

	import flash.display.Sprite;

	public class xml extends Sprite
	{
		// validators
		// XML
		[Embed ( source="../../../home/bin/data/manual/xml_validators.xml" )]
		public var validators:Class;

		// swf_manifest
		// XML
		[Embed ( source="../../../home/bin/data/manual/swf_manifest.xml" )]
		public var swf_manifest:Class;

		// sound_sets
		// XML
		[Embed ( source="../../../home/bin/data/manual/sound_sets.xml" )]
		public var sound_sets:Class;

		// ship_templates
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_templates.xml" )]
		public var ship_templates:Class;

		// ship_scores
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_scores.xml" )]
		public var ship_scores:Class;

		// ship_destruction_fx
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_destruction_fx.xml" )]
		public var ship_destruction_fx:Class;

		// platform_config
		// XML
		[Embed ( source="../../../home/bin/data/manual/platforms.xml" )]
		public var platform_config:Class;

		// game_config
		// XML
		[Embed ( source="../../../home/bin/data/manual/game_config.xml" )]
		public var game_config:Class;

		// fx_pools
		// XML
		[Embed ( source="../../../home/bin/data/manual/fx_pools.xml" )]
		public var fx_pools:Class;

		// audio_config
		// XML
		[Embed ( source="../../../home/bin/data/manual/audio_config.xml" )]
		public var audio_config:Class;

		// animations
		// XML
		[Embed ( source="../../../home/bin/data/manual/animations.xml" )]
		public var animations:Class;

		// additional_config
		// XML
		[Embed ( source="../../../home/bin/data/manual/additional_config.xml" )]
		public var additional_config:Class;

		// flat_weapons
		// XML
		[Embed ( source="../../../home/bin/data/manual/weapons/flat_weapons.xml" )]
		public var flat_weapons:Class;

		// special_weapons
		// XML
		[Embed ( source="../../../home/bin/data/manual/weapons/special_weapons.xml" )]
		public var special_weapons:Class;

		// armor_components
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_components/armor.xml" )]
		public var armor_components:Class;

		// chassis_components
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_components/chassis.xml" )]
		public var chassis_components:Class;

		// engine_components
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_components/engines.xml" )]
		public var engine_components:Class;

		// evasion_components
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_components/evasion.xml" )]
		public var evasion_components:Class;

		// sensor_components
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_components/sensors.xml" )]
		public var sensor_components:Class;

		// shield_components
		// XML
		[Embed ( source="../../../home/bin/data/manual/ship_components/shields.xml" )]
		public var shield_components:Class;

	}
}
// EOF
