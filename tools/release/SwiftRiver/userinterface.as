package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 6/3/2012 10:12:53 PM
	// -------------------------------------------

	import flash.display.Sprite;

	public class userinterface extends Sprite
	{
		// UIGenericPopupImg
		// Image
		[Embed ( source="converted/userinterface/UIGenericPopupImg.png" )]
		public var UIGenericPopupImg:Class;

		// UIGenericButtonImg
		// Image
		[Embed ( source="converted/userinterface/UIGenericButtonImg.png" )]
		public var UIGenericButtonImg:Class;

		// UIGenericPopupSmallImg
		// Image
		[Embed ( source="converted/userinterface/UIGenericPopupSmallImg.png" )]
		public var UIGenericPopupSmallImg:Class;

	}
}
// EOF
