package
{
	// Automatically built AS3 file
	// Compiled using Swift River
	// Copyright 2012 Andrew A. Grapsas
	// Last updated: 5/15/2012 6:52:41 PM
	// -------------------------------------------

	import flash.display.Sprite;

	public class animations_pack extends Sprite
	{
		// gun_firing
		// Animation
		[Embed ( source="converted/animations_pack/gun_firing0.png" )]
		public var gun_firing0:Class;
		[Embed ( source="converted/animations_pack/gun_firing1.png" )]
		public var gun_firing1:Class;
		[Embed ( source="converted/animations_pack/gun_firing2.png" )]
		public var gun_firing2:Class;
		[Embed ( source="converted/animations_pack/gun_firing3.png" )]
		public var gun_firing3:Class;
		[Embed ( source="converted/animations_pack/gun_firing4.png" )]
		public var gun_firing4:Class;
		[Embed ( source="converted/animations_pack/gun_firing5.png" )]
		public var gun_firing5:Class;

		// anim_80mm_firing
		// Animation
		[Embed ( source="converted/animations_pack/anim_80mm_firing0.png" )]
		public var anim_80mm_firing0:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing1.png" )]
		public var anim_80mm_firing1:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing2.png" )]
		public var anim_80mm_firing2:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing3.png" )]
		public var anim_80mm_firing3:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing4.png" )]
		public var anim_80mm_firing4:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing5.png" )]
		public var anim_80mm_firing5:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing6.png" )]
		public var anim_80mm_firing6:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing7.png" )]
		public var anim_80mm_firing7:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing8.png" )]
		public var anim_80mm_firing8:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing9.png" )]
		public var anim_80mm_firing9:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing10.png" )]
		public var anim_80mm_firing10:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing11.png" )]
		public var anim_80mm_firing11:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing12.png" )]
		public var anim_80mm_firing12:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing13.png" )]
		public var anim_80mm_firing13:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing14.png" )]
		public var anim_80mm_firing14:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing15.png" )]
		public var anim_80mm_firing15:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing16.png" )]
		public var anim_80mm_firing16:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing17.png" )]
		public var anim_80mm_firing17:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing18.png" )]
		public var anim_80mm_firing18:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing19.png" )]
		public var anim_80mm_firing19:Class;
		[Embed ( source="converted/animations_pack/anim_80mm_firing20.png" )]
		public var anim_80mm_firing20:Class;

		// anim_20mm_firing
		// Animation
		[Embed ( source="converted/animations_pack/anim_20mm_firing0.png" )]
		public var anim_20mm_firing0:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_firing1.png" )]
		public var anim_20mm_firing1:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_firing2.png" )]
		public var anim_20mm_firing2:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_firing3.png" )]
		public var anim_20mm_firing3:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_firing4.png" )]
		public var anim_20mm_firing4:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_firing5.png" )]
		public var anim_20mm_firing5:Class;

		// anim_20mm_fmj_firing
		// Animation
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing0.png" )]
		public var anim_20mm_fmj_firing0:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing1.png" )]
		public var anim_20mm_fmj_firing1:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing2.png" )]
		public var anim_20mm_fmj_firing2:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing3.png" )]
		public var anim_20mm_fmj_firing3:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing4.png" )]
		public var anim_20mm_fmj_firing4:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing5.png" )]
		public var anim_20mm_fmj_firing5:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing6.png" )]
		public var anim_20mm_fmj_firing6:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing7.png" )]
		public var anim_20mm_fmj_firing7:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing8.png" )]
		public var anim_20mm_fmj_firing8:Class;
		[Embed ( source="converted/animations_pack/anim_20mm_fmj_firing9.png" )]
		public var anim_20mm_fmj_firing9:Class;

		// anim_40mm_duel_turret_firing
		// Animation
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing0.png" )]
		public var anim_40mm_duel_turret_firing0:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing1.png" )]
		public var anim_40mm_duel_turret_firing1:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing2.png" )]
		public var anim_40mm_duel_turret_firing2:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing3.png" )]
		public var anim_40mm_duel_turret_firing3:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing4.png" )]
		public var anim_40mm_duel_turret_firing4:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing5.png" )]
		public var anim_40mm_duel_turret_firing5:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing6.png" )]
		public var anim_40mm_duel_turret_firing6:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing7.png" )]
		public var anim_40mm_duel_turret_firing7:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing8.png" )]
		public var anim_40mm_duel_turret_firing8:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing9.png" )]
		public var anim_40mm_duel_turret_firing9:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing10.png" )]
		public var anim_40mm_duel_turret_firing10:Class;
		[Embed ( source="converted/animations_pack/anim_40mm_duel_turret_firing11.png" )]
		public var anim_40mm_duel_turret_firing11:Class;

		// fighter_bay_launching
		// Animation
		[Embed ( source="converted/animations_pack/fighter_bay_launching0.png" )]
		public var fighter_bay_launching0:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching1.png" )]
		public var fighter_bay_launching1:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching2.png" )]
		public var fighter_bay_launching2:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching3.png" )]
		public var fighter_bay_launching3:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching4.png" )]
		public var fighter_bay_launching4:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching5.png" )]
		public var fighter_bay_launching5:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching6.png" )]
		public var fighter_bay_launching6:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching7.png" )]
		public var fighter_bay_launching7:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching8.png" )]
		public var fighter_bay_launching8:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching9.png" )]
		public var fighter_bay_launching9:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching10.png" )]
		public var fighter_bay_launching10:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching11.png" )]
		public var fighter_bay_launching11:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching12.png" )]
		public var fighter_bay_launching12:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching13.png" )]
		public var fighter_bay_launching13:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching14.png" )]
		public var fighter_bay_launching14:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching15.png" )]
		public var fighter_bay_launching15:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching16.png" )]
		public var fighter_bay_launching16:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching17.png" )]
		public var fighter_bay_launching17:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching18.png" )]
		public var fighter_bay_launching18:Class;
		[Embed ( source="converted/animations_pack/fighter_bay_launching19.png" )]
		public var fighter_bay_launching19:Class;

		// heavy_duel_plasma_firing
		// Animation
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing0.png" )]
		public var heavy_duel_plasma_firing0:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing1.png" )]
		public var heavy_duel_plasma_firing1:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing2.png" )]
		public var heavy_duel_plasma_firing2:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing3.png" )]
		public var heavy_duel_plasma_firing3:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing4.png" )]
		public var heavy_duel_plasma_firing4:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing5.png" )]
		public var heavy_duel_plasma_firing5:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing6.png" )]
		public var heavy_duel_plasma_firing6:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing7.png" )]
		public var heavy_duel_plasma_firing7:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing8.png" )]
		public var heavy_duel_plasma_firing8:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing9.png" )]
		public var heavy_duel_plasma_firing9:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing10.png" )]
		public var heavy_duel_plasma_firing10:Class;
		[Embed ( source="converted/animations_pack/heavy_duel_plasma_firing11.png" )]
		public var heavy_duel_plasma_firing11:Class;

		// heavy_plasma_firing
		// Animation
		[Embed ( source="converted/animations_pack/heavy_plasma_firing0.png" )]
		public var heavy_plasma_firing0:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing1.png" )]
		public var heavy_plasma_firing1:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing2.png" )]
		public var heavy_plasma_firing2:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing3.png" )]
		public var heavy_plasma_firing3:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing4.png" )]
		public var heavy_plasma_firing4:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing5.png" )]
		public var heavy_plasma_firing5:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing6.png" )]
		public var heavy_plasma_firing6:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing7.png" )]
		public var heavy_plasma_firing7:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing8.png" )]
		public var heavy_plasma_firing8:Class;
		[Embed ( source="converted/animations_pack/heavy_plasma_firing9.png" )]
		public var heavy_plasma_firing9:Class;

		// human_artillery_firing
		// Animation
		[Embed ( source="converted/animations_pack/human_artillery_firing0.png" )]
		public var human_artillery_firing0:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing1.png" )]
		public var human_artillery_firing1:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing2.png" )]
		public var human_artillery_firing2:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing3.png" )]
		public var human_artillery_firing3:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing4.png" )]
		public var human_artillery_firing4:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing5.png" )]
		public var human_artillery_firing5:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing6.png" )]
		public var human_artillery_firing6:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing7.png" )]
		public var human_artillery_firing7:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing8.png" )]
		public var human_artillery_firing8:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing9.png" )]
		public var human_artillery_firing9:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing10.png" )]
		public var human_artillery_firing10:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing11.png" )]
		public var human_artillery_firing11:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing12.png" )]
		public var human_artillery_firing12:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing13.png" )]
		public var human_artillery_firing13:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing14.png" )]
		public var human_artillery_firing14:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing15.png" )]
		public var human_artillery_firing15:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing16.png" )]
		public var human_artillery_firing16:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing17.png" )]
		public var human_artillery_firing17:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing18.png" )]
		public var human_artillery_firing18:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing19.png" )]
		public var human_artillery_firing19:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing20.png" )]
		public var human_artillery_firing20:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing21.png" )]
		public var human_artillery_firing21:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing22.png" )]
		public var human_artillery_firing22:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing23.png" )]
		public var human_artillery_firing23:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing24.png" )]
		public var human_artillery_firing24:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing25.png" )]
		public var human_artillery_firing25:Class;
		[Embed ( source="converted/animations_pack/human_artillery_firing26.png" )]
		public var human_artillery_firing26:Class;

		// mass_driver_firing
		// Animation
		[Embed ( source="converted/animations_pack/mass_driver_firing0.png" )]
		public var mass_driver_firing0:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing1.png" )]
		public var mass_driver_firing1:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing2.png" )]
		public var mass_driver_firing2:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing3.png" )]
		public var mass_driver_firing3:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing4.png" )]
		public var mass_driver_firing4:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing5.png" )]
		public var mass_driver_firing5:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing6.png" )]
		public var mass_driver_firing6:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing7.png" )]
		public var mass_driver_firing7:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing8.png" )]
		public var mass_driver_firing8:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing9.png" )]
		public var mass_driver_firing9:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing10.png" )]
		public var mass_driver_firing10:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing11.png" )]
		public var mass_driver_firing11:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing12.png" )]
		public var mass_driver_firing12:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing13.png" )]
		public var mass_driver_firing13:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing14.png" )]
		public var mass_driver_firing14:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing15.png" )]
		public var mass_driver_firing15:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing16.png" )]
		public var mass_driver_firing16:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing17.png" )]
		public var mass_driver_firing17:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing18.png" )]
		public var mass_driver_firing18:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing19.png" )]
		public var mass_driver_firing19:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing20.png" )]
		public var mass_driver_firing20:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing21.png" )]
		public var mass_driver_firing21:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing22.png" )]
		public var mass_driver_firing22:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing23.png" )]
		public var mass_driver_firing23:Class;
		[Embed ( source="converted/animations_pack/mass_driver_firing24.png" )]
		public var mass_driver_firing24:Class;

		// plasma_artillery_firing
		// Animation
		[Embed ( source="converted/animations_pack/plasma_artillery_firing0.png" )]
		public var plasma_artillery_firing0:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing1.png" )]
		public var plasma_artillery_firing1:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing2.png" )]
		public var plasma_artillery_firing2:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing3.png" )]
		public var plasma_artillery_firing3:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing4.png" )]
		public var plasma_artillery_firing4:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing5.png" )]
		public var plasma_artillery_firing5:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing6.png" )]
		public var plasma_artillery_firing6:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing7.png" )]
		public var plasma_artillery_firing7:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing8.png" )]
		public var plasma_artillery_firing8:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing9.png" )]
		public var plasma_artillery_firing9:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing10.png" )]
		public var plasma_artillery_firing10:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing11.png" )]
		public var plasma_artillery_firing11:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing12.png" )]
		public var plasma_artillery_firing12:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing13.png" )]
		public var plasma_artillery_firing13:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing14.png" )]
		public var plasma_artillery_firing14:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing15.png" )]
		public var plasma_artillery_firing15:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing16.png" )]
		public var plasma_artillery_firing16:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing17.png" )]
		public var plasma_artillery_firing17:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing18.png" )]
		public var plasma_artillery_firing18:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing19.png" )]
		public var plasma_artillery_firing19:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing20.png" )]
		public var plasma_artillery_firing20:Class;
		[Embed ( source="converted/animations_pack/plasma_artillery_firing21.png" )]
		public var plasma_artillery_firing21:Class;

		// plasma_pulse_firing
		// Animation
		[Embed ( source="converted/animations_pack/plasma_pulse_firing0.png" )]
		public var plasma_pulse_firing0:Class;
		[Embed ( source="converted/animations_pack/plasma_pulse_firing1.png" )]
		public var plasma_pulse_firing1:Class;
		[Embed ( source="converted/animations_pack/plasma_pulse_firing2.png" )]
		public var plasma_pulse_firing2:Class;
		[Embed ( source="converted/animations_pack/plasma_pulse_firing3.png" )]
		public var plasma_pulse_firing3:Class;
		[Embed ( source="converted/animations_pack/plasma_pulse_firing4.png" )]
		public var plasma_pulse_firing4:Class;

		// shield_explosion
		// Animation
		[Embed ( source="converted/animations_pack/shield_explosion0.png" )]
		public var shield_explosion0:Class;
		[Embed ( source="converted/animations_pack/shield_explosion1.png" )]
		public var shield_explosion1:Class;
		[Embed ( source="converted/animations_pack/shield_explosion2.png" )]
		public var shield_explosion2:Class;
		[Embed ( source="converted/animations_pack/shield_explosion3.png" )]
		public var shield_explosion3:Class;
		[Embed ( source="converted/animations_pack/shield_explosion4.png" )]
		public var shield_explosion4:Class;
		[Embed ( source="converted/animations_pack/shield_explosion5.png" )]
		public var shield_explosion5:Class;
		[Embed ( source="converted/animations_pack/shield_explosion6.png" )]
		public var shield_explosion6:Class;
		[Embed ( source="converted/animations_pack/shield_explosion7.png" )]
		public var shield_explosion7:Class;
		[Embed ( source="converted/animations_pack/shield_explosion8.png" )]
		public var shield_explosion8:Class;
		[Embed ( source="converted/animations_pack/shield_explosion9.png" )]
		public var shield_explosion9:Class;
		[Embed ( source="converted/animations_pack/shield_explosion10.png" )]
		public var shield_explosion10:Class;

		// single_explosion
		// Animation
		[Embed ( source="converted/animations_pack/single_explosion0.png" )]
		public var single_explosion0:Class;
		[Embed ( source="converted/animations_pack/single_explosion1.png" )]
		public var single_explosion1:Class;
		[Embed ( source="converted/animations_pack/single_explosion2.png" )]
		public var single_explosion2:Class;
		[Embed ( source="converted/animations_pack/single_explosion3.png" )]
		public var single_explosion3:Class;
		[Embed ( source="converted/animations_pack/single_explosion4.png" )]
		public var single_explosion4:Class;
		[Embed ( source="converted/animations_pack/single_explosion5.png" )]
		public var single_explosion5:Class;
		[Embed ( source="converted/animations_pack/single_explosion6.png" )]
		public var single_explosion6:Class;
		[Embed ( source="converted/animations_pack/single_explosion7.png" )]
		public var single_explosion7:Class;
		[Embed ( source="converted/animations_pack/single_explosion8.png" )]
		public var single_explosion8:Class;
		[Embed ( source="converted/animations_pack/single_explosion9.png" )]
		public var single_explosion9:Class;
		[Embed ( source="converted/animations_pack/single_explosion10.png" )]
		public var single_explosion10:Class;
		[Embed ( source="converted/animations_pack/single_explosion11.png" )]
		public var single_explosion11:Class;
		[Embed ( source="converted/animations_pack/single_explosion12.png" )]
		public var single_explosion12:Class;
		[Embed ( source="converted/animations_pack/single_explosion13.png" )]
		public var single_explosion13:Class;
		[Embed ( source="converted/animations_pack/single_explosion14.png" )]
		public var single_explosion14:Class;
		[Embed ( source="converted/animations_pack/single_explosion15.png" )]
		public var single_explosion15:Class;
		[Embed ( source="converted/animations_pack/single_explosion16.png" )]
		public var single_explosion16:Class;
		[Embed ( source="converted/animations_pack/single_explosion17.png" )]
		public var single_explosion17:Class;
		[Embed ( source="converted/animations_pack/single_explosion18.png" )]
		public var single_explosion18:Class;
		[Embed ( source="converted/animations_pack/single_explosion19.png" )]
		public var single_explosion19:Class;

		// multi_explosion
		// Animation
		[Embed ( source="converted/animations_pack/multi_explosion0.png" )]
		public var multi_explosion0:Class;
		[Embed ( source="converted/animations_pack/multi_explosion1.png" )]
		public var multi_explosion1:Class;
		[Embed ( source="converted/animations_pack/multi_explosion2.png" )]
		public var multi_explosion2:Class;
		[Embed ( source="converted/animations_pack/multi_explosion3.png" )]
		public var multi_explosion3:Class;
		[Embed ( source="converted/animations_pack/multi_explosion4.png" )]
		public var multi_explosion4:Class;
		[Embed ( source="converted/animations_pack/multi_explosion5.png" )]
		public var multi_explosion5:Class;
		[Embed ( source="converted/animations_pack/multi_explosion6.png" )]
		public var multi_explosion6:Class;
		[Embed ( source="converted/animations_pack/multi_explosion7.png" )]
		public var multi_explosion7:Class;
		[Embed ( source="converted/animations_pack/multi_explosion8.png" )]
		public var multi_explosion8:Class;
		[Embed ( source="converted/animations_pack/multi_explosion9.png" )]
		public var multi_explosion9:Class;
		[Embed ( source="converted/animations_pack/multi_explosion10.png" )]
		public var multi_explosion10:Class;
		[Embed ( source="converted/animations_pack/multi_explosion11.png" )]
		public var multi_explosion11:Class;
		[Embed ( source="converted/animations_pack/multi_explosion12.png" )]
		public var multi_explosion12:Class;
		[Embed ( source="converted/animations_pack/multi_explosion13.png" )]
		public var multi_explosion13:Class;
		[Embed ( source="converted/animations_pack/multi_explosion14.png" )]
		public var multi_explosion14:Class;
		[Embed ( source="converted/animations_pack/multi_explosion15.png" )]
		public var multi_explosion15:Class;
		[Embed ( source="converted/animations_pack/multi_explosion16.png" )]
		public var multi_explosion16:Class;
		[Embed ( source="converted/animations_pack/multi_explosion17.png" )]
		public var multi_explosion17:Class;
		[Embed ( source="converted/animations_pack/multi_explosion18.png" )]
		public var multi_explosion18:Class;
		[Embed ( source="converted/animations_pack/multi_explosion19.png" )]
		public var multi_explosion19:Class;
		[Embed ( source="converted/animations_pack/multi_explosion20.png" )]
		public var multi_explosion20:Class;
		[Embed ( source="converted/animations_pack/multi_explosion21.png" )]
		public var multi_explosion21:Class;
		[Embed ( source="converted/animations_pack/multi_explosion22.png" )]
		public var multi_explosion22:Class;
		[Embed ( source="converted/animations_pack/multi_explosion23.png" )]
		public var multi_explosion23:Class;
		[Embed ( source="converted/animations_pack/multi_explosion24.png" )]
		public var multi_explosion24:Class;
		[Embed ( source="converted/animations_pack/multi_explosion25.png" )]
		public var multi_explosion25:Class;
		[Embed ( source="converted/animations_pack/multi_explosion26.png" )]
		public var multi_explosion26:Class;
		[Embed ( source="converted/animations_pack/multi_explosion27.png" )]
		public var multi_explosion27:Class;
		[Embed ( source="converted/animations_pack/multi_explosion28.png" )]
		public var multi_explosion28:Class;
		[Embed ( source="converted/animations_pack/multi_explosion29.png" )]
		public var multi_explosion29:Class;

		// death_explosion
		// Animation
		[Embed ( source="converted/animations_pack/death_explosion0.png" )]
		public var death_explosion0:Class;
		[Embed ( source="converted/animations_pack/death_explosion1.png" )]
		public var death_explosion1:Class;
		[Embed ( source="converted/animations_pack/death_explosion2.png" )]
		public var death_explosion2:Class;
		[Embed ( source="converted/animations_pack/death_explosion3.png" )]
		public var death_explosion3:Class;
		[Embed ( source="converted/animations_pack/death_explosion4.png" )]
		public var death_explosion4:Class;
		[Embed ( source="converted/animations_pack/death_explosion5.png" )]
		public var death_explosion5:Class;
		[Embed ( source="converted/animations_pack/death_explosion6.png" )]
		public var death_explosion6:Class;
		[Embed ( source="converted/animations_pack/death_explosion7.png" )]
		public var death_explosion7:Class;
		[Embed ( source="converted/animations_pack/death_explosion8.png" )]
		public var death_explosion8:Class;
		[Embed ( source="converted/animations_pack/death_explosion9.png" )]
		public var death_explosion9:Class;
		[Embed ( source="converted/animations_pack/death_explosion10.png" )]
		public var death_explosion10:Class;
		[Embed ( source="converted/animations_pack/death_explosion11.png" )]
		public var death_explosion11:Class;
		[Embed ( source="converted/animations_pack/death_explosion12.png" )]
		public var death_explosion12:Class;
		[Embed ( source="converted/animations_pack/death_explosion13.png" )]
		public var death_explosion13:Class;
		[Embed ( source="converted/animations_pack/death_explosion14.png" )]
		public var death_explosion14:Class;
		[Embed ( source="converted/animations_pack/death_explosion15.png" )]
		public var death_explosion15:Class;
		[Embed ( source="converted/animations_pack/death_explosion16.png" )]
		public var death_explosion16:Class;
		[Embed ( source="converted/animations_pack/death_explosion17.png" )]
		public var death_explosion17:Class;
		[Embed ( source="converted/animations_pack/death_explosion18.png" )]
		public var death_explosion18:Class;
		[Embed ( source="converted/animations_pack/death_explosion19.png" )]
		public var death_explosion19:Class;
		[Embed ( source="converted/animations_pack/death_explosion20.png" )]
		public var death_explosion20:Class;
		[Embed ( source="converted/animations_pack/death_explosion21.png" )]
		public var death_explosion21:Class;
		[Embed ( source="converted/animations_pack/death_explosion22.png" )]
		public var death_explosion22:Class;
		[Embed ( source="converted/animations_pack/death_explosion23.png" )]
		public var death_explosion23:Class;
		[Embed ( source="converted/animations_pack/death_explosion24.png" )]
		public var death_explosion24:Class;
		[Embed ( source="converted/animations_pack/death_explosion25.png" )]
		public var death_explosion25:Class;
		[Embed ( source="converted/animations_pack/death_explosion26.png" )]
		public var death_explosion26:Class;
		[Embed ( source="converted/animations_pack/death_explosion27.png" )]
		public var death_explosion27:Class;
		[Embed ( source="converted/animations_pack/death_explosion28.png" )]
		public var death_explosion28:Class;
		[Embed ( source="converted/animations_pack/death_explosion29.png" )]
		public var death_explosion29:Class;
		[Embed ( source="converted/animations_pack/death_explosion30.png" )]
		public var death_explosion30:Class;
		[Embed ( source="converted/animations_pack/death_explosion31.png" )]
		public var death_explosion31:Class;
		[Embed ( source="converted/animations_pack/death_explosion32.png" )]
		public var death_explosion32:Class;
		[Embed ( source="converted/animations_pack/death_explosion33.png" )]
		public var death_explosion33:Class;
		[Embed ( source="converted/animations_pack/death_explosion34.png" )]
		public var death_explosion34:Class;

		// mini_death_explosion
		// Animation
		[Embed ( source="converted/animations_pack/mini_death_explosion0.png" )]
		public var mini_death_explosion0:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion1.png" )]
		public var mini_death_explosion1:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion2.png" )]
		public var mini_death_explosion2:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion3.png" )]
		public var mini_death_explosion3:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion4.png" )]
		public var mini_death_explosion4:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion5.png" )]
		public var mini_death_explosion5:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion6.png" )]
		public var mini_death_explosion6:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion7.png" )]
		public var mini_death_explosion7:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion8.png" )]
		public var mini_death_explosion8:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion9.png" )]
		public var mini_death_explosion9:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion10.png" )]
		public var mini_death_explosion10:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion11.png" )]
		public var mini_death_explosion11:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion12.png" )]
		public var mini_death_explosion12:Class;
		[Embed ( source="converted/animations_pack/mini_death_explosion13.png" )]
		public var mini_death_explosion13:Class;

		// human_armor_debris
		// Animation
		[Embed ( source="converted/animations_pack/human_armor_debris0.png" )]
		public var human_armor_debris0:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris1.png" )]
		public var human_armor_debris1:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris2.png" )]
		public var human_armor_debris2:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris3.png" )]
		public var human_armor_debris3:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris4.png" )]
		public var human_armor_debris4:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris5.png" )]
		public var human_armor_debris5:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris6.png" )]
		public var human_armor_debris6:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris7.png" )]
		public var human_armor_debris7:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris8.png" )]
		public var human_armor_debris8:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris9.png" )]
		public var human_armor_debris9:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris10.png" )]
		public var human_armor_debris10:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris11.png" )]
		public var human_armor_debris11:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris12.png" )]
		public var human_armor_debris12:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris13.png" )]
		public var human_armor_debris13:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris14.png" )]
		public var human_armor_debris14:Class;
		[Embed ( source="converted/animations_pack/human_armor_debris15.png" )]
		public var human_armor_debris15:Class;

		// argo_armor_debris
		// Animation
		[Embed ( source="converted/animations_pack/argo_armor_debris0.png" )]
		public var argo_armor_debris0:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris1.png" )]
		public var argo_armor_debris1:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris2.png" )]
		public var argo_armor_debris2:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris3.png" )]
		public var argo_armor_debris3:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris4.png" )]
		public var argo_armor_debris4:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris5.png" )]
		public var argo_armor_debris5:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris6.png" )]
		public var argo_armor_debris6:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris7.png" )]
		public var argo_armor_debris7:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris8.png" )]
		public var argo_armor_debris8:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris9.png" )]
		public var argo_armor_debris9:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris10.png" )]
		public var argo_armor_debris10:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris11.png" )]
		public var argo_armor_debris11:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris12.png" )]
		public var argo_armor_debris12:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris13.png" )]
		public var argo_armor_debris13:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris14.png" )]
		public var argo_armor_debris14:Class;
		[Embed ( source="converted/animations_pack/argo_armor_debris15.png" )]
		public var argo_armor_debris15:Class;

		// missile_explosion
		// Animation
		[Embed ( source="converted/animations_pack/missile_explosion0.png" )]
		public var missile_explosion0:Class;
		[Embed ( source="converted/animations_pack/missile_explosion1.png" )]
		public var missile_explosion1:Class;
		[Embed ( source="converted/animations_pack/missile_explosion2.png" )]
		public var missile_explosion2:Class;
		[Embed ( source="converted/animations_pack/missile_explosion3.png" )]
		public var missile_explosion3:Class;
		[Embed ( source="converted/animations_pack/missile_explosion4.png" )]
		public var missile_explosion4:Class;
		[Embed ( source="converted/animations_pack/missile_explosion5.png" )]
		public var missile_explosion5:Class;
		[Embed ( source="converted/animations_pack/missile_explosion6.png" )]
		public var missile_explosion6:Class;
		[Embed ( source="converted/animations_pack/missile_explosion7.png" )]
		public var missile_explosion7:Class;
		[Embed ( source="converted/animations_pack/missile_explosion8.png" )]
		public var missile_explosion8:Class;
		[Embed ( source="converted/animations_pack/missile_explosion9.png" )]
		public var missile_explosion9:Class;
		[Embed ( source="converted/animations_pack/missile_explosion10.png" )]
		public var missile_explosion10:Class;
		[Embed ( source="converted/animations_pack/missile_explosion11.png" )]
		public var missile_explosion11:Class;
		[Embed ( source="converted/animations_pack/missile_explosion12.png" )]
		public var missile_explosion12:Class;
		[Embed ( source="converted/animations_pack/missile_explosion13.png" )]
		public var missile_explosion13:Class;
		[Embed ( source="converted/animations_pack/missile_explosion14.png" )]
		public var missile_explosion14:Class;
		[Embed ( source="converted/animations_pack/missile_explosion15.png" )]
		public var missile_explosion15:Class;
		[Embed ( source="converted/animations_pack/missile_explosion16.png" )]
		public var missile_explosion16:Class;
		[Embed ( source="converted/animations_pack/missile_explosion17.png" )]
		public var missile_explosion17:Class;
		[Embed ( source="converted/animations_pack/missile_explosion18.png" )]
		public var missile_explosion18:Class;
		[Embed ( source="converted/animations_pack/missile_explosion19.png" )]
		public var missile_explosion19:Class;

	}
}
// EOF
